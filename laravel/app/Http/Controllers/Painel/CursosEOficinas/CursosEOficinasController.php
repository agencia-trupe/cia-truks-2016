<?php

namespace CiaTruks\Http\Controllers\Painel\CursosEOficinas;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;
use View;

use CiaTruks\Models\CursosEOficinas;

class CursosEOficinasController extends Controller{

	public function index()
	{
		$registros = CursosEOficinas::ordenado()->get();

		return view('painel.cursos-e-oficinas.index')->with(compact('registros'));
	}

	public function create()
	{
		return view('painel.cursos-e-oficinas.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
      'titulo' => 'required|unique:cursos_e_oficinas'
  	]);

		$object = new CursosEOficinas;

		$object->titulo = $request->titulo;
    $object->slug = str_slug($request->titulo);
    $object->texto = $request->texto;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Curso/Oficina criado com sucesso.');

			return redirect()->route('painel.cursos-e-oficinas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar Curso/Oficina! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$registro = CursosEOficinas::find($id);

		return view('painel.cursos-e-oficinas.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
        'titulo' => 'required|unique:cursos_e_oficinas,titulo,'.$id
    	]);

		$object = CursosEOficinas::find($id);

    $object->titulo = $request->titulo;
    $object->slug = str_slug($request->titulo);
    $object->texto = $request->texto;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Curso/Oficina alterado com sucesso.');

			return redirect()->route('painel.cursos-e-oficinas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar Curso/Oficina! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = CursosEOficinas::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Curso/Oficina removido com sucesso.');

		return redirect()->route('painel.cursos-e-oficinas.index');
	}
}
