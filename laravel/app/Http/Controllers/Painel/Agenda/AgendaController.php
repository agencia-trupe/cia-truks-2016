<?php

namespace CiaTruks\Http\Controllers\Painel\Agenda;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\Agenda;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      if($request->filtro == 'futuras')
        $registros = Agenda::ordenado()->datasFuturas()->get();
      elseif($request->filtro == 'passadas')
        $registros = Agenda::ordenado()->datasPassadas()->get();
      else
        $registros = Agenda::ordenado()->get();

      return view('painel.agenda.index')->with(compact('registros'))->with('filtro', $request->filtro);
    }

    public function create(Request $request)
    {
      $espetaculos = Espetaculo::all();
      return view('painel.agenda.create', compact('espetaculos'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'espetaculos_id' => 'required|exists:espetaculos,id',
        'data'           => 'required|date_format:d/m/Y',
    	]);

      $object = new Agenda;

      $object->espetaculos_id = $request->espetaculos_id;
      $object->data = $request->data;
      $object->horarios = $request->horarios;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Item da Agenda criado com sucesso.');

        return redirect()->route('painel.agenda.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar item da agenda! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $espetaculos = Espetaculo::all();
      return view('painel.agenda.edit')->with('registro', Agenda::find($id))
                                       ->with('espetaculos', $espetaculos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'espetaculos_id' => 'required|exists:espetaculos,id',
        'data'           => 'required|date_format:d/m/Y',
    	]);

      $object = Agenda::find($id);

      $object->espetaculos_id = $request->espetaculos_id;
      $object->data = $request->data;
      $object->horarios = $request->horarios;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Item da Agenda alterado com sucesso.');

        return redirect()->route('painel.agenda.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar item da agenda! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Agenda::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Item da Agenda removido com sucesso.');

      return redirect()->route('painel.agenda.index');
    }

}
