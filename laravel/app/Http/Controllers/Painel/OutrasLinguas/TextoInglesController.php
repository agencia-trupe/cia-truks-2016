<?php

namespace CiaTruks\Http\Controllers\Painel\OutrasLinguas;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\TextoIngles;

class TextoInglesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = TextoIngles::all();

      return view('painel.texto-ingles.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.texto-ingles.edit')->with('registro', TextoIngles::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $object = TextoIngles::find($id);
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Texto em Inglês alterado com sucesso.');

        return redirect()->route('painel.texto-ingles.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

      }
    }

}
