<?php

namespace CiaTruks\Http\Controllers\Painel\OutrasLinguas;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\TextoEspanhol;

class TextoEspanholController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = TextoEspanhol::all();

      return view('painel.texto-espanhol.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.texto-espanhol.edit')->with('registro', TextoEspanhol::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $object = TextoEspanhol::find($id);
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Texto em Espanhol alterado com sucesso.');

        return redirect()->route('painel.texto-espanhol.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

      }
    }

}
