<?php

namespace CiaTruks\Http\Controllers\Painel\PaginasTexto;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\PaginaTexto;
use CiaTruks\Models\Imagem;
use CiaTruks\Models\Video;

class PaginasTextoController extends Controller
{
  private $pathAlbum = 'paginas-texto/';

  public function index()
  {
    $registros = PaginaTexto::all();

    return view('painel.paginas-texto.index')->with(compact('registros'));
  }

  public function edit($id)
  {
    return view('painel.paginas-texto.edit')->with('registro', PaginaTexto::find($id))
                                            ->with('path', $this->pathAlbum);
  }

  public function update(Request $request, $id)
  {
    $object = PaginaTexto::find($id);

    $object->texto = $request->texto;

    try {

      $object->save();

      foreach ($object->imagens as $img_atual)
        $img_atual->delete();

      $imagens_albuns = $request->imagem_album;
      if(sizeof($imagens_albuns)){
        foreach ($imagens_albuns as $ordem => $img) {
          $object->imagens()->save( new Imagem([
              'imagem' => $img,
              'ordem' => $ordem
            ])
          );
        }
      }

      foreach ($object->videos as $video_atual)
        $video_atual->delete();

      $videos_urls = $request->video_url;
      if(sizeof($videos_urls)){
        foreach ($videos_urls as $ordem => $video) {
          $object->videos()->save( new Video([
              'url' => $video,
              'video_id' => $request->video_id[$ordem],
              'titulo' => $request->video_titulo[$ordem],
              'thumb' => $request->video_thumb[$ordem],
              'ordem' => $ordem
            ])
          );
        }
      }

      $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

      return redirect()->route('painel.paginas-texto.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

    }
  }
}
