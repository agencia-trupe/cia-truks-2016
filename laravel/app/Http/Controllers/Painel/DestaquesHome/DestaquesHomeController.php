<?php

namespace CiaTruks\Http\Controllers\Painel\DestaquesHome;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\DestaqueHome;

class DestaquesHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = DestaqueHome::ordenado()->get();

      return view('painel.destaques-home.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.destaques-home.edit')->with('registro', DestaqueHome::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required',
      	'link'   => 'required'
    	]);

      $object = DestaqueHome::find($id);
      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->link = $request->link;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Destaque alterada com sucesso.');

        return redirect()->route('painel.destaques-home.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar destaque! ('.$e->getMessage().')'));

      }
    }

}
