<?php

namespace CiaTruks\Http\Controllers\Painel\ChamadasHome;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\ChamadaHome;

class ChamadasHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = ChamadaHome::ordenado()->get();

      return view('painel.chamadas-home.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.chamadas-home.edit')->with('registro', ChamadaHome::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required',
      	'link'   => 'required'
    	]);

      $object = ChamadaHome::find($id);
      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->link = $request->link;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Chamada alterada com sucesso.');

        return redirect()->route('painel.chamadas-home.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar chamada! ('.$e->getMessage().')'));

      }
    }

}
