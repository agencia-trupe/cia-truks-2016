<?php

namespace CiaTruks\Http\Controllers\Painel\Imagens;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;
use CiaTruks\Libs\Thumbs;

use CiaTruks\Models\PaginaTexto;
use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\GaleriaAdicional;
use CiaTruks\Models\Imagem;

class ImagensController extends Controller
{

  /*
    Armazena as imagens enviadas via Ajax
  */
  public function postUpload(Request $request)
  {
    $arquivo = $request->file('files');
    $path    = $request->input('path');

    $path_original = "/novo/assets/img/{$path}/originais/";
    $path_asset    = "/novo/assets/img/{$path}/redimensionadas/";
    $path_thumb    = "/novo/assets/img/{$path}/thumbs/";
    $path_upload   = public_path($path_original);

    //$this->criarDiretoriosPadrao($path);

    $nome_arquivo = $arquivo->getClientOriginalName();
    $extensao_arquivo = $arquivo->getClientOriginalExtension();
    $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

    $filename = date('YmdHis').str_slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $name = $path_original.$filename;
    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada

    // redimensionadas
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);

    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 220, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    return [
      'envio' => 1,
      'thumb' => $thumb,
      'filename' => $filename
    ];
  }


  private function criarDiretoriosPadrao($path)
  {
    if(!file_exists(public_path("/novo/assets/img/{$path}")))
      mkdir(public_path("/novo/assets/img/{$path}"), 0777);

    if(!file_exists(public_path("/novo/assets/img/{$path}/originais/")))
      mkdir(public_path("/novo/assets/img/{$path}/originais/"), 0777);

    if(!file_exists(public_path("/novo/assets/img/{$path}/redimensionadas/")))
      mkdir(public_path("/novo/assets/img/{$path}/redimensionadas/"), 0777);

    if(!file_exists(public_path("/novo/assets/img/{$path}/thumbs/")))
      mkdir(public_path("/novo/assets/img/{$path}/thumbs/"), 0777);
  }

}
