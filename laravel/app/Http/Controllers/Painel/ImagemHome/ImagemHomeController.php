<?php

namespace CiaTruks\Http\Controllers\Painel\ImagemHome;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Libs\Thumbs;
use CiaTruks\Models\ImagemHome;

class ImagemHomeController extends Controller
{
  public function index()
  {
    $registros = ImagemHome::all();

    return view('painel.imagem-home.index')->with(compact('registros'));
  }

  public function edit($id)
  {
    return view('painel.imagem-home.edit')->with('registro', ImagemHome::find($id));
  }

  public function update(Request $request, $id)
  {
    $object = ImagemHome::find($id);

    $imagem = Thumbs::make('imagem', 380, 550, 'imagem-home/');
    if($imagem) $object->imagem = $imagem;

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Imagem da Home alterada com sucesso.');

      return redirect()->route('painel.imagem-home.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar Imagem da Home! ('.$e->getMessage().')'));

    }
  }
}
