<?php

namespace CiaTruks\Http\Controllers\Painel\CriticasEMaterias;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;
use CiaTruks\Libs\Thumbs;

use View;

use CiaTruks\Models\CriticasEMaterias;

class CriticasEMateriasController extends Controller{

	public function index()
	{
		$registros = CriticasEMaterias::ordenado()->get();

		return view('painel.criticas-e-materias.index')->with(compact('registros'));
	}

	public function create()
	{
		return view('painel.criticas-e-materias.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
      'titulo' => 'required',
			'data'   => 'required|date_format:d/m/Y',
			'imagem' => 'required|image'
  	]);

		$object = new CriticasEMaterias;

		$object->titulo = $request->titulo;
    $object->data = $request->data;

		$imagem = Thumbs::make('imagem', 900, null, 'criticas-e-materias/ampliadas/');
		if($imagem) $object->imagem = $imagem;

		Thumbs::make('imagem', 180, 180, 'criticas-e-materias/capas/');

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Crítica criada com sucesso.');

			return redirect()->route('painel.criticas-e-materias.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar Crítica! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$registro = CriticasEMaterias::find($id);

		return view('painel.criticas-e-materias.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
      'titulo' => 'required',
			'data'   => 'required|date_format:d/m/Y',
			'imagem' => 'sometimes|image'
  	]);

		$object = CriticasEMaterias::find($id);

		$object->titulo = $request->titulo;
    $object->data = $request->data;

		$imagem = Thumbs::make('imagem', 900, null, 'criticas-e-materias/ampliadas/');
		if($imagem) $object->imagem = $imagem;

		Thumbs::make('imagem', 180, 180, 'criticas-e-materias/capas/');

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Crítica alterada com sucesso.');

			return redirect()->route('painel.criticas-e-materias.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar Crítica! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = CriticasEMaterias::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Crítica removida com sucesso.');

		return redirect()->route('painel.criticas-e-materias.index');
	}
}
