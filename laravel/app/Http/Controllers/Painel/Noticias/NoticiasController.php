<?php

namespace CiaTruks\Http\Controllers\Painel\Noticias;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Libs\Thumbs;
use CiaTruks\Models\Noticia;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Noticia::ordenado()->get();

      return view('painel.noticias.index', compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.noticias.create', compact('espetaculos'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo' => 'required',
        'data'   => 'required|date_format:d/m/Y',
        'imagem' => 'required|image'
    	]);

      $object = new Noticia;

      $object->data   = $request->data;
      $object->titulo = $request->titulo;

      $slug = str_slug($object->data->format('d-m-Y').'-'.$request->titulo);
      $slug_append = 1;
      $check = Noticia::where('slug', $slug)->first();
      if(sizeof($check) > 0){
        while(sizeof($check) > 0){
          $slug = str_slug($object->data->format('d-m-Y').'-'.$request->titulo) . '-' . $slug_append;
          $slug_append++;
          $check = Noticia::where('slug', $slug)->first();
        }
      }
      $object->slug = $slug;

      $object->olho   = $request->olho;
      $object->texto  = $request->texto;

      $imagem = Thumbs::make('imagem', 300, 300, 'noticias/');
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Notícia criada com sucesso.');

        return redirect()->route('painel.noticias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar Notícia! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.noticias.edit')->with('registro', Noticia::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required',
        'data'   => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = Noticia::find($id);

      $object->data   = $request->data;
      $object->titulo = $request->titulo;

      $slug = str_slug($object->data->format('d-m-Y').'-'.$request->titulo);
      $slug_append = 1;
      $check = Noticia::where('slug', $slug)->where('id', '!=', $id)->first();
      if(sizeof($check) > 0){
        while(sizeof($check) > 0){
          $slug = str_slug($object->data->format('d-m-Y').'-'.$request->titulo) . '-' . $slug_append;
          $slug_append++;
          $check = Noticia::where('slug', $slug)->where('id', '!=', $id)->first();
        }
        $object->slug = $slug;
      }else{
        $object->slug = $slug;
      }

      $object->olho   = $request->olho;
      $object->texto  = $request->texto;

      $imagem = Thumbs::make('imagem', 300, 300, 'noticias/');
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Notícia alterada com sucesso.');

        return redirect()->route('painel.noticias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Notícia! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Noticia::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Notícia removida com sucesso.');

      return redirect()->route('painel.noticias.index');
    }

}
