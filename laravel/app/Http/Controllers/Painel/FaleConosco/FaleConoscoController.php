<?php

namespace CiaTruks\Http\Controllers\Painel\FaleConosco;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\FaleConosco;

class FaleConoscoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = FaleConosco::all();

      return view('painel.fale-conosco.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.fale-conosco.edit')->with('registro', FaleConosco::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'texto' => 'required'
    	]);

      $object = FaleConosco::find($id);
      $object->texto = $request->texto;
      $object->email = $request->email;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

        return redirect()->route('painel.fale-conosco.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

      }
    }

}
