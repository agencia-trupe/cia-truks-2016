<?php

namespace CiaTruks\Http\Controllers\Painel\Espetaculos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\ComentarioImprensa;

class ComentariosDaImprensaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $espetaculo = Espetaculo::findOrFail($request->espetaculos_id);

      return view('painel.comentarios-da-imprensa.index')->with(compact('espetaculo'));
    }

    public function create(Request $request)
    {
      $espetaculo = Espetaculo::findOrFail($request->espetaculos_id);
      return view('painel.comentarios-da-imprensa.create', compact('espetaculo'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'espetaculos_id' => 'required|exists:espetaculos,id',
        'autor' => 'required',
        'data' => 'required|date_format:d/m/Y'
    	]);

      $object = new ComentarioImprensa;

      $object->espetaculos_id = $request->espetaculos_id;
      $object->autor = $request->autor;
      $object->texto = $request->texto;
      $object->data = $request->data;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Comentário criado com sucesso.');

        return redirect()->route('painel.comentarios-da-imprensa.index', ['espetaculos_id' => $object->espetaculos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar Comentário! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
      $registro = ComentarioImprensa::find($id);
      $espetaculo = Espetaculo::findOrFail($registro->espetaculos_id);
      return view('painel.comentarios-da-imprensa.edit')->with(compact('registro', 'espetaculo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'espetaculos_id' => 'required|exists:espetaculos,id',
        'autor' => 'required',
        'data' => 'required|date_format:d/m/Y'
    	]);

      $object = ComentarioImprensa::find($id);

      $object->espetaculos_id = $request->espetaculos_id;
      $object->autor = $request->autor;
      $object->texto = $request->texto;
      $object->data = $request->data;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Comentário alterado com sucesso.');

        return redirect()->route('painel.comentarios-da-imprensa.index', ['espetaculos_id' => $object->espetaculos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Comentário! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = ComentarioImprensa::find($id);
      $id_redirecionamento = $object->espetaculos_id;
      $object->delete();

      $request->session()->flash('sucesso', 'Comentário removido com sucesso.');

      return redirect()->route('painel.comentarios-da-imprensa.index', ['espetaculos_id' => $id_redirecionamento]);
    }

}
