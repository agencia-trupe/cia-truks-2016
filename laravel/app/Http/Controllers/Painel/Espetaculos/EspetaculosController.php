<?php

namespace CiaTruks\Http\Controllers\Painel\Espetaculos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Libs\Thumbs;
use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\Imagem;
use CiaTruks\Models\Video;

class EspetaculosController extends Controller
{
  private $pathAlbum = 'espetaculos/';
  private $pathCapas = 'espetaculos/capas/';
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = Espetaculo::all();

      return view('painel.espetaculos.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.espetaculos.create', ['path' => $this->pathAlbum]);
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:espetaculos'
    	]);

      $object = new Espetaculo;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->ficha_tecnica = $request->ficha_tecnica;
      $object->texto = $request->texto;

      $imagem = Thumbs::make('imagem', 260, 390, $this->pathCapas);
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new Imagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $videos_urls = $request->video_url;
        if(sizeof($videos_urls)){
          foreach ($videos_urls as $ordem => $video) {
            $object->videos()->save( new Video([
                'url' => $video,
                'video_id' => $request->video_id[$ordem],
                'titulo' => $request->video_titulo[$ordem],
                'thumb' => $request->video_thumb[$ordem],
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Espetáculo criado com sucesso.');

        return redirect()->route('painel.espetaculos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.espetaculos.edit')->with('registro', Espetaculo::find($id))
                                            ->with('path', $this->pathAlbum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:espetaculos,titulo,'.$id
    	]);

      $object = Espetaculo::find($id);

      $object->titulo = $request->titulo;
      $object->ficha_tecnica = $request->ficha_tecnica;
      $object->texto = $request->texto;

      $imagem = Thumbs::make('imagem', 260, 390, $this->pathCapas);
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        foreach ($object->imagens as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new Imagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        foreach ($object->videos as $video_atual)
          $video_atual->delete();

        $videos_urls = $request->video_url;
        if(sizeof($videos_urls)){
          foreach ($videos_urls as $ordem => $video) {
            $object->videos()->save( new Video([
                'url' => $video,
                'video_id' => $request->video_id[$ordem],
                'titulo' => $request->video_titulo[$ordem],
                'thumb' => $request->video_thumb[$ordem],
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Espetáculo alterado com sucesso.');

        return redirect()->route('painel.espetaculos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Espetaculo::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Espetáculo removido com sucesso.');

      return redirect()->route('painel.espetaculos.index');
    }

}
