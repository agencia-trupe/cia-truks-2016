<?php

namespace CiaTruks\Http\Controllers\Painel\Videos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Libs\Youtube;
use CiaTruks\Models\Video;

class VideosAdicionaisController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = Video::adicionais()->ordenado()->get();

      return view('painel.videos-adicionais.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.videos-adicionais.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'url' => 'required'
    	]);

      $youtube = new Youtube($request->url);

      if(!$youtube->getId()){
        $request->flash();
        return back()->withErrors(array('Vídeo inválido!'));
      }

      $object = new Video;

      $object->url = $request->url;
      $object->video_id = $youtube->getId();
      $object->titulo = $youtube->getTitulo();
      $object->thumb = $youtube->getThumbnail();

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Vídeo Adicional criado com sucesso.');

        return redirect()->route('painel.videos-adicionais.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao adicionar vídeo! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.videos-adicionais.edit')->with('registro', Video::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'url' => 'required'
    	]);

      $youtube = new Youtube($request->url);

      if(!$youtube->getId()){
        $request->flash();
        return back()->withErrors(array('Vídeo inválido!'));
      }

      $object = Video::find($id);

      $object->url = $request->url;
      $object->video_id = $youtube->getId();
      $object->titulo = $youtube->getTitulo();
      $object->thumb = $youtube->getThumbnail();

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Vídeo Adicional alterado com sucesso.');

        return redirect()->route('painel.videos-adicionais.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Vídeo Adicional! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Video::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Video adicional removido com sucesso.');

      return redirect()->route('painel.videos-adicionais.index');
    }

}
