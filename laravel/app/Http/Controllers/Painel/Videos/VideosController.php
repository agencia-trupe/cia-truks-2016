<?php

namespace CiaTruks\Http\Controllers\Painel\Videos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;
use CiaTruks\Libs\Thumbs;
use CiaTruks\Libs\Youtube;

use CiaTruks\Models\Video;

class VideosController extends Controller
{

  /*
    Consulta a url de um vídeo do youtube
  */
  public function postConsultar(Request $request)
  {
    $url = $request->url;

    $youtube = new Youtube($url);

    return [
      'video_id' => $youtube->getId(),
      'titulo' => $youtube->getTitulo(),
      'thumb' => $youtube->getThumbnail(),
      'url' => $youtube->getUrl()
    ];
  }

  public function postAtualizarTitulo(Request $request)
  {
    $video = Video::find($request->pk);

    $video->titulo = $request->value;
    $video->save();
  }

}
