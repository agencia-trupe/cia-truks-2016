<?php

namespace CiaTruks\Http\Controllers\Painel\GaleriasAdicionais;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\GaleriaAdicional;
use CiaTruks\Models\Imagem;

class GaleriasAdicionaisController extends Controller
{
  private $pathAlbum = 'galerias-adicionais/';

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $registros = GaleriaAdicional::ordenado()->get();

    return view('painel.galerias-adicionais.index')->with(compact('registros'));
  }

  public function create(Request $request)
  {
    return view('painel.galerias-adicionais.create', ['path' => $this->pathAlbum]);
  }

  public function store(Request $request)
  {
    $this->validate($request, [
    	'titulo' => 'required|unique:galerias_adicionais'
  	]);

    $object = new GaleriaAdicional;

    $object->titulo = $request->titulo;
    $object->slug = str_slug($request->titulo);

    try {

      $object->save();

      $imagens_albuns = $request->imagem_album;
      if(sizeof($imagens_albuns)){
        foreach ($imagens_albuns as $ordem => $img) {
          $object->imagens()->save( new Imagem([
              'imagem' => $img,
              'ordem' => $ordem
            ])
          );
        }
      }

      $request->session()->flash('sucesso', 'Galeria Adicional criada com sucesso.');

      return redirect()->route('painel.galerias-adicionais.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar Galeria Adicional! ('.$e->getMessage().')'));

    }
  }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.galerias-adicionais.edit')->with('registro', GaleriaAdicional::find($id))
                                                    ->with('path', $this->pathAlbum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:galerias_adicionais,titulo,'.$id
    	]);

      $object = GaleriaAdicional::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);

      try {

        $object->save();

        foreach ($object->imagens as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new Imagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Galeria Adicional alterada com sucesso.');

        return redirect()->route('painel.galerias-adicionais.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Galeria Adicional! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = GaleriaAdicional::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Galeria Adicional removida com sucesso.');

      return redirect()->route('painel.galerias-adicionais.index');
    }

}
