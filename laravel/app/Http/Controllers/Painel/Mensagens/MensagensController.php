<?php

namespace CiaTruks\Http\Controllers\Painel\Mensagens;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Mensagem;

class MensagensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = Mensagem::ordenado()->paginate('30');

      return view('painel.mensagens.index')->with(compact('registros'));
    }

    public function postAprovacaoToggle(Request $request)
    {
      $object = Mensagem::find($request->id);

      if($object->aprovado == 1){
        $object->aprovado = 0;
      }elseif($object->aprovado == 0){
        $object->aprovado = 1;
      }

      $object->save();

      return $object->aprovado;
    }

    public function destroy(Request $request, $id){
      $object = Mensagem::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Mensagem removida com sucesso.');

      return redirect()->route('painel.mensagens.index');
    }

}
