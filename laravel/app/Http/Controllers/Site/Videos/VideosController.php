<?php

namespace CiaTruks\Http\Controllers\Site\Videos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Video;
use CiaTruks\Libs\Youtube;

class VideosController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.videos.index', ['videos' => Video::ordenado()->get()]);
  }

  public function getDetalhes(Request $request, $slug)
  {
    $video = Video::find( (int) $slug);
    $youtube = new Youtube($video->url);

    return view('site.videos.detalhes', ['video' => $video, 'youtube' => $youtube]);
  }
}
