<?php

namespace CiaTruks\Http\Controllers\Site\Espetaculos;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Espetaculo;

class EspetaculosController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.espetaculos.index');
  }

  public function getDetalhes(Request $request, $slug)
  {
    $espetaculo = Espetaculo::findBySlug($slug);

    if(!$espetaculo) return redirect()->route('home');

    return view('site.espetaculos.detalhes', compact('espetaculo'));
  }

}
