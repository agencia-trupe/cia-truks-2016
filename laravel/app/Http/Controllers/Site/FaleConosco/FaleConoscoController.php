<?php

namespace CiaTruks\Http\Controllers\Site\FaleConosco;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use Mail;
use View;

class FaleConoscoController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.fale-conosco.index');
  }

  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'faleconosco.nome' => 'required',
      'faleconosco.email' => 'required',
      'faleconosco.telefone' => 'required',
      'faleconosco.mensagem' => 'required'
    ]);

    $data['nome'] = $request->input('faleconosco.nome');
    $data['email'] = $request->input('faleconosco.email');
    $data['telefone'] = $request->input('faleconosco.telefone');
    $data['mensagem'] = $request->input('faleconosco.mensagem');

    Mail::send('emails.contato', $data, function($message) use ($data)
    {
      $message->to('contato@ciatruks.com.br', 'Cia Truks')
              ->subject('Contato via site')
              ->bcc('bruno@trupe.net')
              ->replyTo($data['email'], $data['nome']);
    });


    $request->session()->flash('contato-enviada', 'Mensagem enviada com sucesso!');

    return redirect()->route('fale-conosco');
  }

}
