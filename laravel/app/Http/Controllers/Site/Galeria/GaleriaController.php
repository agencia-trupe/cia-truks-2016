<?php

namespace CiaTruks\Http\Controllers\Site\Galeria;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\PaginaTexto;
use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\GaleriaAdicional;

class GaleriaController extends Controller
{

  public function getIndex(Request $request)
  {
    $albuns['espetaculos']         = Espetaculo::has('imagens')->get();
    $albuns['paginas-texto']       = PaginaTexto::has('imagens')->get();
    $albuns['galerias-adicionais'] = GaleriaAdicional::has('imagens')->get();

    return view('site.galeria-de-fotos.index', compact('albuns'));
  }

}
