<?php

namespace CiaTruks\Http\Controllers\Site\CriticasEMaterias;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\CriticasEMaterias;

class CriticasEMateriasController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.criticas-e-materias.index', ['criticas' => CriticasEMaterias::ordenado()->get()]);
  }

}
