<?php

namespace CiaTruks\Http\Controllers\Site\CursosEOficinas;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\CursosEOficinas;

class CursosEOficinasController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.cursos-e-oficinas.index', ['texto' => CursosEOficinas::findBySlug($request->slug)]);
  }

  public function getDetalhes(Request $request, $slug)
  {
    $espetaculo = Espetaculo::findBySlug($slug);

    if(!$espetaculo) return redirect()->route('home');

    return view('site.cursos-e-oficinas.detalhes', compact('espetaculo'));
  }

}
