<?php

namespace CiaTruks\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\ImagemHome;
use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\DestaqueHome;
use CiaTruks\Models\Video;
use CiaTruks\Models\Imagem;
use CiaTruks\Models\Agenda;
use CiaTruks\Models\Noticia;
use CiaTruks\Models\ChamadaHome;
use CiaTruks\Models\Mensagem;
use CiaTruks\Models\FaleConosco;

class HomeController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.home.index', [
      'imagemHome'  => ImagemHome::first(),
      'destaques'   => DestaqueHome::ordenado()->get(),
      'videos'      => Video::destaques()->get(),
      'galerias'    => Imagem::destaques()->get(),
      'agendas'     => Agenda::proximoPrimeiro()->datasFuturas()->limit(3)->get(),
      'noticias'    => Noticia::ordenado()->limit(3)->get(),
      'chamadas'    => ChamadaHome::ordenado()->get(),
      'mensagens'   => Mensagem::aprovados()->ordenado()->limit(5)->get()
    ]);
  }

  public function postEnviarMensagem(Request $request)
  {
    $this->validate($request, [
      'mensagem.nome' => 'required',
      'mensagem.email' => 'required',
      'mensagem.texto' => 'required'
    ]);

    $msg = new Mensagem;

    $msg->nome = htmlentities($request->input('mensagem.nome'));
    $msg->email = htmlentities($request->input('mensagem.email'));
    $msg->texto = htmlentities($request->input('mensagem.texto'));

    $msg->save();

    $request->session()->flash('mensagem-enviada', 'Mensagem enviada com sucesso!');

    return redirect()->route('home');
  }

}
