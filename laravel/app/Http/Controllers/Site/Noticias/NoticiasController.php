<?php

namespace CiaTruks\Http\Controllers\Site\Noticias;

use Illuminate\Http\Request;
use CiaTruks\Http\Controllers\Controller;

use View;

use CiaTruks\Models\Noticia;


class NoticiasController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.noticias.index', ['noticias' => Noticia::ordenado()->get()]);
  }

  public function getDetalhes(Request $request, $slug)
  {
    $noticia = Noticia::findBySlug($slug);

    $proxima_noticia = Noticia::orderBy('data', 'asc')->where('data', '>', $noticia->data)->first();
    $noticia_anterior = Noticia::orderBy('data', 'desc')->where('data', '<', $noticia->data)->first();

    $mais_noticias = Noticia::where('id', '!=', $noticia->id)->ordenado()->limit(3)->get();

    return view('site.noticias.detalhes', [
      'noticia' => $noticia,
      'proxima_noticia' => $proxima_noticia,
      'noticia_anterior' => $noticia_anterior,
      'mais_noticias' => $mais_noticias
    ]);
  }
}
