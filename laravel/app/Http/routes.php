<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
  'namespace' => 'Site'
], function(){

  Route::get('/', ['as' => 'home', 'uses' => 'Home\HomeController@getIndex']);
  Route::post('enviar-mensagem', ['as' => 'enviar-mensagem', 'uses' => 'Home\HomeController@postEnviarMensagem']);

  Route::get('a-companhia',              ['as' => 'a-companhia',                function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('a-companhia')]); }]);
  Route::get('eventos',                  ['as' => 'eventos',                    function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('eventos')]); }]);
  Route::get('centro-de-estudos',        ['as' => 'centro-de-estudos',          function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('centro-de-estudos')]); }]);
  Route::get('contacao-de-historias',    ['as' => 'contacao-de-historias',      function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('contacao-de-historias')]); }]);
  Route::get('contratacoes',             ['as' => 'contratacoes',               function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('contratacoes')]); }]);
  Route::get('cursos-e-oficinas',        ['as' => 'cursos-e-oficinas',          function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\PaginaTexto::findBySlug('cursos-e-oficinas')]); }]);
  Route::get('cursos-e-oficinas/{slug}', ['as' => 'cursos-e-oficinas.detalhes', 'uses' => 'CursosEOficinas\CursosEOficinasController@getIndex']);
  Route::get('agenda',                   ['as' => 'agenda',                     function(){ return view('site.agenda.index',       ['agendas' => CiaTruks\Models\Agenda::proximoPrimeiro()->datasFuturas()->get()]); }]);

  Route::get('espetaculos',        ['as' => 'espetaculos.index', 'uses' => 'Espetaculos\EspetaculosController@getIndex']);
  Route::get('espetaculos/{slug}', ['as' => 'espetaculos.detalhes', 'uses' => 'Espetaculos\EspetaculosController@getDetalhes']);
  Route::get('galeria-de-fotos',   ['as' => 'galeria-de-fotos', 'uses' => 'Galeria\GaleriaController@getIndex']);
  Route::get('videos',             ['as' => 'videos', 'uses' => 'Videos\VideosController@getIndex']);
  Route::get('videos/{slug}',      ['as' => 'videos.detalhes', 'uses' => 'Videos\VideosController@getDetalhes']);
  Route::get('noticias', ['as' => 'noticias.index', 'uses' => 'Noticias\NoticiasController@getIndex']);
  Route::get('noticias/{slug}', ['as' => 'noticias.detalhes', 'uses' => 'Noticias\NoticiasController@getDetalhes']);

  Route::get('criticas-e-materias', ['as' => 'criticas-e-materias', 'uses' => 'CriticasEMaterias\CriticasEMateriasController@getIndex']);
  Route::get('fale-conosco',  ['as' => 'fale-conosco', 'uses' => 'FaleConosco\FaleConoscoController@getIndex']);
  Route::post('fale-conosco', ['as' => 'fale-conosco.enviar', 'uses' => 'FaleConosco\FaleConoscoController@postEnviar']);

  Route::get('texto-ingles', ['as' => 'texto-ingles', function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\TextoIngles::first()]); }]);
  Route::get('texto-espanhol', ['as' => 'texto-espanhol', function(){ return view('site.pagina-texto.index', ['texto' => CiaTruks\Models\TextoEspanhol::first()]); }]);

});

// Authentication routes
Route::get('painel/auth/login', ['as' => 'painel.login','uses' => 'Painel\Auth\AuthController@getLogin']);
Route::post('painel/auth/login', ['as' => 'painel.auth','uses' => 'Painel\Auth\AuthController@postLogin']);
Route::get('painel/auth/logout', ['as' => 'painel.logout','uses' => 'Painel\Auth\AuthController@getLogout']);

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Painel',
  'prefix' => 'painel'
], function() {

  Route::get('/', ['as' => 'painel.dashboard', 'uses' => function() {
    return view('painel.dashboard.index');
  }]);

  Route::post('gravar-ordem-registros', function(Request $request){
    if(Request::ajax()){

      $menu   = Input::get('data');
			$tabela = Input::get('tabela');

      for ($i = 0; $i < count($menu); $i++)
	    	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));

    }
  });

  Route::resource('usuarios', 'Usuarios\UsuariosController');
  Route::resource('espetaculos', 'Espetaculos\EspetaculosController', ['except' => ['show']]);
  Route::resource('comentarios-da-imprensa', 'Espetaculos\ComentariosDaImprensaController', ['except' => ['show']]);

  Route::post('mensagens/toggle-aprovacao', ['as' => 'painel.mensagens.toggle-aprovacao', 'uses' => 'Mensagens\MensagensController@postAprovacaoToggle']);
  Route::resource('mensagens', 'Mensagens\MensagensController', ['only' => ['index', 'destroy']]);
  Route::resource('paginas-texto', 'PaginasTexto\PaginasTextoController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('cursos-e-oficinas', 'CursosEOficinas\CursosEOficinasController', ['except' => ['show']]);
  Route::resource('criticas-e-materias', 'CriticasEMaterias\CriticasEMateriasController', ['except' => ['show']]);
  Route::resource('fale-conosco', 'FaleConosco\FaleConoscoController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('destaques-home', 'DestaquesHome\DestaquesHomeController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('chamadas-home', 'ChamadasHome\ChamadasHomeController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('imagem-home', 'ImagemHome\ImagemHomeController', ['except' => ['show', 'create', 'store', 'destroy']]);
  Route::resource('texto-espanhol', 'OutrasLinguas\TextoEspanholController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('texto-ingles', 'OutrasLinguas\TextoInglesController', ['only' => ['index', 'edit', 'update']]);
  Route::resource('agenda', 'Agenda\AgendaController', ['except' => ['show']]);
  Route::resource('noticias', 'Noticias\NoticiasController', ['except' => ['show']]);
  Route::resource('galerias-adicionais', 'GaleriasAdicionais\GaleriasAdicionaisController', ['except' => ['show']]);
  Route::resource('videos-adicionais', 'Videos\VideosAdicionaisController', ['except' => ['show', 'edit', 'update']]);

  Route::post('imagens/upload', 'Imagens\ImagensController@postUpload');
  Route::post('videos/consultar', 'Videos\VideosController@postConsultar');
  Route::post('videos/atualizar-titulo', 'Videos\VideosController@postAtualizarTitulo');

});
