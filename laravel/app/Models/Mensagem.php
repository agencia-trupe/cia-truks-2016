<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class Mensagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mensagens';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nome',
    'email',
    'texto',
    'aprovado',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeAprovados($query)
  {
    return $query->where('aprovado', 1);
  }

  public function scopeReprovados($query)
  {
    return $query->where('aprovado', 0);
  }
}
