<?php

namespace CiaTruks\Models;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class CriticasEMaterias extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'criticas_e_materias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'data',
    'imagem'
  ];

  protected $dates = ['data', 'created_at', 'updated_at'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataAttribute($value)
  {
    $data = Carbon::createFromFormat('d/m/Y', $value);
    $this->attributes['data'] = $data->format('Y-m-d');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }
}
