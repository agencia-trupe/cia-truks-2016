<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class Espetaculo extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'espetaculos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'login',
    'email',
    'password'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function imagens()
  {
    return $this->morphMany('CiaTruks\Models\Imagem', 'com_imagem');
  }

  public function videos()
  {
    return $this->morphMany('CiaTruks\Models\Video', 'com_video');
  }

  public function comentariosImprensa()
  {
    return $this->hasMany('CiaTruks\Models\ComentarioImprensa', 'espetaculos_id')
                ->orderBy('data', 'desc');
  }

  public function agendamentos()
  {
    return $this->hasMany('CiaTruks\Models\Agenda', 'espetaculos_id')
                ->orderBy('data', 'desc');;
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug', '=', $slug)->first();
  }
}
