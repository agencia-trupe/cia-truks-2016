<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class Imagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'imagens';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'com_imagem_id',
    'com_imagem_type',
    'imagem',
    'legenda',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function com_imagem()
  {
    return $this->morphTo();
  }

  public function scopeDestaques($query)
  {
    return $query->distinct('com_imagem_id')->orderBy('created_at', 'desc')->take(3);
  }
}
