<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class GaleriaAdicional extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'galerias_adicionais';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'slug',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function imagens()
  {
    return $this->morphMany('CiaTruks\Models\Imagem', 'com_imagem');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('titulo', 'asc');
  }
}
