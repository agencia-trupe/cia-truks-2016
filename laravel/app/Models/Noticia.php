<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class Noticia extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'noticias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'data',
    'titulo',
    'olho',
    'texto'
  ];

  protected $dates = ['data', 'created_at', 'updated_at'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataAttribute($value)
  {
    $data = Carbon::createFromFormat('d/m/Y', $value);
    $this->attributes['data'] = $data->format('Y-m-d');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug', '=', $slug)->first();
  }

}
