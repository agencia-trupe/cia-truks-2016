<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'videos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'com_video_id',
    'com_video_type',
    'url',
    'video_id',
    'titulo',
    'thumb',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function com_video()
  {
    return $this->morphTo();
  }

  public function scopeAdicionais($query)
  {
    return $query->where('com_video_type', '');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeDestaques($query)
  {
    return $query->orderBy('created_at', 'desc')->take(3);
  }

}
