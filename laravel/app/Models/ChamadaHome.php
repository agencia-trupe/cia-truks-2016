<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class ChamadaHome extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'chamadas_home';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'texto',
    'link',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }
}
