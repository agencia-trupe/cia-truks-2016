<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class ImagemHome extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'imagem_home';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'imagem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

}
