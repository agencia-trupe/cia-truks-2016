<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class Agenda extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'agenda';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'espetaculos_id',
    'data',
    'horarios',
    'texto'
  ];

  protected $dates = ['data', 'created_at', 'updated_at'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataAttribute($value)
  {
    $data = Carbon::createFromFormat('d/m/Y', $value);
    $this->attributes['data'] = $data->format('Y-m-d');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeProximoPrimeiro($query)
  {
    return $query->orderBy('data', 'asc');
  }

  public function scopeDatasFuturas($query)
  {
    return $query->where('data', '>=', date('Y-m-d'));
  }

  public function scopeDatasPassadas($query)
  {
    return $query->where('data', '<', date('Y-m-d'));
  }

  public function espetaculo()
  {
    return $this->belongsTo('CiaTruks\Models\Espetaculo', 'espetaculos_id');
  }
}
