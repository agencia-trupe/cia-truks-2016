<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class PaginaTexto extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'paginas_texto';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'slug',
    'texto'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function imagens()
  {
    return $this->morphMany('CiaTruks\Models\Imagem', 'com_imagem');
  }

  public function videos()
  {
    return $this->morphMany('CiaTruks\Models\Video', 'com_video');
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug', $slug)->first();
  }
}
