<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class TextoIngles extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'texto_ingles';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'texto'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

}
