<?php

namespace CiaTruks\Models;

use Illuminate\Database\Eloquent\Model;

class CursosEOficinas extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos_e_oficinas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'slug',
    'texto',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug', '=', $slug)->first();
  }

}
