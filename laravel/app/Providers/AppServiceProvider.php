<?php

namespace CiaTruks\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

use CiaTruks\Models\FaleConosco;
use CiaTruks\Models\Espetaculo;
use CiaTruks\Models\CursosEOficinas;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->share('faleconosco', FaleConosco::first());
      view()->share('espetaculos', Espetaculo::all());
      view()->share('cursos_e_oficinas', CursosEOficinas::ordenado()->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      Relation::morphMap([
        \CiaTruks\Models\PaginaTexto::class,
        \CiaTruks\Models\Espetaculo::class,
        \CiaTruks\Models\GaleriaAdicional::class
      ]);

      $this->app->bind('path.public', function() {
        return base_path().'/../public_html';
      });

      setlocale(LC_ALL, 'pt_BR.utf8');
      date_default_timezone_set('America/Sao_Paulo');
    }
}
