$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

jQuery(function($){

  $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };

  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function busca(termo){
  $("table").find("tr").each(function(index) {
    if (!index) return;
    var id = $(this).find("td.celula-nome").first().text().toLowerCase();
    $(this).toggle(id.indexOf(termo.toLowerCase()) !== -1);
  });
}

function testarUrlThumb(url){
  if (url != undefined || url != '') {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
      return true;
    }
    else {
      return false;
    }
  }else{
    return false;
  }
}

function bindTituloEditavel(){
  $('.titulo-video-nopost').editable();

  $('.titulo-video-nopost').on('save', function(e, params) {
    $(this).parent().parent().find('.video-titulo').val(params.newValue);
  });
}

$('document').ready( function(){

  // Botão de excluir registro
  $('.btn-delete').click( function(e){
  	e.preventDefault();
  	var form = $(this).closest('form');
  	bootbox.confirm('Deseja Excluir o Registro?', function(result){
    	if(result)
      	form.submit();
    	else
      	$(this).modal('hide');
  	});
	});

  // Ordenação na tabela
  $('table.table-sortable tbody').sortable({
    update : function () {
      serial = [];
      tabela = $('table.table-sortable').attr('data-tabela');
      $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
        serial.push(elm.id.split('_')[1])
      });
      $.post('painel/gravar-ordem-registros', {
        data : serial,
        tabela : tabela
      });
    },
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    handle : $('.btn-move')
  }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

  if($('textarea').length){

    $('textarea').not('.textarea-simples').ckeditor({
      customConfig: '/novo/assets/js/ckeditor_config.js'
    });

    CKEDITOR.stylesSet.add( 'estilos', [
      { name: 'Título', element: 'h1', styles: { 'color': '#F58634', 'font-family': 'bankir-retroregular', 'font-size': '24px' } },
      { name: 'Sub-Título', element: 'h2', styles: { 'color': '#F58634', 'font-family': "'Verdana', sans-serif", 'font-size': '18px', 'font-weight': 'bold' } },
    ]);

  }

  $('.btn-toggle-comentario-aprovado').click( function(){
    var botao = $(this);
    var icone = botao.find('span');

    $.post('painel/mensagens/toggle-aprovacao', {
      id : botao.attr('data-id')
    }, function(resposta){
      if(resposta == 1){
        botao.addClass('btn-success').removeClass('btn-danger');
        icone.addClass('glyphicon-ok').removeClass('glyphicon-remove');
      }else if(resposta == 0){
        botao.removeClass('btn-success').addClass('btn-danger');
        icone.removeClass('glyphicon-ok').addClass('glyphicon-remove');
      }
    });
  });

  $('.texto-expansivel').readmore({
    collapsedHeight: 18,
    moreLink: '<a href="#">Ver mais</a>',
    lessLink: '<a href="#">Fechar</a>'
  });

  $('#adicionarVideo').click( function(){
    var botao = $(this);
    var input = $('#input-video');

    if(input.val() == '' || !testarUrlThumb(input.val())){
      input.parent().addClass('has-error').addClass('has-feedback');
      input.on('focus', function(){
        input.parent().removeClass('has-error').removeClass('has-feedback');
      });
      return false;
    }

    botao.html("<img src='assets/img/layout/ajax-loader.gif'>");

    // requisição ajax pra pegar titulo e gravar thumb do vídeo
    $.post('painel/videos/consultar', {
      url : input.val()
    }, function(resposta){

      var item_video = "";
      item_video += "<div class='list-group-item projetoVideo'>";
      item_video += "<a href='#' class='btn btn-sm btn-danger btn-remover pull-right' title='remover o vídeo'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover vídeo</strong></a>";
        item_video += "<h4 class='list-group-item-heading'>";
         item_video += "<a href='#' class='titulo-video-nopost' data-type='text' data-title='Alterar título' title='Alterar título'>" + resposta.titulo + "</a"
        item_video += "</h4>";
        item_video += "<p class='list-group-item-text'>";
          item_video += "<img class='img-thumbnail' style='max-width: 60px; margin-right: 15px' src='assets/img/videos/thumbs/" + resposta.thumb + "'>";
          item_video += "<a href='" + resposta.url + "' target='_blank'>" + resposta.url + "</a>";
        item_video += "</p>";
        item_video += "<input type='hidden' name='video_id[]' value=\"" + resposta.video_id + "\">";
        item_video += "<input type='hidden' name='video_url[]' value=\"" + resposta.url + "\">";
        item_video += "<input type='hidden' name='video_titulo[]' class='video-titulo' value=\"" + resposta.titulo + "\">";
        item_video += "<input type='hidden' name='video_thumb[]' value=\"" + resposta.thumb + "\">";
      item_video += "</div>";

      $('#listaVideos').append(item_video);
      $('#listaVideos').sortable("refresh");

      input.val('');

      botao.html("<span class='glyphicon glyphicon-circle-arrow-right'></span>");

      bindTituloEditavel();

    });

  });

  $('#listaVideos').sortable().disableSelection();

  $(document).on('click', '.projetoVideo a.btn-remover', function(e){
    e.preventDefault();
    var parent = $(this).parent();
    parent.css('opacity', .35);
    setTimeout( function(){
        parent.remove();
    }, 350);
  });

  $('.titulo-video').editable();

  bindTituloEditavel();

});
