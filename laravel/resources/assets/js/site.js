$('document').ready( function(){

  $('#menu-espetaculos ul li a').hover( function(){
      var imagem = $(this).attr('data-capa');
      $('#capas-espetaculos').html("<img src='assets/img/espetaculos/capas/"+imagem+"'>");
  });

  $('.link-topo').click( function(e){
    e.preventDefault();
    $('body, html').animate({
      scrollTop: 0
    }, 300);
  });

  $('.fancybox').fancybox();

  $('.fancybox-youtube').fancybox({
    type : 'iframe'
  });

  $('#menu-toggle').click(function(e){
    e.preventDefault();
    $('nav').toggleClass('fechado');
  });

  $('html').click(function() {
    if(!$('nav').hasClass('fechado'))
      $('nav').addClass('fechado');
  });

  $('nav ul > li a, #menu-toggle').click(function(event){
    event.stopPropagation();
  });

});
