@extends('site.template')

@section('conteudo')

  <div id="coluna-texto">

    <h1>Notícias</h1>

    <div class="noticia-topo">
      <img src="assets/img/noticias/{{$noticia->imagem}}" alt="{{$noticia->titulo}}">
      <div class="info">
        <div class="data">{{$noticia->data->formatLocalized("%d %b %Y")}}</div>
        <p class="titulo">{{$noticia->titulo}}</p>
        <p class="olho">{{$noticia->olho}}</p>
      </div>
    </div>

    <div class="cke">
      {!! $noticia->texto !!}
    </div>

    <div class="contem-links-noticias">
      @if($noticia_anterior)
        <a href="noticias/{{$noticia_anterior->slug}}" title="ver notícia anterior"><img src="assets/img/layout/seta-laranja-abaixo-esquerda.png"> ver notícia anterior</a>
      @endif
      <a href="#" class="link-topo"><img src="assets/img/layout/seta-laranja-acima-esquerda.png"> topo</a>
      @if($proxima_noticia)
        <a href="noticias/{{$proxima_noticia->slug}}" title="ver próxima notícia"><img src="assets/img/layout/seta-laranja-acima-direita.png"> ver próxima notícia</a>
      @endif
    </div>

  </div>

  <div id="coluna-galerias">

    @if(sizeof($mais_noticias))

      <h3>mais notícias</h3>

      <div class="lista-mais-noticias">
        @foreach($mais_noticias as $noticia_relacionada)
          <a href="noticias/{{$noticia_relacionada->slug}}">
            <div class="data">{{$noticia_relacionada->data->format('d/m/Y')}}</div>
            <div class="titulo">{{$noticia_relacionada->titulo}}</div>
          </a>
        @endforeach
      </div>
    @endif

  </div>

@stop
