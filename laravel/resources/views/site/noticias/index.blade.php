@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>notícias</h1>

    <p class="destaque">Acompanhe aqui todas as nossas novidades!</p>

    <div class="lista-noticias-internas">
      @foreach($noticias as $noticia)
        <div class="noticia">
          <a href="noticias/{{$noticia->slug}}" title="{{$noticia->titulo}}">
            <img src="assets/img/noticias/{{$noticia->imagem}}" alt="{{$noticia->titulo}}">
            <div class="info">
              <div class="data">{{$noticia->data->formatLocalized("%d %b %Y")}}</div>
              <p class="titulo">{{$noticia->titulo}}</p>
              <p class="olho">{{$noticia->olho}}</p>
            </div>
          </a>
        </div>
      @endforeach
    </div>

  </div>

@stop
