@extends('site.template')

@section('conteudo')

  <div id="coluna-texto">

    <h1>{{$texto->titulo}}</h1>

    <div class="cke">
      {!! $texto->texto !!}
    </div>

    @if(count($texto->imagens) > 0)
      <div class="desktop-hidden">
        <h3>fotos</h3>
        <div class="lista-imagens">
          @foreach($texto->imagens as $img)
            <a href="assets/img/paginas-texto/redimensionadas/{{$img->imagem}}" class="fancybox" rel="galeria">
              <img src="assets/img/paginas-texto/thumbs/{{$img->imagem}}">
            </a>
          @endforeach
        </div>
      </div>
    @endif

    @if(count($texto->videos) > 0)
      <div class="desktop-hidden">
        <h3>vídeos</h3>
        <div class="lista-videos">
          @foreach($texto->videos as $vid)
            <a href="{{ youtube_embed_link($vid->video_id) }}" class="fancybox-youtube" rel="galeria">
              <img src="assets/img/videos/thumbs/{{$vid->thumb}}">
            </a>
          @endforeach
        </div>
      </div>
    @endif

    @if(str_is('cursos-e-oficinas', Route::currentRouteName()))

      <h2>Conheça aqui os planejamentos básicos das oficinas:</h2>

      @foreach($cursos_e_oficinas as $curso)
        <a class="link-oficina" href="cursos-e-oficinas/{{$curso->slug}}" title="{{$curso->titulo}}">&raquo; {{$curso->titulo}}</a>
      @endforeach

    @else

      <div class="contem-link-topo">
        <a href="#" class="link-topo"><img src="assets/img/layout/seta-laranja-acima-esquerda.png"> topo</a>
      </div>

    @endif
  </div>

  <div id="coluna-galerias">

    @if(count($texto->imagens) > 0)
      <h3>fotos</h3>
      <div class="lista-imagens">
        @foreach($texto->imagens as $img)
          <a href="assets/img/paginas-texto/redimensionadas/{{$img->imagem}}" class="fancybox" rel="galeria">
            <img src="assets/img/paginas-texto/thumbs/{{$img->imagem}}">
          </a>
        @endforeach
      </div>
    @endif

    @if(count($texto->videos) > 0)
      <h3>vídeos</h3>
      <div class="lista-videos">
        @foreach($texto->videos as $vid)
          <a href="{{ youtube_embed_link($vid->video_id) }}" class="fancybox-youtube" rel="galeria">
            <img src="assets/img/videos/thumbs/{{$vid->thumb}}">
          </a>
        @endforeach
      </div>
    @endif    

  </div>

@stop
