@extends('site.template')

@section('conteudo')

  <div id="coluna-texto">

    <h1>{{$texto->titulo}}</h1>

    <div class="cke">
      {!! $texto->texto !!}
    </div>

    <div class="contem-link-topo">
      <a href="cursos-e-oficinas" title="Voltar para Cursos e Oficinas"><img src="assets/img/layout/seta-laranja-abaixo-esquerda.png"> voltar para Cursos e Oficinas</a>
      <a href="#" class="link-topo"><img src="assets/img/layout/seta-laranja-acima-esquerda.png"> topo</a>
    </div>

  </div>

  <div id="coluna-galerias"></div>

@stop
