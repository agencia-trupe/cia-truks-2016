<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2016 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta name="keywords" content="" />

	<title>Cia Truks</title>
	<meta name="description" content="">
	<meta property="og:title" content="Cia Truks"/>
	<meta property="og:description" content=""/>

  <meta property="og:site_name" content=""/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link rel="stylesheet" href="assets/css/vendor.css">

	<link rel="stylesheet" href="assets/css/site.css?cachebuster={{ time() }}">

	<script src="assets/js/jquery.js"></script>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->

</head>
<body>

  <div class="conteudo @if(!str_is('home', Route::currentRouteName())) internas @endif ">

    @include('site.partials.menu')

  	@yield('conteudo')

  </div>

  <footer>
		<div class="centralizar">
	    <ul>
	      <li><a href="/" title="home">&raquo; home</a></li>
	      <li><a href="a-companhia" title="a companhia">&raquo; a companhia</a></li>
	    </ul>
	    <ul>
	      <li><a href="espetaculos" title="espetáculos">&raquo; espetáculos</a></li>
				@foreach($espetaculos as $k => $espetaculo)
					@if($k > 0 && $k%8 == 0)
						</ul><ul>
					@endif
					<li><a href="espetaculos/{{$espetaculo->slug}}" title="{{$espetaculo->titulo}}" class="link-espetaculo">{{$espetaculo->titulo}}</a></li>
				@endforeach
	    </ul>
	    <ul>
	      <li><a href="cursos-oficinas" title="cursos e oficinas">&raquo; cursos e oficinas</a></li>
	      <li><a href="eventos" title="eventos">&raquo; eventos</a></li>
	      <li><a href="centro-de-estudos" title="centro de estudos">&raquo; centro de estudos</a></li>
	      <li><a href="contacao-de-historias" title="contação de histórias">&raquo; contação de histórias</a></li>
	      <li><a href="contratacoes" title="contratações">&raquo; contratações</a></li>
	      <li><a href="galeria-de-fotos" title="galeria de fotos">&raquo; galeria de fotos</a></li>
	      <li><a href="videos" title="vídeos">&raquo; vídeos</a></li>
	      <li><a href="noticias" title="notícias">&raquo; notícias</a></li>
	      <li><a href="criticas-e-materias" title="críticas e matérias">&raquo; críticas e matérias</a></li>
	    </ul>
	    <ul class="coluna-contato">
	      <li><a href="fale-conosco" title="fale conosco">&raquo; fale conosco</a></li>
	      <li>
	        <div class="contato">
	          {!! $faleconosco->texto !!}
						<a href="mailto:{{$faleconosco->email}}">{{$faleconosco->email}}</a>
	        </div>
	        <div class="assinatura">
	          &copy; {{Date('Y')}} Cia Truks &bull; Todos os direitos reservados
	          <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Criação de Sites: Trupe Agência Criativa <img src="assets/img/layout/icone-trupe.png" alt="Criação de Sites: Trupe Agência Criativa"></a>
	        </div>
					<a href="#" class="link-topo"><img src="assets/img/layout/seta-laranja-acima-esquerda.png" > topo</a>
	      </li>
	    </ul>
		</div>
  </footer>

	<script src="assets/js/site.js?cachebuster={{ time() }}"></script>

</body>
</html>
