@extends('site.template')

@section('conteudo')

  <div id="imagem-home" style="background-image:url('/novo/assets/img/imagem-home/{{$imagemHome->imagem}}')">
    <a href="a-companhia" title="Saiba mais sobre a companhia">Saiba mais sobre a companhia</a>
  </div>

  <div id="espacamento-home-medium"></div>

  <div id="menu-espetaculos">
    <h1>espetáculos</h1>
    <ul>
      @foreach ($espetaculos as $k => $espetaculo)
        <li>
          <a href="espetaculos/{{$espetaculo->slug}}" title="{{$espetaculo->titulo}}" data-capa="{{$espetaculo->imagem}}">{{$espetaculo->titulo}}</a>
        </li>
      @endforeach
    </ul>
    <div id="capas-espetaculos"></div>
  </div>

  <div id="destaques-e-midia">

    <div class="lista-destaques">
      <h1>destaques</h1>
      @foreach($destaques as $destaque)
        <a href="{{$destaque->link}}" class="destaque">
          <strong>{{$destaque->titulo}}</strong>
          <p>{{$destaque->texto}}</p>
          <div class="ver-mais">ver mais ></div>
        </a>
      @endforeach
    </div>

    <div class="lista-videos">
      <h1>
        <span>vídeos</span>
        <a href="videos" title="ver todos os vídeos">ver todos os vídeos <img src="assets/img/layout/seta-preta-acima-direita.png"/></a>
      </h1>
      @foreach($videos as $video)
        <a href="videos/{{str_slug($video->id.'-'.$video->titulo)}}" title="{{$video->titulo}}" class="video">
          <img src="assets/img/videos/thumbs/{{$video->thumb}}" alt="{{$video->titulo}}">
          {{$video->titulo}}
        </a>
      @endforeach
    </div>

    <div class="lista-galerias">
      <h1>
        <span>galeria</span>
        <a href="galeria-de-fotos" title="ver galeria completa">ver galeria completa <img src="assets/img/layout/seta-preta-acima-direita.png"/></a>
      </h1>
      @foreach($galerias as $img)
        <a href="galeria-de-fotos" title="Galeria de Fotos" class="imagem">
          @if($img->com_imagem_type == 'espetaculos')
            <img src="assets/img/espetaculos/thumbs/{{$img->imagem}}" alt="">
          @elseif($img->com_imagem_type == 'paginas_texto')
            <img src="assets/img/paginas-texto/thumbs/{{$img->imagem}}" alt="">
          @elseif($img->com_imagem_type == 'galerias_adicionais')
            <img src="assets/img/galerias-adicionais/thumbs/{{$img->imagem}}" alt="">
          @endif
          {{$img->com_imagem->titulo}}
        </a>
      @endforeach
    </div>
  </div>

  <div id="agenda">
    <div class="fundo-agenda">
      <h1>agenda</h1>
      <p>
        Acompanhe aqui todas as nossas apresentações e compareça!
      </p>
      <ul>
        @forelse($agendas as $agenda)
          <li class="agenda">
            <div class="data">
              <div class="dia">{{$agenda->data->format('d')}}</div>
              <div class="mes">{{$agenda->data->formatLocalized('%b')}}</div>
              <div class="ano">{{$agenda->data->format('Y')}}</div>
            </div>
            <div class="info">
              <div class="horarios">{{$agenda->horarios}}</div>
              <div class="texto">{!! nl2br($agenda->texto) !!}</div>
              <div class="espetaculo"><span>espetáculo:</span> <strong>{{$agenda->espetaculo->titulo}}</strong></div>
            </div>
          </li>
        @empty
          <li class="vazio">
            Não há eventos agendados
          </li>
        @endforelse
      </ul>
      <div class="link-agenda">
        <div class="disclaimer">
          Listamos apenas os eventos abertos ao público, excluindo apresentações fechadas para escolas, clubes, particulares e afins.
        </div>
        <a href="" title="ver agenda completa">ver agenda completa <img src="assets/img/layout/seta-branca-acima-direita.png"></a>
      </div>
      <a href='fale-conosco' title="FALE CONOSCO" class="link-contato">
        <p class="link-contato-titulo">FALE CONOSCO</p>
        <p class="link-contato-subtitulo">ENVIE-NOS UMA MENSAGEM ELETRÔNICA OU LIGUE!</p>
      </a>
    </div>
  </div>

  <div id="noticias-e-chamadas">

    <h1>
      <span>notícias</span>
      <a href="noticias" title="ver todas notícias">ver todas notícias <img src="assets/img/layout/seta-preta-acima-direita.png"></a>
    </h1>

    <div class="lista-noticias">
      @foreach($noticias as $noticia)
        <a href="noticias/{{$noticia->slug}}" title="{{$noticia->titulo}}">
          <div class="titulo">
            <img src="assets/img/noticias/{{$noticia->imagem}}" alt="">
            <h2>
              {{$noticia->titulo}}
            </h2>
            <p class="olho-mobile">{{$noticia->olho}}</p>
          </div>
          <div class="olho">{{$noticia->olho}}</div>
          <div class="ver-mais">ver mais <img src="assets/img/layout/seta-laranja-acima-direita.png"></div>
        </a>
      @endforeach
    </div>

    <div class="lista-chamadas">
      @foreach($chamadas as $chamada)
        <div class="chamada">
          <a href="" title="{{$chamada->titulo}}">
            <div class="titulo">{{$chamada->titulo}}</div>
            <div class="olho">{{$chamada->texto}}</div>
            <div class="ver-mais">ver mais ></div>
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div id="mensagens">
    <div class="form">
      <h1>escreva o seu depoimento sobre a Cia Truks</h1>
      @if(session('mensagem-enviada'))
        <div id="mensagem-enviada">Sua mensagem foi enviada com sucesso!</div>
        <script>alert('Sua mensagem foi enviada com sucesso!')</script>
      @endif
      <form action="{{route('enviar-mensagem')}}" method="post">
        {{csrf_field()}}
        <input type="text" name="mensagem[nome]" placeholder="nome" required>
        <input type="email" name="mensagem[email]" placeholder="e-mail" required>
        <textarea name="mensagem[texto]" placeholder="depoimento"></textarea>
        <input type="submit" value="ENVIAR">
      </form>
      <a href="" title="ver todos os comentários">
        <img src="assets/img/layout/ver-todos-comentarios.png" alt="ver todos os comentários">
      </a>
    </div>
    <div class="lista-mensagens">
      <h1>Depoimentos:</h1>
      <ul>
        @foreach($mensagens as $msg)
          <li>
            <p>
              {{$msg->texto}}
            </p>
            <div class="assinatura">{{ $msg->nome }} - {{ $msg->created_at->format('d/m/Y') }}</div>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
@stop
