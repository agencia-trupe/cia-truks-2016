@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>Galeria de Fotos</h1>

    <div class="lista-galerias">

      @foreach($albuns as $secoes => $registros)
        @foreach($registros as $i => $album)

          <div class="galeria">

            @foreach($album->imagens as $k => $img)
              @if($k == 0)
                <a href="assets/img/{{$secoes}}/redimensionadas/{{$img->imagem}}" title="{{$album->titulo}}" class="fancybox" rel="galeria-{{$secoes.$i}}">
                  <img src="assets/img/{{$secoes}}/thumbs/{{$img->imagem}}" alt="{{$album->titulo}}">
                  <p>{{$album->titulo}}</p>
                </a>
              @else
                <a href="assets/img/{{$secoes}}/redimensionadas/{{$img->imagem}}" title="{{$album->titulo}}" style="display:none;" class="fancybox" rel="galeria-{{$secoes.$i}}"></a>
              @endif
            @endforeach

          </div>

        @endforeach
      @endforeach

    </div>

  </div>

@stop
