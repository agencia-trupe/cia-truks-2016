<nav class="fechado @if(!str_is('home', Route::currentRouteName())) internas @endif ">

  <a href="{{route('home')}}" id="link-home">
    <img src="assets/img/layout/marca-ciatruks.png" alt="Cia Truks">
  </a>

  <ul>
    <li>
      <a href="/" title="Página Inicial" class="menu-item-home @if(str_is('home', Route::currentRouteName())) ativo @endif">HOME</a>
    </li>
    <li>
      <a href="a-companhia" title="A Companhia" class="menu-item-a-companhia @if(str_is('a-companhia', Route::currentRouteName())) ativo @endif">A COMPANHIA</a>
    </li>
    <li>
      <a href="agenda" title="Agenda" class="menu-item-agenda @if(str_is('agenda', Route::currentRouteName())) ativo @endif">AGENDA</a>
    </li>
    <li>
      <a href="espetaculos" title="Espetáculos" class="menu-item-espetaculos @if(str_is('espetaculos*', Route::currentRouteName())) ativo @endif">ESPETÁCULOS</a>
    </li>
    <li>
      <a href="cursos-e-oficinas" title="Cursos e Oficinas" class="menu-item-cursos-e-oficinas @if(str_is('cursos-e-oficinas*', Route::currentRouteName())) ativo @endif">CURSOS E OFICINAS</a>
    </li>
    <li>
      <a href="eventos" title="Eventos" class="menu-item-eventos @if(str_is('eventos', Route::currentRouteName())) ativo @endif">EVENTOS</a>
    </li>
    <li>
      <a href="centro-de-estudos" title="Centro de Estudos" class="menu-item-centro-de-estudos @if(str_is('centro-de-estudos', Route::currentRouteName())) ativo @endif">CENTRO DE ESTUDOS</a>
    </li>
    <li>
      <a href="contacao-de-historias" title="Contação de Histórias" class="menu-item-contacao-de-historias @if(str_is('contacao-de-historias', Route::currentRouteName())) ativo @endif">CONTAÇÃO DE HISTÓRIAS</a>
    </li>
    <li>
      <a href="contratacoes" title="Contratações" class="menu-item-contratacoes @if(str_is('contratacoes', Route::currentRouteName())) ativo @endif">CONTRATAÇÕES</a>
    </li>
    <li>
      <a href="galeria-de-fotos" title="Galeria de Fotos" class="menu-item-galeria-de-fotos @if(str_is('galeria-de-fotos', Route::currentRouteName())) ativo @endif">GALERIA DE FOTOS</a>
    </li>
    <li>
      <a href="videos" title="Vídeos" class="menu-item-videos @if(str_is('videos*', Route::currentRouteName())) ativo @endif">VÍDEOS</a>
    </li>
    <li>
      <a href="noticias" title="Notícias" class="menu-item-noticias @if(str_is('noticias*', Route::currentRouteName())) ativo @endif">NOTÍCIAS</a>
    </li>
    <li>
      <a href="criticas-e-materias" title="Críticas e Matérias" class="menu-item-criticas-e-materias @if(str_is('criticas-e-materias', Route::currentRouteName())) ativo @endif">CRÍTICAS E MATÉRIAS</a>
    </li>
    <li>
      <a href="fale-conosco" title="Fale Conosco" class="menu-item-fale-conosco @if(str_is('fale-conosco', Route::currentRouteName())) ativo @endif">FALE CONOSCO</a>
    </li>
    </li>
  </ul>

  <div class="links-idiomas">
    <a href="texto-ingles" title="Inglês"><img src="assets/img/layout/idioma-ingles.png" alt="Inglês"></a>
    <a href="texto-espanhol" title="Espanhol"><img src="assets/img/layout/idioma-espanhol.png" alt="Espanhol"></a>
  </div>

</nav>

<div id="controle-menu-mobile">
  <a href="/" class="link-home-mobile">
    <img src="assets/img/layout/marca-ciatruks.png" alt="Cia Truks">
  </a>
  <div class="contato-e-controle">
    <small>Contato:</small>
    <p>
      {!! nl2br($faleconosco->texto) !!}
    </p>
    <a href="#" id="menu-toggle">> menu</a>
  </div>
</div>
