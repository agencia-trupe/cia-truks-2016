@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>Fale Conosco</h1>

    <div class="contato-info">
      <a href="mailto:{{$faleconosco->email}}">{{$faleconosco->email}}</a>
      {!! $faleconosco->texto !!}
    </div>

    <div class="contato-form">
      @if(session('contato-enviada'))
        <div id="contato-enviada">Seu contato foi enviado com sucesso!</div>
        <script>alert('Seu contato foi enviado com sucesso!')</script>
      @endif
      <form action="{{route('fale-conosco.enviar')}}" method="post">
        {{csrf_field()}}
        <input type="text" name="faleconosco[nome]" placeholder="nome" required>
        <input type="email" name="faleconosco[email]" placeholder="e-mail" required>
        <input type="text" name="faleconosco[telefone]" placeholder="telefone" required>
        <textarea name="faleconosco[mensagem]" placeholder="mensagem"></textarea>
        <input type="submit" value="ENVIAR">
      </form>
    </div>

  </div>

@stop
