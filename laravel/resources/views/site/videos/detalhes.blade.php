@extends('site.template')

@section('conteudo')

  <div id="coluna-videos">

    <h1>Vídeos</h1>

    <h2>{{$video->titulo}}</h2>

    {!! $youtube->getEmbed(730) !!}

    <a href="videos" title="Voltar para vídeos" class="btn-voltar"><img src="assets/img/layout/seta-laranja-abaixo-esquerda.png"> voltar para vídeos</a>
  </div>

@stop
