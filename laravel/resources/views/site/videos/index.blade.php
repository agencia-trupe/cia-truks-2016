@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>Vídeos</h1>

    <div class="lista-videos">
      @foreach($videos as $video)
        <div class="video">
          <a href="videos/{{str_slug($video->id.'-'.$video->titulo)}}" title="{{$video->titulo}}">
            <img src="assets/img/videos/thumbs/{{$video->thumb}}" alt="{{$video->titulo}}">
            <p>{{$video->titulo}}</p>
          </a>
        </div>
      @endforeach
    </div>

  </div>

@stop
