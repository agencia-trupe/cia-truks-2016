@extends('site.template')

@section('conteudo')

  <div id="coluna-texto" class="espetaculos-internas-detalhes">

    <h1>nossos espetáculos</h1>

    @if($espetaculo->imagem && file_exists("assets/img/espetaculos/capas/{$espetaculo->imagem}"))
      <div class="capa-espetaculo">
        <img src="assets/img/espetaculos/capas/{{$espetaculo->imagem}}">
      </div>
    @endif

    <div class="cke">
      {!! $espetaculo->texto !!}
    </div>

    <div class="imprensa-comenta">
      <h1>Imprensa comenta</h1>
      @foreach($espetaculo->comentariosImprensa as $msg)
        <div class="comentario-imprensa">
          <p>
            {!! $msg->texto !!}
          </p>
          <p class="autor">
            {{$msg->autor}} - {{$msg->data->format('d/m/Y')}}
          </p>
        </div>
      @endforeach
    </div>

    <div class="contem-link-topo">
      <a href="espetaculos" title="Voltar para Espetáculos"><img src="assets/img/layout/seta-laranja-abaixo-esquerda.png"> voltar para Espetáculos</a>
      <a href="#" class="link-topo"><img src="assets/img/layout/seta-laranja-acima-esquerda.png"> topo</a>
    </div>

  </div>

  <div id="coluna-galerias">

    @if(count($espetaculo->imagens) > 0)
      <h3>fotos</h3>
      <div class="lista-imagens">
        @foreach($espetaculo->imagens as $img)
          <a href="assets/img/espetaculos/redimensionadas/{{$img->imagem}}" class="fancybox" rel="galeria">
            <img src="assets/img/espetaculos/thumbs/{{$img->imagem}}">
          </a>
        @endforeach
      </div>
    @endif

    @if(count($espetaculo->videos) > 0)
      <h3>vídeos</h3>
      <div class="lista-videos">
        @foreach($espetaculo->videos as $vid)
          <a href="{{ youtube_embed_link($vid->video_id) }}" class="fancybox-youtube" rel="galeria">
            <img src="assets/img/videos/thumbs/{{$vid->thumb}}">
          </a>
        @endforeach
      </div>
    @endif

    <div id="texto-ficha-tecnica">
      <h4>Ficha Técnica do Espetáculo</h4>
      <p>
        {!! nl2br($espetaculo->ficha_tecnica) !!}
      </p>
    </div>

  </div>

@stop
