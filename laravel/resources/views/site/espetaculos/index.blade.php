@extends('site.template')

@section('conteudo')

  <div class="espetaculos-internas">

    <div class="coluna-esquerda-espetaculos">
      <p>
        selecione o <br>espetáculo <br>para ver mais <br>detalhes<br>
        <img src="assets/img/layout/seta-laranja-abaixo-direita.png">
      </p>
    </div>

    <div id="menu-espetaculos">
      <h1>nossos espetáculos</h1>
      <ul>
        @foreach ($espetaculos as $espetaculo)
          <li>
            <a href="espetaculos/{{$espetaculo->slug}}" title="{{$espetaculo->titulo}}" data-capa="{{$espetaculo->imagem}}">{{$espetaculo->titulo}}</a>
          </li>
        @endforeach
      </ul>
      <div id="capas-espetaculos"></div>
    </div>

    <div class="coluna-direita-espetaculos">
      <div class="link-agenda">
        <h2>Agenda de Espetáculos</h2>
        <p>
          A Cia. Truks realiza espetáculos regularmente.<br>
          Esperamos por você!
        </p>
        <p class="destaque">
          Confira os espetáculos em cartaz e venha nos prestigiar!
        </p>
        <a href="agenda" title="ver agenda completa">ver agenda completa <img src="assets/img/layout/seta-branca-acima-direita.png"></a>
      </div>
    </div>

  </div>

@stop
