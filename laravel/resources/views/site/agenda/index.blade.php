@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>Agenda</h1>

    <p class="destaque">Acompanhe aqui todas as nossa apresentações e compareça!</p>

    <div class="lista-agenda-internas">
      @foreach($agendas as $agenda)
        <div class="item-agenda">

          <div class="data">
            <div class="dia">{{$agenda->data->format('d')}}</div>
            <div class="mes">{{$agenda->data->formatLocalized('%b')}}</div>
            <div class="ano">{{$agenda->data->format('Y')}}</div>
          </div>

          @if($agenda->espetaculo->imagem && file_exists("assets/img/espetaculos/capas/{$agenda->espetaculo->imagem}"))
            <div class="capa">
              <img src="assets/img/espetaculos/capas/{{$agenda->espetaculo->imagem}}" alt="{{$agenda->espetaculo->titulo}}">
            </div>
          @endif

          <div class="info">
            <div class="espetaculo"><span>espetáculo:</span> <strong>{{$agenda->espetaculo->titulo}}</strong></div>
            <div class="horarios">{{$agenda->horarios}}</div>
            <div class="texto">{!! nl2br($agenda->texto) !!}</div>
          </div>

        </div>
      @endforeach
    </div>

  </div>

@stop
