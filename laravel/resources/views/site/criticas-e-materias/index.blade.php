@extends('site.template')

@section('conteudo')

  <div id="coluna-agenda">

    <h1>Críticas e Matérias</h1>

    <div class="lista-criticas">
      @foreach($criticas as $critica)

        <div class="critica">
          <a href="assets/img/criticas-e-materias/ampliadas/{{$critica->imagem}}" title="{{$critica->data->formatLocalized("%B %Y")}}" class="fancybox">
            <img src="assets/img/criticas-e-materias/capas/{{$critica->imagem}}" alt="{{$critica->data->formatLocalized("%B %Y")}}">
            <p>{{$critica->data->formatLocalized("%B %Y")}}</p>
          </a>
        </div>

      @endforeach
    </div>

  </div>

@stop
