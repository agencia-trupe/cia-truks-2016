@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Galerias de Fotos Adicionais</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.galerias-adicionais.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Galeria de Fotos</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Título</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>{{$registro->titulo}}</td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.galerias-adicionais.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.galerias-adicionais.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
