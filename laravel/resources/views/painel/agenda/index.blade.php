@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Agenda</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.agenda.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Espetáculo na Agenda</a>

        <hr>

        <div class="btn-group">
          <a href="{{route('painel.agenda.index')}}" class="btn btn-sm btn-default @if($filtro == '') btn-info @endif">mostrar todos</a>
          <a href="{{route('painel.agenda.index', ['filtro' => 'futuras'])}}" class="btn btn-sm btn-default @if($filtro == 'futuras') btn-info @endif">mostrar somente eventos futuros</a>
          <a href="{{route('painel.agenda.index', ['filtro' => 'passadas'])}}" class="btn btn-sm btn-default @if($filtro == 'passadas') btn-info @endif">mostrar somente eventos passados</a>
        </div>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
              <th>Espetáculo</th>
          		<th>Data</th>
              <th>Horário</th>
              <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>{{$registro->espetaculo->titulo}}</td>
            		<td>{{$registro->data->format('d/m/Y')}}</td>
                <td>{{$registro->horarios}}</td>
                <td>{{ str_words(strip_tags($registro->texto), 15)  }}</td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.agenda.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.agenda.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
