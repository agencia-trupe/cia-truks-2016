@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Espetáculo na Agenda</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.agenda.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputEspetaculo">Espetáculo</label>
            <select name="espetaculos_id" id="inputEspetaculo" class="form-control" required>
              <option value=""></option>
              @foreach($espetaculos as $espetaculo)
                <option value="{{$espetaculo->id}}" @if($registro->espetaculos_id == $espetaculo->id) selected @endif >{{$espetaculo->titulo}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group" style="max-width:120px">
            <label for="inputData">Data</label>
            <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{$registro->data->format('d/m/Y')}}">
          </div>

          <div class="form-group">
            <label for="inputHorarios">Horários</label>
            <input type="text" name="horarios" class="form-control" id="inputHorarios" value="{{$registro->horarios}}">
          </div>

          <div class="form-group">
            <label for="inputTexto">Texto</label>
            <textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{$registro->texto}}</textarea>
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.agenda.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
