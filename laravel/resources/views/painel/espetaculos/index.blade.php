@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Espetáculos</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.espetaculos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Espetáculo</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Título</th>
              <th>Texto</th>
              <td><span class="glyphicon glyphicon-list-alt"></span></td>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>{{$registro->titulo}}</td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
                <td>
                  <a href="{{ route('painel.comentarios-da-imprensa.index', ['espetaculos_id' => $registro->id]) }}" class="btn btn-default btn-sm">comentários da imprensa</a>
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.espetaculos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.espetaculos.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
