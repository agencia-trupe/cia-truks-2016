@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Espetáculo</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.espetaculos.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

  			    <div class="form-group">
  						<label for="inputTexto">Texto</label>
  						<textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
  					</div>

            <div class="form-group">
  						<label for="inputFichaTecnica">Ficha Técnica</label>
  						<textarea name="ficha_tecnica" class="form-control textarea-simples" id="inputFichaTecnica">{{old('ficha_tecnica')}}</textarea>
  					</div>

  					<div class="form-group">
  						@if(old('imagem'))
  							Imagem de Capa atual<br>
  							<img src="assets/img/espetaculos/capas/{{old('imagem')}}"><br>
  						@endif
  						<label for="inputImagem">Imagem de Capa</label>
  						<input type="file" class="form-control" id="inputImagem" name="imagem">
  					</div>

            @include('painel.partials.upload-imagens')

            @include('painel.partials.form-videos')

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.espetaculos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
