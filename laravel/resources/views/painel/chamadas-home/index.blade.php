@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Chamadas da Home</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="chamadas_home">

          <thead>
          	<tr>
              <th>Ordenar</th>
          		<th>Título</th>
              <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td class="move-actions">
                  <a href='#' class='btn btn-info btn-move btn-sm'>mover</a>
                </td>
                <td>{{$registro->titulo}}</td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.chamadas-home.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
