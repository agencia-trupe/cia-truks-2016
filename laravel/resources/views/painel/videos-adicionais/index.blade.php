@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Vídeos Adicionais</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.videos-adicionais.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Vídeo</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
              <th>Thumbnail</th>
          		<th>Título</th>
              <th>Url</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td><img src="assets/img/videos/thumbs/{{$registro->thumb}}" style="max-width: 70px;"></td>
                <td>
                  <a href="#" class="titulo-video" data-type="text" data-pk="{{$registro->id}}" data-url="painel/videos/atualizar-titulo" data-title="Alterar título" title="Alterar título">{{$registro->titulo}}</a
                </td>
            		<td>
                  <a href="{{$registro->url}}" target="_blank">{{$registro->url}}</a>
                </td>
                <td class="crud-actions">
                  <form action="{{ URL::route('painel.videos-adicionais.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
