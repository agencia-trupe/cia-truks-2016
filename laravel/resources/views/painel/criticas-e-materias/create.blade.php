@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Crítica/Matéria</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.criticas-e-materias.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

  			    <div class="form-group" style="max-width:120px">
  						<label for="inputData">Data</label>
    					<input type="text" name="data" class="form-control datepicker" id="inputData" value="{{old('data')}}">
  					</div>

            <div class="form-group">
  						@if(old('imagem'))
  							Imagem Atual<br>
  							<img src="assets/img/criticas-e-materias/capas/{{old('imagem')}}"><br>
  						@endif
  						<label for="inputImagem">Imagem de Capa</label>
  						<input type="file" class="form-control" id="inputImagem" name="imagem">
  					</div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.criticas-e-materias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
