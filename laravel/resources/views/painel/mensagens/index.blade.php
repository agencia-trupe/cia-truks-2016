@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Depoimentos</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
              <th>Aprovado</th>
          		<th>Nome</th>
              <th>E-mail</th>
              <th>Data</th>
              <th>Mensagem</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  @if($registro->aprovado == 1)
                    <button type="button" class="btn btn-sm btn-success btn-toggle-comentario-aprovado" data-id="{{$registro->id}}"><span class="glyphicon glyphicon-ok"></span></button>
                  @else
                    <button type="button" class="btn btn-sm btn-danger btn-toggle-comentario-aprovado" data-id="{{$registro->id}}"><span class="glyphicon glyphicon-remove"></span></button>
                  @endif
                </td>
                <td>{{$registro->nome}}</td>
                <td>{{$registro->email}}</td>
                <td>{{$registro->created_at->format('d/m/Y')}}</td>
            		<td>
                  <div class="texto-expansivel">
                    {!! nl2br($registro->texto) !!}
                  </div>
                </td>
            		<td class="crud-actions visualizar">
              		<form action="{{ URL::route('painel.mensagens.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

        {!! $registros->render() !!}

      </div>
    </div>
  </div>

@endsection
