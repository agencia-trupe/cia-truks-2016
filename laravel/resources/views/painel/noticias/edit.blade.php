@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Notícia</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.noticias.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

          <div class="form-group" style="max-width:120px">
            <label for="inputData">Data</label>
            <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{$registro->data->format('d/m/Y')}}">
          </div>

          <div class="form-group">
            <label for="inputOlho">Olho</label>
            <textarea name="olho" class="form-control textarea-simples" id="inputOlho">{{$registro->olho}}</textarea>
          </div>

          <div class="form-group">
            <label for="inputTexto">Texto</label>
            <textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
          </div>

          <div class="form-group">
						@if($registro->imagem)
							Imagem Atual<br>
							<img src="assets/img/noticias/{{$registro->imagem}}"><br>
						@endif
						<label for="inputImagem">Trocar Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem">
					</div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.noticias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
