@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Notícias</h2>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{ URL::route('painel.noticias.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Notícia</a>

        <hr>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
              <th>Data</th>
              <th>Título</th>
              <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>{{$registro->data->format('d/m/Y')}}</td>
                <td>{{$registro->titulo}}</td>
                <td>{{ str_words(strip_tags($registro->texto), 15)  }}</td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.noticias.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.noticias.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
