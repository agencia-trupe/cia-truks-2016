@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Imagens</h2>

        <hr>

        <h3>Espetáculo: {{$espetaculo->titulo}}</h3>

        <hr>

      	@include('painel.partials.mensagens')

        <a href="{{route('painel.espetaculos.index')}}" class="btn btn-default btn-sm">&larr; voltar</a>

        <a href="{{ URL::route('painel.imagens.create', ['espetaculos_id' => $espetaculo->id]) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Comentário da Imprensa</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
              <th>Autor</th>
          		<th>Comentário</th>
              <th>Data</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($espetaculo->comentariosImprensa as $registro)

            	<tr class="tr-row">
                <td>{{$registro->autor}}</td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
                <td>{{$registro->data->format('d/m/Y')}}</td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.imagens.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.imagens.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
