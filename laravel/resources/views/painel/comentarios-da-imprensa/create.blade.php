@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Adicionar Comentário da Imprensa</h2>

          <hr>

          <h3>Espetáculo: {{$espetaculo->titulo}}</h3>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.comentarios-da-imprensa.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <input type="hidden" name="espetaculos_id" value="{{$espetaculo->id}}">

            <div class="form-group">
  						<label for="inputAutor">Autor</label>
  						<input type="text" name="autor" class="form-control" id="inputAutor" value="{{old('autor')}}">
  					</div>

            <div class="form-group" style="max-width:120px">
  						<label for="inputData">Data</label>
  						<input type="text" name="data" class="form-control datepicker" id="inputData" value="{{old('data')}}">
  					</div>

            <div class="form-group">
  						<label for="inputTexto">Texto</label>
  						<textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
  					</div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.comentarios-da-imprensa.index', ['espetaculos_id' => $espetaculo->id])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
