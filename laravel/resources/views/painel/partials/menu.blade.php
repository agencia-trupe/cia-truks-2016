<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.dashboard') }}">Cia Truks</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.dashboard')}}" title="Início">Início</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(destaques-home|chamadas-home|imagem-home).*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.imagem-home*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.imagem-home.index')}}" title="Imagem da Home">Imagem da Home</a>
						</li>

						<li @if(str_is('painel.destaques-home*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.destaques-home.index')}}" title="Destaques da Home">Destaques da Home</a>
						</li>

						<li @if(str_is('painel.chamadas-home*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.chamadas-home.index')}}" title="Chamadas da Home">Chamadas da Home</a>
						</li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.(espetaculos|comentarios-da-imprensa|paginas-texto|cursos-e-oficinas|criticas-e-materias|fale-conosco|texto-espanhol|texto-ingles|galerias-adicionais|videos-adicionais).*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Conteúdo <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(espetaculos|comentarios-da-imprensa*)~', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.espetaculos.index')}}" title="Espetáculos">Espetáculos</a>
						</li>

						<li @if(str_is('painel.paginas-texto*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.paginas-texto.index')}}" title="Páginas de Texto">Páginas de Texto</a>
						</li>

						<li @if(str_is('painel.galerias-adicionais*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.galerias-adicionais.index')}}" title="Galerias Adicionais">Galerias de Fotos Adicionais</a>
						</li>

						<li @if(str_is('painel.videos-adicionais*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.videos-adicionais.index')}}" title="Galerias Adicionais">Vídeos Adicionais</a>
						</li>

						<li @if(str_is('painel.cursos-e-oficinas*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.cursos-e-oficinas.index')}}" title="Cursos e Oficinas">Cursos e Oficinas</a>
						</li>

						<li @if(str_is('painel.criticas-e-materias*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.criticas-e-materias.index')}}" title="Críticas e Matérias">Críticas e Matérias</a>
						</li>

						<li @if(str_is('painel.fale-conosco*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.fale-conosco.index')}}" title="Fale Conosco">Fale Conosco</a>
						</li>

						<li @if(str_is('painel.texto-espanhol*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.texto-espanhol.index')}}" title="Texto em Espanhol">Texto em Espanhol</a>
						</li>

						<li @if(str_is('painel.texto-ingles*', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.texto-ingles.index')}}" title="Texto em Inglês">Texto em Inglês</a>
						</li>
					</ul>
				</li>

				<li @if(str_is('painel.mensagens*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.mensagens.index')}}" title="Depoimentos">Depoimentos</a>
				</li>

				<li @if(str_is('painel.agenda*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.agenda.index')}}" title="Agenda">Agenda</a>
				</li>

				<li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.noticias.index')}}" title="Notícias">Notícias</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>
