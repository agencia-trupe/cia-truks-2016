@if($errors->any())
	<div class="alert alert-block alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		{{ $errors->first() }}
	</div>
@endif

@if(Session::has('sucesso'))
	<div class="alert alert-block alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		{{ Session::get('sucesso') }}
	</div>
@endif