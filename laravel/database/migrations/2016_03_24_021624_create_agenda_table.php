<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espetaculos_id')->unsigned();
            $table->foreign('espetaculos_id')->references('id')->on('espetaculos')->onDelete('cascade');

            $table->date('data');
            $table->string('horarios');
            $table->text('texto');
            $table->integer('ordem');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agenda');
    }
}
