<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspetaculosComentariosImprensaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios_imprensa', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espetaculos_id')->unsigned();
            $table->foreign('espetaculos_id')->references('id')->on('espetaculos')->onDelete('cascade');

            $table->text('texto');
            $table->string('assinatura');
            $table->date('data');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comentarios_imprensa');
    }
}
