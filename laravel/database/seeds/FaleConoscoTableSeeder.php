<?php

use Illuminate\Database\Seeder;

class FaleConoscoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('fale_conosco')->delete();

      DB::table('fale_conosco')->insert([
        [
          'texto' => '11 3865-8019<br>11 99952-8553<br>Rua LUMINÁRIAS, 274 / 91<br>05439-000 - São Paulo - SP'
        ]
      ]);
    }
}
