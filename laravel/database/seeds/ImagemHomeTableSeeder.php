<?php

use Illuminate\Database\Seeder;

class ImagemHomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('imagem_home')->delete();

      DB::table('imagem_home')->insert([
        [
          'imagem' => ''
        ]
      ]);
    }
}
