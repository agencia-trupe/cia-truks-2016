<?php

use Illuminate\Database\Seeder;

class PaginasTextoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('paginas_texto')->delete();

      DB::table('paginas_texto')->insert(
      [
        [
          'titulo' => 'A Companhia',
          'slug' => str_slug('A Companhia'),
          'texto' => '',
        ],
        [
          'titulo' => 'Eventos',
          'slug' => str_slug('Eventos'),
          'texto' => '',
        ],
        [
          'titulo' => 'Centro de Estudos',
          'slug' => str_slug('Centro de Estudos'),
          'texto' => '',
        ],
        [
          'titulo' => 'Contação de Histórias',
          'slug' => str_slug('Contação de Histórias'),
          'texto' => '',
        ],
        [
          'titulo' => 'Contratações',
          'slug' => str_slug('Contratações'),
          'texto' => '',
        ],
        [
          'titulo' => 'Cursos e Oficinas',
          'slug' => str_slug('Cursos e Oficinas'),
          'texto' => '',
        ],
      ]);
    }
}
