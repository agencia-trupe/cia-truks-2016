<?php

use Illuminate\Database\Seeder;

class CursosEOficinasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('cursos_e_oficinas')->delete();

      DB::table('cursos_e_oficinas')->insert([
        [
          'titulo' => 'Dramartugia para o teatro de animação',
          'slug'   => str_slug('Dramartugia para o teatro de animação'),
          'ordem'  => 0
        ],
        [
          'titulo' => 'Técnicas de animação de bonecos, objetos e figuras',
          'slug'   => str_slug('Técnicas de animação de bonecos, objetos e figuras'),
          'ordem'  => 1
        ],
        [
          'titulo' => 'O teatro de objetos da Cia Truks',
          'slug'   => str_slug('O teatro de objetos da Cia Truks'),
          'ordem'  => 2
        ]
      ]);
    }
}
