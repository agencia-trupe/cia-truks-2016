<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('EspetaculosTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('MensagensTableSeeder');
        $this->call('PaginasTextoTableSeeder');
        $this->call('CursosEOficinasTableSeeder');
        $this->call('FaleConoscoTableSeeder');
        $this->call('DestaquesHomeTableSeeder');
        $this->call('ChamadasHomeTableSeeder');
        $this->call('TextoInglesTableSeeder');
        $this->call('TextoEspanholTableSeeder');
        $this->call('ImagemHomeTableSeeder');

        Model::reguard();
    }
}
