<?php

use Illuminate\Database\Seeder;

class ChamadasHomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('chamadas_home')->delete();

      DB::table('chamadas_home')->insert([
        [
          'titulo' => 'Centro de estudos',
          'texto'  => 'Texto chamada 1',
          'link'   => '#',
          'ordem'  => 0
        ],
        [
          'titulo' => 'Cursos e Oficinas',
          'texto'  => 'Texto chamada 2',
          'link'   => '#',
          'ordem'  => 1
        ],
        [
          'titulo' => 'Eventos',
          'texto'  => 'Texto chamada 3',
          'link'   => '#',
          'ordem'  => 2
        ]
      ]);
    }
}
