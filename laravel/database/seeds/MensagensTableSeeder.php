<?php

use Illuminate\Database\Seeder;

class MensagensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('mensagens')->delete();

      $registros = [
        [
          'nome' => 'Bruna Guedes',
          'email' => 'brubru08@hotmail.com',
          'texto' => 'A Cia Truks é demais. Graças a ela me apaixonei por teatro de bonecos! Este grupo é simplesmente maravilhoso, suas apresentações fazem com que nós adultos voltemos a ser crianças. Parabéns a todos!!! ',
          'created_at' => '2010-07-08 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Marilia Teofilo da Silva',
          'email' => 'marilia_teofilo@hotmail.com',
          'texto' => 'Eu e minha filha Isabela amamos muto vocês.Já assistimos todas as peças.Vocês são mais que atores,são Encantadores de Alma...Parabéns í  todos.\r\nCom o carinho de sempre ...\r\n               Marilia e Isabela',
          'created_at' => '2010-06-07 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Josimar Noriega',
          'email' => 'josimar.n@hotmail.com',
          'texto' => 'Acabo de vê-los no Festival de Belo Horizonte. Eu e meu filho de 4 anos não conseguiamos piscar, de tão encantador espetáculo! Vocês estão de parabéns! Ficamos emocionados e choramos juntos no final! Continuem! Vocês são importantes para a formação das nossas crianças! Um forte abraço!',
          'created_at' => '2010-06-12 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Gorete e Lucas',
          'email' => 'goretequeiroga@bol.com.br',
          'texto' => 'Estamos encantados com o verdadeiro espetáculo o  \"Senhor dos Sonhos\". Meu filho Lucas, nem piscou. Eu fiquei emocionada. Voces são demais.\r\nObrigada pelo encantamento.\r\nGorete e Lucas - 15/06/2010',
          'created_at' => '2010-06-15 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Mariane Denegredo',
          'email' => 'marianedenegredo@hotmail.com',
          'texto' => 'Parabéns! Tive a oportunidade de ver e sentir a magia do trabalho de vcs, \'Cidade Azul\', foi fantástico, me emocionei com a beleza e o sentimento que vcs transmitem.. abraço a tOdos! Mari Denegredo, Joinville- SC',
          'created_at' => '2010-06-29 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'JÚLIO/ROSE',
          'email' => 'costa_nascimentoss@yahoo.com.br',
          'texto' => 'Cia Truks...\r\n\r\nO que falar de pessoas tao talentosas e seres humanos fantasticos !!!!!!!\r\n\r\nSaudades',
          'created_at' => '2010-07-25 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Luiz Lourencetti',
          'email' => 'luizlouren@gmail.com',
          'texto' => 'Olá, sou de votuporanga, onde a cia Truks esteve se apresentando neste fim de semana... Gostaria de expressar aqui meu encantamento pelo trabalho da cia, fiz os dois dias de workshop com o Henrique que foi maravilhoso, e assistindo o Senhor dos sonhos, vi que podemos sim sonhar, e o sonhos podem ser poderosos, assim como os do \"Lucas\"... Eu, enquanto ator, fui surpreendido com a mágica dos bonecos, com a vida que eles apresentam e com o talento dos atores que os manipulam com tanto amor... Conheci a mágica que só os bonecos podem proporcionar e encantar as crianças e os adultos também... e isso pode ser feito até com um bonequinho de sacolinha..rss.. como é lindo... Parabéns mais uma vez a todos, obrigado pela mágica, e Henrique, obrigado pelos ensinamentos... ',
          'created_at' => '2010-10-25 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Eliete Nicola',
          'email' => 'elietenicola@hotmail.com',
          'texto' => 'Fiquei tão feliz em achar a página de vcs. Tive a oportunidade de ver vcs no colégio do meu filho ha um tempo atrás(anos), e desde então me tornei fí. Lucas...meu preferido, acho que somos parecidos ! Vou fazer 40 anos em março, mas guardo dentro de mim ,os sonhos e inocencias de criança. Parabens meus amores ! Que Deus continue abençoando o trabalho e o caminhar de vcs. Que mtos olhinhos continuem a brilhar com cada apresentação e que atraves desse trabalho, se façam corações melhores p/ o mundo. ',
          'created_at' => '2011-01-07 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'ROSE RIBEIRO',
          'email' => 'rosepsico@hotmail.com',
          'texto' => 'Adoreiiii... assisti em são caetano ao espetáculo Sonhatório... sou psicóloga e trabalho com pessoas com problemas psiquiatricos... e é isso mesmo...o melhor remédio é BRINCAR.... ',
          'created_at' => '2012-08-13 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Luciana Barrichelo',
          'email' => 'luciana@barrichelo.com.br',
          'texto' => 'MARAVILHOSO! Fomos ao Sesc Pinheiros esse final de semana e tivemos o imenso prazer de assistir Sonhatório. TO-DOS curtimos! Eu, meu marido, meu filho de 3 anos e minhas filhas de 4 e 8 anos. Cada um guardou uma parte diferente da peça, mas os 3 ficaram encantados. A Cia Truks nossos parabéns e MUITO OBRIGADA pelo talento. Gde abraço, Luciana e famí­lia Barrichelo.',
          'created_at' => '2012-08-28 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'edna soprano',
          'email' => 'ed.soprano@ig.com.br',
          'texto' => 'Eu e minha filha de 5 anos somos fãs de vocês. Agora é hora da minha outra filhinha que vai fazer 2 anos, também se apaixonar por vocês!!!Quando vocês estarão em SP? Voltem logo!!!!\r\nEdna, Ana Lais e Lí­vya',
          'created_at' => '2011-03-01 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Adriana Bega',
          'email' => 'adrianabega@ig.com.br',
          'texto' => 'Parabéns por esta magia que nos envolve e nos faz sonhar.Tive a de vê-los pessoalmente na biblioteca Monteiro Lobato-SP com o espetáculo Gigante e sempre que puder estarei acompanhando vocês junto com meu marido e minha filha de 05 anos que ficou encantada...Milhíµes de beijos e mais uma vez parabéns pelo maravilhoso trabalho....',
          'created_at' => '2011-03-22 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Patrí­cia Marçal',
          'email' => 'triciamarcal@gmail.com',
          'texto' => 'Ontem, meu marido e eu levamos nossa filha de 5 anos para assistir ao \"Zoo-ilógico\". Ela vibrou o tempo todo, não piscou os olhos. A peça é linda, muito criativa. Eu me emocionei no final, quando os atores tiram o chapéu e começam a retirar vários \"bichos\" da cesta, informando que ainda há muitos... lindo!! é um grande incentivo a nossa criatividade! Parabéns! Abraços, Patrí­cia.\"',
          'created_at' => '2011-03-23 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Maria Paula Vilches',
          'email' => 'mariapaulavilches@ig.com.br',
          'texto' => 'A emoção tomou conta de mim,ontem í  tarde ao assistir \"Os vizinhos\" no Sesc Santana...Meu filho de 11 anos e meu marido irradiavam alegria através dos olhos!!!Melhor ainda foi escutar do meu filho:...Míe compra os ingressos para o próximo espetáculo no domingo que vem,eu não posso perder! \r\nOs efeitos \"especiais\", a integração entre os atores, tudo nos faz aplaudir!\r\nMas o principal de tudo é a mensagem...podemos transformar a situação...a guerra em PAZ ! Basta nos unirmos! \r\nQue Deus continue inspirando í  todos e abençoando a cada dia o trabalho maravilhoso de vocês! \r\nQue a energia positiva que vcs nos transmitem jamais se esgote!!!!Abraços í  todos!',
          'created_at' => '2011-05-23 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'Simone Sofia',
          'email' => 'simnediogenes@hotmail.com',
          'texto' => 'Gostaria de parabenizá-los pelo espetáculo Bruxinha, apresentado ontem em Extrema/MG. Minha filhas e eu ficamos encantadas com tanta pureza transmitida durante a apresentação. Vocês com certeza transformarão o mundo, com tanta magia e pureza. Parabéns\r\n',
          'created_at' => '2011-07-21 00:00:00',
          'aprovado' => 1
        ],
        [
          'nome' => 'claudia corleto gomes',
          'email' => 'claudia.corleto70@hotmail.com',
          'texto' => 'Conheci a Cia ano passado aqui em Jaboticabal,fiquei,aliás ficamos eu e meus filhos com o espetáculo.E ontem podemos repetir a emoção de viajar no mundo fantático dos bonecos.Amo vocês,desejo mais e mais sucesso para todos.\r\nFiz o whorkshop í  tarde e simplesmente adorei,meu filho mais velho me acompanhou,participou e adorou também.Enfim foi um sábado muito gostoso.Muita energia para todos e um ótimo domingo. ',
          'created_at' => '2011-10-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Selma Zollo',
          'email' => 'selmazollo@hotmail.com',
          'texto' => 'Gostaria de registar a imensa satisfação de prestigiar este maravilhoso grupo e tão quão maravilhoso espetáculo BIG BANG, que tive a oportunidade de ver ao acompanhar meus alunos no Projeto Cultura é Currí­culo. Obrigada í  todos por deixar a Geografia e a História do mundo tão próxima e tão didaticaticamente divertida. Os aplausos pra vocês serão eternos. ProfÂª Selma EE Presidente Kennedy',
          'created_at' => '2011-10-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Bruna Corrêa Nonato',
          'email' => 'bruna_cnon@hotmail.com',
          'texto' => 'Gostaria de parabenizar a companhia pelo excelente trabalho. Em 20/10/11, fui com meus alunos ao SESC Interlagos para a apresentação de vocês e eles assim como eu saimos de lá encantados com a performance dos atores e dos bonecos. Vocês trazem muita vida í  apresentação além de serem profissionais muito preparados, têm uma simpatia muito grande pois prenderam a atenção do público e também deixaram todos bem í  vontade para tirar as dúvidas no fim da peça. Sinto-me privilegiada de ter feito parte da plateia de vocês. Parabéns!!',
          'created_at' => '2011-10-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Claudmeiry Galeazi',
          'email' => 'claudiagaleazi@hotmail.com',
          'texto' => ' Tive o prazer de assistir na manhã desse dia10) í  peça Big Bang, junto com meus alunos do 3o. ano, ensino médio. Muito obrigada pela generosa oportunidade de diversão com inteligência. Todos os alunos ficaram encantados. Continuem esse trabalho maravilhoso! Parabéns!',
          'created_at' => '2011-10-31 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cibele Pimentel',
          'email' => 'cibele_pimentel@hotmail.com',
          'texto' => 'Já tive o prazer de assistir aos espetáculos Big Bang e Zoo-Ilógico e me encantem pelo trabalho de vocês. Parabéns a todos e continuem com essa linda missão de proporcionar í s crianças e aos adultos esse mundo da imaginação. Adoro vocês!!!!',
          'created_at' => '2012-01-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'ana paula',
          'email' => 'ludoroque@zipmail.com.br',
          'texto' => 'hoje em orlandia tive  privlegio de assistir com meu filho \"o senhor dos sonhos\", um espetaculo simplesmente lindo!! sensivel,engraçado,emocionante!! parabens! realmente e um trabalho perfeito!! fiquei sem palavras!!espero que continuem essa maravilha!!\r\n',
          'created_at' => '2012-03-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'ELMAZ',
          'email' => 'elmamonesi@gmail.com',
          'texto' => 'MUUUITO LEGAL, PRENDE A GENTE, O TEMPO PASSA QUE A GENTE NEM VÚ E DA UM GOSTINHO DE QUERO MAIS!!!! UMA CRIATIVIDADE E SINCRONISMO MARAVILHOSOS QUE NOSSA FAMíLIA CURTIU MUITO COM O \"ZOO-ILÓGICO\", PARABéNS!!!!',
          'created_at' => '2012-04-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Nilton',
          'email' => 'niltonbrilhantines@ig.com.br',
          'texto' => 'Tive o prazer de assistir os espetáculo \"Isto não é um Cachimbo\",em Cerquilho, e gostaria de parabenizar-los pelo ótimo momento que proporcionaram. Quase não pisquei os olhos durante toda a apresentação! Recomendo a todos para assistirem esse espetáculo, pois ficarão vidrados a cada minuto desse show. Sucesso!!',
          'created_at' => '2012-05-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Regilson Feliciano',
          'email' => 'regilson@uol.com.br',
          'texto' => 'A convite de amigos, fui assistir ao espetáculo SONHATÓRIO no SESC Pinheiros. Quero dizer que saí­ emocionado do teatro, e muito feliz com o que assisti, por isso, essa mensagem de imenso PARABéNS a todos desta CIA. A mim, como produtor cultural, faltava assistir há muuuito tempo, um espetáculo na linha \"Infantil\" que na verdade, é para todas as idades...e que fosse Deslumbrante ! ',
          'created_at' => '2012-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lisiani Pellegrini',
          'email' => 'lisiani@ig.com.br',
          'texto' => 'Adorei a oportunidade de assistir ao espetáculo Sonhatório no Sesc Pinheiros, recomendo para todas as idades um programa muito bacana para famí­lia. Sensacional parabéns para CIA.\r\n',
          'created_at' => '2012-07-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lilian Haffner da Rocha Oliveira',
          'email' => 'lilianhro@hotmail.com',
          'texto' => 'Olá, Sou fí de carteirinha do trabalho de vocês. Já assisti a vários espetáculos e é impressionante como vocês conseguem se superar e apresentar coisas novas e de muita qualidade sempre. Hoje fui assistir \"Sonhatório\" com a famí­lia e todos se divertiram muito6 aos 50 anos). Indicarei a peça aos professores e aos pais da escola onde trabalho. Mais uma vez parabéns! ',
          'created_at' => '2012-09-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Danilo Sanches',
          'email' => 'danilo.sanches@gmail.com',
          'texto' => 'Olá, assisti hoje ao espetáculo Sonhatório. Eu e minha filha. Muito obrigado por acrescentar tanto ao diálogo com ela, e também a toda a incursão que tento fazer com ela pelo mundo encantado da imaginação. Me emocionei muito e pelo que pude notar, essa peça pega muito as crianças, coloca-as em contato com coisas internas muito bonitas. Bem, obrigado e parabéns.',
          'created_at' => '2012-09-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'luisa',
          'email' => 'fernandesluisa@terra.com.br',
          'texto' => 'Fomos ontem assistir sonhatório com nossas filhas, Nina e Clara de 5 anos e 1 ano e três meses. SENSACIONAL!! A mais fiel representação do lúdico que já vimos. \r\nRimos um monte e no final impossí­vel segurar as lágrimas. EMOCINANTE!!!\r\nObrigada por nos ajudar com a arte de vocês, a mostrar as nossas filhas como é bom fantasiar!!! Isso é que é arte para todas as idades.\r\n',
          'created_at' => '2012-09-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Danyela Liberalino',
          'email' => 'dany.liberalino@gmail.com',
          'texto' => 'Sonhatório ... o que é isso gente??!!! Simplesmente amei... Vcs são show. Rí­ até ficar com dor na barriga rsrs e depois chorei igual criança... Linda a mensagem que vcs passam !!! Ganharam mais uma fí. Parabéns !!!',
          'created_at' => '2012-09-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sí´nia Prevedello',
          'email' => 'soninhajau@hotmail.com',
          'texto' => 'A felicidade está nas coisas simples, no abraço amigo, em um sorriso espontí¢neo, nas brincadeiras e na imaginação da gente. Vocês me fizeram rir, chorar de rir e me encantar. Muito obrigada. ;)',
          'created_at' => '2012-10-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Mariana Antunes de Oliveira',
          'email' => 'marian_antunes@hotmail.com',
          'texto' => 'Apenas quero parabenizar a todos pela maravilhosa e mágica apresentação que fizeram ontem em Taquarituba, estava perfeito, tudo perfeito. Foi extremamente engraçada, porém emocionante, vocês me fizeram rir demais e, ao final, me fizeram chorar, afinal o garotinho Lucas simbolizou a realidade de muitas crianças, mostrando seus sonhos e muitas vezes se escondendo do mundo real, talvez por medo ou repressão...Enfim, a CIA veio para quem sabe reacender a chama da cultura em nossa cidade, que deveria investir mais nessa arte! Amei mesmo, meus sinceros agradecimentos e muito sucesso a todos!!!!!',
          'created_at' => '2012-11-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Lucia Cardoso ',
          'email' => 'analcrp@gmail.com',
          'texto' => 'Sou de são José dos Campos e gostaria de agradecer a vcs o presente que me foi dado durante as duas apresentações que tive a oportunidade de assistir no Sesc! Por uma Estrela e Sonhatório sao peças infantis para gente pequena e grande, que tocam a alma! Parabéns pelo desempenho dos atores, criatividade, cenário e especialmente pela delicadeza e sensibilidade do teor da historia! Obrigada!!! ',
          'created_at' => '2012-10-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Tati Maluf',
          'email' => 'tati.maluf@uol.com.br',
          'texto' => 'Em Por Uma Estrela, os manipuladores são incrí­veis e os bonecos parecem realmente ter vida. Na hora em que o menino e a menina se separam, o Felipeanos) e eu ficamos muito emocionados. Daí­ em diante, nós dois choramos até o final. Depois da peça os atores muito gentilmente e percebendo a emoção do Felipe vieram conversar com ele, explicaram que estava tudo bem e ele gostou muito. Muita sensibilidade e emoção em uma peça com foco totalmente diferente do que estamos acostumadose com marcações mais rápidas). O espetáculo lentamente cativa e emociona.\r\nIMPERDíVEL.',
          'created_at' => '2012-11-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rene Padilha',
          'email' => 'rpadilhe@yahoo.com.br',
          'texto' => 'Adorei a peça \"Por Uma Estrela\" é realmente espetacular! Fui levar a minha filha de 7 anos pensando em ver uma historinha infantil, porém acabei ficando emocionado com uma grande história de amor interpretada com maestria pelos artistas que manipulavam os bonecos. Sinceramente tenho vontade de repetir a dose. Recomendo a todos! ',
          'created_at' => '2012-12-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Silvia Nogueira',
          'email' => 'silviacoli@yahoo.com.br',
          'texto' => 'Hoje assisti \"Sonhatório\" com minha filha de 5 anos e meu marido. Gostei tanto da peça que senti vontade de vir aqui escrever essas palavras. Uma delicia de peça, inteligente, engraçada, diní¢mica, bem executada, e a mensagem no final, simplesmente linda!!! Ri e chorei! Lindo mesmo!! Que boa surpresa! 09/12/2012\r\n',
          'created_at' => '2012-12-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Monalisa Lins',
          'email' => 'monalisalins@hotmail.com.br',
          'texto' => 'Estivemos hoje assistindo o espetáculo Por uma estrela e gotarí­amos de registrar nosso encantamento. Meu marido foi ontem com nossa filha e eu não pude acompanhar. Ele chegou em casa dizendo que eu TINHA que ver. E realmente ele estava com a razão. A capacidade de transmitir emoção através de um boneco manipulado ao mesmo tempo por três pessoas é tocante. Acho que nós e as crianças vamos sonhar com estes personagens sem voz e sem nome por muitas noites quando olharmos as estrelas! Obrigada',
          'created_at' => '2012-12-19 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cristiane Paduan',
          'email' => 'crisspaduan@hotmail.com',
          'texto' => 'Amei a peça Sonhatório, assisti com minha filha de 6 anos e minha adolescente de 13 e só posso dizer que ela eh ótima, feita pra todas as idades, minha filhas e eu mesma nos divertimos do iní­cio ao fim, a criatividade deles é o mais impressionante. Recomendo a todos porque prende a atenção do inicio ao fim.. tanto a mim como as crianças!!! Vale a Pena. PARABéNS ao elenco de SONHATÓRIO!!!!\r\n',
          'created_at' => '2013-02-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Erica Valle',
          'email' => 'erica_valle@yahoo.com.br',
          'texto' => 'Acabo de ouvir a seguinte conversa vinda lá da cozinha: Violeta: - tem uma peça que a gente viu lá em são Paulo que a gente adora. Pai: - qual? Violetatom de suspence): - tem uma da estrela... Pai: - Eu sei! Violeta: - Vamos brincar? Eu sou a menina! Violeta foi buscar alguns bonecos pra serem os pais e as míes. E agora estão os dois brincando. E se encontrando... \" - vc é aquela menina?\" \"- Eu sou!\" \"- Então me dá um abraço\" ... Enfim. Vim aqui correndo contar pra vcs, pois fiquei devendo um depoimento sobre a peça, desde que a Violeta assistiu 3 vezes ao espetáculo no Sesc Pompéia. Achei o momento perfeito para lhes mostrar o quanto conquistaram e foram compreendidos pela pequena Violeta que tinha no final de 2012 2 anos e meio. Três ou quatro meses depois, depois de Natal com bicicleta cor de rosa, carnaval de havaiana e uma mudança de cidademoramos mais em são Paulo), e depois de assistir a vários outros bons espetáculos, \"Por uma Estrela\" continua inesquecí­vel. Obrigada por tudo isso. E espero lhes encontrar aqui pelo interior... érica ',
          'created_at' => '2013-02-26 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Gabriela',
          'email' => 'delucasrr@hotmail.com',
          'texto' => 'Boa noite! Estou passando para conhecer melhor a companhia, pois acabei de chegar de uma apresentação feita na cidade de Sao Joaquim da Barra, com a peça Sonhatório e estou completamente apaixonada pelo trabalho! Parabéns a vcs por td dedicação, talento, pelo dom de nos transformar através da arte. Que o caminho de vcs seja d mta luz e sucesso!!!! Abçs',
          'created_at' => '2013-02-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luciana Gomes C Centini',
          'email' => 'lcunha@portoseguro.org.br',
          'texto' => 'Depois de hoje, minha semana começa melhor! ESPETíCULO: SONHATÓRIO. Simplesmente encantador. Uma mensagem atual e divertida. Por que nos preocuparmos tanto se o melhor remédio para a saúde é SER FELIZ!!! Obrigada pela linda mensagem!! RECOMENDADíSSIMO!!!',
          'created_at' => '2013-04-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'WALTER ZUANAZZI JUNIOR',
          'email' => 'ZAUPE@HOTMAIL.COM',
          'texto' => 'hj fui com os meus filhos assistir\r\npor uma estrela , no sesi franca sp ..\r\nadoramos , vcs estam de parabens ...\r\nobrigado por existir pessoas como vcs \r\nq nos tras a alegria a imaginação,etc\r\nmuito obrigado pelo dia maravilhoso !!!esperamos em breve assistir de novo ...',
          'created_at' => '2013-04-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luciane Souza',
          'email' => 'luciane_souza@yahoo.com',
          'texto' => 'Simplesmente encantador... Adorei...',
          'created_at' => '2013-04-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Patricia',
          'email' => 'patriciaro@uol.com.br',
          'texto' => 'Parabéns pela peça Sonhatório. Atores excelentes, trilha sonora e iluminação perfeitas. Engraçado, inteligente e sensí­vel. Minha famí­lia adorou!',
          'created_at' => '2013-04-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'érika Christiane de Lima Sato Nassif Nunes',
          'email' => 'erikasato@netmg.com.br',
          'texto' => 'Parabéns Cia Truks, o Espetáculo Sonhatório nos fez enxergar, de uma forma bem humorada, que a vida pode e deve ser vivida de forma leve e criativa... Simplesmente DEMAIS!!!!',
          'created_at' => '2013-05-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ingrid',
          'email' => 'ireis434@gmail.com',
          'texto' => 'Foi simplesmente incrivel peça Sonhatório se pudesse veria mais umas mil vezes.  ',
          'created_at' => '2013-05-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alice Lane',
          'email' => 'alicelane.41@hotmail.com',
          'texto' => 'Parabéns pela bela apresentação na cidade de Anápolis-Go, Teatro Municipal com a peça Cidade Azul, minha filha e eu gostamos muito, vcs estão de parabéns muito bom qdo os bonecos interagem com o público...voltem sempre com novidades nossa cidade é carente de peças boas assim...sucessos e um abraço, Alice e Vitória.',
          'created_at' => '2013-06-04 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cynthia Lima',
          'email' => 'cynthiapachecolima@hotmail.com',
          'texto' => 'Vocês são fantásticos, amei, adorei, o Teatro Azul é uma maravilhosa história, a forma como dão vida aos bonecos é liiiindo e emocionante. Continuem assim e não desistam desse ofí­cio, pois nasceram para isso, que DEUS continue abençoando todos vocês!!!',
          'created_at' => '2013-06-04 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Walace Oliveira',
          'email' => 'walaceteatro@gmail.com',
          'texto' => 'Olá, Sou de Anápolis-Go, assisti hoje a apresentação de CIDADE AZUL. Estou escrevendo para agradece-los pelo trabalho de vocês, com gratidão por nos proporcionarem esse grande espetáculo . Fiquei sentindo a magia da apresentação por horas. Estou encantado pela forma que vocês respeitam os bonecos em cena, o carinho que nos passam na manipulação, durante a apresentação, e isso é importantí­ssimo. Aquela cena em que o menino vai se deitar cobrindo com um pedaço de jornal, mexeu muito comigo deixando-me com lágrimas a escorrer, uma cena fortí­ssima! Ela mostra a realidade que ainda existe no nosso paí­s. Ao sair do teatro só consegui agradecer a Deus, pelo conforto que tenho, pela minha cama quentinha, meu coberto, e ao meu belí­ssimo lar que tenho junto com minha famí­lia. Um grande abraço desse Goiás, vida longa ao espetáculo, muita merda a todos e muita luz a CIA TRUKS. Saravá...',
          'created_at' => '2013-06-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Nina Brandão Canal',
          'email' => 'nina.canal@gmail.com',
          'texto' => 'Sonhatório... \r\nQue maravilha!!! Assistimos o espetáculo agora a tarde aqui em Jahu e amamos! Parabéns!!! Deus abençoe vcs!!!\r\nah! não perdoo vcs pela bagunça que minha filha está fazendo na minha cozinha... hahahahaha ',
          'created_at' => '2013-06-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alessandra Dionisio Carraro',
          'email' => 'alessandracarraro@hotmail.com',
          'texto' => 'Fui com meu marido e nossos três filhos, sábado  a tarde, no teatro em Jahu e saí­mos de lá felizes da vida e muito satisfeitos com a peça Sonhatório.Parabéns! Lindo espetáculo!',
          'created_at' => '2013-06-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Janei',
          'email' => 'ane_vic@hotmail.com',
          'texto' => 'Parabéns ! Acabamos de sair do espetáculo(eu e meu marido) e adoramos. Como é gostoso sorrir e ouvir o sorriso das crianças, confesso que me emocionei em saber que existe espetáculos como esse que afirmam que sonhar é possí­vel.',
          'created_at' => '2013-06-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'SAMIRA BAUAB CAPORUSSO',
          'email' => 'samirabauab@ig.com.br',
          'texto' => 'Sonhatório. excelente peça, o público ficou encantado.\r\nObrigado pela apresentação na cidade de Guariba/SP. Que Deus os abençõe.\r\nAguardamos com ansiedade outras apresentações.    \r\n',
          'created_at' => '2013-06-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Flávia S. Gomes',
          'email' => 'flaviasgarbosagomes@yahoo.com.br',
          'texto' => 'Surpreedente, emocionante, cativante, inteligente,encantador,impressionante! Como psicóloga fiquei totalmente tocada e como míe agradecida por ver minha filhinha imitando vcs logo que chegou em casa! Profissionais do Sonhatorio, Guariba agradece e muito a presença de vcs!Parabens!',
          'created_at' => '2013-06-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Tiago Piotto',
          'email' => 'tiagopiotto@gmail.com',
          'texto' => 'Acabo de assistir ao espetáculo Sonhatório aqui em Ibitinga. Fiquei encantado com a apresentação. Parabéns pelo excelente trabalho, pelo resgate da imaginação e do brincar com as coisas do dia a dia. Sou Psicólogo e posso dizer que fiquei emocionado pelo poder cativante da peça, desde crianças até idosos, todos contemplando e sorrindo com vocês. Parabéns e que muitos tenham a oportunidade de lhes conhecer! Abraços',
          'created_at' => '2013-06-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vanessa Cristina Ferreira',
          'email' => 'vancrisfer79@gmail.com',
          'texto' => 'Cidade Azul - apresentação no auditório do CENFORPE em SBCampo - apresentado em 29/06 - UM ESPETíCULO ! \r\nTive a oportunidade de assistir uma apresentação no shopping Plaza são Bernardo e me encantei !! LINDO ! Me emociono demais porque minha imaginação viaja... Quero parabenizar a todos pela excelente ideia... Por toda alegria que vocês passam e sentimentos de bem estar,espero poder ver mais e me emocionar ainda mais ! PARABéNS !!!! ',
          'created_at' => '2013-07-01 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'shirley rodrigues',
          'email' => 'shirod1@hotmail.com',
          'texto' => 'Bom dia, ontem, pela segunda vez, eu e meu filho Gabriel de 9 anos, assistimos a peça SONHATí’RIO....amamos muito, é uma peça rica de mensagens, muito divertida, muito bem escrita.parabéns, vcs são demais.Abraços:Shirley e Gabriel.',
          'created_at' => '2013-07-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => ' Patricia Spinelli',
          'email' => 'patel@uol.com.br',
          'texto' => 'Assistimnos ao espetáculo Sonhatório no center norte!!Divertido, sensí­vel e com uma intencionalidade que atinge cada um naquilo que lhe é necessário naquele momento...emociona e nos leva a refletir... Parabéns!! Eu, meu marido e filhos adoramos!',
          'created_at' => '2013-07-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Bruno C. Ribeiro',
          'email' => 'bruno.chapadeiro@ufpr.br',
          'texto' => 'Assisti ao espetáculo \"Por uma Estrela\" no Teatro Guairinha aqui em Curitiba-PR e fiquei emocionado. Vocês são excelentes! não somente pela técnica de manipulação e articulação dos bonecos, mas também pela expressão de quem manipulava os bonecos! Vocês davam uma vida incrí­vel aos bonecos que pareciam expressar em todos os momentos os sentimentos da história que vocês contavam ao público. estão de parabéns!',
          'created_at' => '2013-07-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'CHICO',
          'email' => 'Chico@wirf.com.br',
          'texto' => 'Voltar a ser criança onde somente o imaginário é senhor, onde somente a magia tem permissão para ousar, agir, interferir ou repousar. Onde o recordar transmite uma beleza serena, uma leveza que a vida não nos permite mais explorar. Brincar com peças inanimadas transformadas em sublime fantasia paralela e real me levou a pensar: Do que são feito os sonhos? Sempre acreditei que sonhos são feitas de realidades além das culpas, alheio a julgamento e imune a contestações. são simples como as bolinhas de sabão, como reservatório de ideias e como as mais doces ilusíµes. são intensos assim como o risos, encravados como o punhal do choro sincero que guardados estavam no fundo do coração. Sao imaginarios reais que dispensam palavras para se fazer representar. A curiosidade disparada não conhece limites para alçar voa e ai a felicidade presente transborda. Uma floresta, o mar, uma jornada especial, a mensagem que só é possí­vel observa com olhos de criança matreira, inocente, certeira, sem o olhar castrador do não. O espetáculo \"SONHATORIO\" da CIA TRUKS é acima de tudo uma viagem de resgate a tudo que remete aos sonhos no mundo do brincar. é brincando que reconhecemos a dureza da vida e desenvolvemos boa parte dos nossos valores. é na poesia do sonhar que nos deparamos com a realidade e na realidade da vida que deixamos de sonhar. Vive aquele que brinca. Brinca aquele que sonha. Sonha aquele que se permite entrar na dança e dar voz a sua criança. Se você tem ainda uma criança dentro de si, ao vê-los vai reencontrar, mas se não sabe onde ela esta, leve teu filho pelas mãos nesta viagem de sonhos.. com certeza ele vai te mostrar. Acredite nunca é tarde pra voltar a sonhar! ',
          'created_at' => '2013-07-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Profa. Silvia M. G. Cardoso',
          'email' => 'silvia.m.g.cardoso@gmail.com',
          'texto' => 'Acompanho o trabalho de vocês há alguns anos, e fico sempre muito impressionada. Após assistir aos seus espetáculos, quando se apresentam na escola onde trabalho, levo a famí­lia para poder sorver esta maravilha, nos teatros onde se apresentam. Vocês são diferenciados. Donos de uma sensibilidade impar, de um entendimento do universo da criança que me deixa emocionada e feliz por encontrar artistas completos, especiais e essenciais para a formação de nossas crianças! Agradeço pela oportunidade de conhecer trabalhos como Zí´o-Ilógico, Sonhatório, O Senhor dos Sonhos, Cidade Azul, um mais lindo do que o outro. Parabéns, continuem sempre nesta linda tarefa de encantar através de sua arte tão bonita. Um grupo como o de vocês deveria ser considerado patrimí´nio cultural de nosso paí­s! ',
          'created_at' => '2013-07-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Daniela',
          'email' => 'daniela_berbel@yahoo.com.br',
          'texto' => 'Assistimos o espetáculo \"Sonhatório\" no Teatro Vitória em Limeira.\r\nFoi fantástico!!!\r\nAlém de ser muito inteligente, criativo e divertido, nos faz refletir sobre a importancia do brincar na vida das crianças; e ainda nos revela que para isso não são necessários brinquedos caros, basta usar a imaginação.\r\nA toda a Cia Truks, meus parabéns e eu minha familia adoramos o espetáculo, o qual nos emocionou muito.\r\nVoltei mais vezes....\r\nSucesso a vocês!!!',
          'created_at' => '2013-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'gilza e andre',
          'email' => 'gilzagava@hotmail.com',
          'texto' => 'Adorei o espetáculo Sonhatório que assisti em Limeira. Parabéns. ótimo trabalho....Nos tornamos seus fí¢s. ',
          'created_at' => '2013-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lia Maura e Ricardo',
          'email' => 'liamaurac@hotmail.com',
          'texto' => 'Assistimos o Grande espetáculo \" Sonhatório\" em Limeira, adoramos !!!!!!! particularmente não consigo olhar para os utensí­lios na cozinha e não lembrar de vocês, parabêns pela criatividade e dedicação que vocês transmitem para nos seres humanos.\r\nRicardo / Lia Maura e Manuela - 27/07/2013',
          'created_at' => '2013-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Denise ',
          'email' => 'denise_silvap@yahoo.com.br',
          'texto' => 'Assisti a peça \"Sonhatório\" na última sexta-feira07) em Limeira e acredito que parabenizá-los seria pouco. Fiquei muito emocionada, como todos os que eu via a meu redor; adultos com olhinhos e sorrisos de criança. Poderia falar muitas coisas e muitos sentimentos que essa peça me despertou, mas me limito a dizer: obrigada por dar uma dose de esperança a corpos e almas tão sufocadas pelo cinza de nosso dia-a-dia. Evoé ',
          'created_at' => '2013-07-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vanessa, Fernando e Viní­cius',
          'email' => 'vgaldao@yahoo.com',
          'texto' => 'Assistimos ao espetáculo \"Sonhatório\" aqui em BH. Saí­mos do teatro maravilhados e ainda mais certos de que, para ser feliz, é preciso simplicidade e um pouquinho de criatividade. Obrigado por terem feito do nosso domingo um dia tão especial! Parabéns, parabéns, parabéns !!!!!!!!!',
          'created_at' => '2013-08-19 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'LARISSA CARVALHO',
          'email' => 'larissa.carvalho@tvglobo.com.br',
          'texto' => 'Sonhatório é fabuloso. Meu filho João, de 7 anos, ficou paralisado, olhos fixos no palco: \"Míe, tem muito brinquedo na cozinha!\" E não?.. rsrs... Minha enteada, Mariana, de 9 anos, gargalhava. O pai dela, pela primeira vez, não cochilou nem procurou jogos no celular em uma peça infantil. Infantil? não. Sonhatório é inteligente, intrigante, emocionante. Cutuca o menino que existe em cada um de nós e peleja pra fazê-lo acordar... é que os \"doidinhos\" daquele divertido sanatório têm um recado... ops! uma \"receita\" pra nos passar: B.R.I.N.C.A.R! ',
          'created_at' => '2013-08-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'KARIME MARQUES LARA',
          'email' => 'karimelara@ig.com.br',
          'texto' => 'Pessoal, fui assistir vcs com a peça Sonhatório em BH e fiquei muito emocionada com a mensagem da peça! Sou professora infantil e trabalho com a imaginação das crianças o tempo todo, mas naquele dia, eu me senti uma criança! Como o mundo está carente de \"brincar\", de sonhar, de ser...Viajei no \"sonhatório\" com vcs! Vocês são ótimos!! Parabéns pela atuação, pela peça, pela mensagem! Vocês tornaram minha vida melhor! Muito obrigada!! Karime',
          'created_at' => '2013-08-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alexandre',
          'email' => 'amoura@ig.com.br',
          'texto' => 'Assisti í  peça Sonhatório em BH e fiquei surpreso com a qualidade do espetaculo. Mistura de talento, criatividade e bom humor. Sem duvida a melhor peça do Festiva de Bonecos 2013 e esperamos que voltem a BH com este e com outros espetaculos da companhia.',
          'created_at' => '2013-08-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andreia Suli',
          'email' => 'andreiasuli@hotmail.com',
          'texto' => 'Encantamento! Esta foi a sensação mágica que sentimos, eu e minha famí­lia, ao assistirmos o espetáculo Sonhatório ontem em Cerqueira César! Seguimos o conselho dos médicos-artistas e fizemos em casa a segunda parte  da peça brincando muito! Parabéns ao grupo por levar alegria, sabedoria e a receita de uma vida feliz: BRINCAR!!!',
          'created_at' => '2013-09-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vanessa Souza',
          'email' => 'v1n2ss1c1rl4s@hotmail.com',
          'texto' => 'Adorei Sonhatório. Uma mistura de poesia e riso, onde o imaginar é exercitado a cada segundo. O espetáculo nos lembra que para viajarmos na nossa imaginação não precisamos de nada mais do que vontade de sonhar...',
          'created_at' => '2013-09-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Dimas',
          'email' => 'dimas.fabricio@gmail.com',
          'texto' => 'O SENHOR DOS SONHOS em Catanduva... simplesmente sensacional.... espero que voltem logo com outra apresentação.',
          'created_at' => '2013-10-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fabiana Janczur Picchi Levada',
          'email' => 'fabiana_levada@hotmail.com',
          'texto' => 'Vi o espetáculo SONHATÓRIO na escola Divina Providência, sou professora de música no colégio, e só posso dizer que se trata de um espetáculo de alta qualidade!!!Criatividade, humor e mensagem são os ingredientes da Cia. Truks. ADOREI!!!!!!',
          'created_at' => '2013-10-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vanessa Rondinoni',
          'email' => 'vanessa_vitor@hotmail.com',
          'texto' => 'Ficamos todos encantados e nos divertimos muito com a Cia Truks em \"O Senhor dos Sonhos\" na cidade de Catanduva. Parabéns e continuem levando muita alegria pra essa criançada0 a 100 anos kkk). ',
          'created_at' => '2013-10-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luciana Kelly de Camargos Batista',
          'email' => 'lu_camargos@yahoo.com.br',
          'texto' => 'Vi o espetáculo sonhatório no SESC Presidente Prudente e me senti logo no inicio privilegiada por estar ali. Era um espetáculo diferente de todos os outros. Me emocionei como míe ao perceber que a loucura e a infí¢ncia eram separadas por uma linha tenue e deliciosa, e como médica que sou, em ver quanto preconceito ainda terí­amos que vencer no tratamento convencional. E depois o final para mim foi apoteótico. Enfim, o espetáculo não saiu da minha cabeça. Parabéns de verdade! Brilhante a ideia, a criação e a execução! ',
          'created_at' => '2013-10-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Bruna Ferreira Pedroso',
          'email' => 'brunaferreirapedroso@gmail.com',
          'texto' => 'Truks foi no Anglo de Osasco10/10/2013) e apresentou o teatro \"Sonhatório\". Achei DEMAIS, amei a peça! Achei criativa,animada, fofa, etc... Gostaria de ver outra apresentação! Mas o que importa é que foi MARAVILHOSA, amei e quero dar os parabéns para os atores e para todos que ajudaram a fazer essa peça!!!!!!!!!!! Bjs,Bruna!',
          'created_at' => '2013-10-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'CARLOS EDUARDO',
          'email' => 'meps5000@hotmail.com',
          'texto' => 'A peça \"A Bruxinha\" é muito legal, todos os personagens são engraçados. O que eu mais gostei foi o Leão, e o monstro do travesseiro é interessante. No começo fiquei com medo, mas depois gostei, por que ele voava. Beijos de Carlos Eduardoanos) SP',
          'created_at' => '2013-10-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leslie',
          'email' => 'leslieloh@ig.com.br',
          'texto' => 'Vcs são demais! Amei a mensagem, a interpretação, a criatividade, a imaginação. Tive vontade de parabeniza-los pessoalmente, mas como fiquei emocionada tb fiquei c vergonha das minhas lágrimas. Amo teatro, amei vcs. Parabéns',
          'created_at' => '2013-10-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Priscila',
          'email' => 'priscila.fca@gmail.com',
          'texto' => 'Fui este domingo í  peça sonhatório...O Nome não poderia ser melhor, uma peça que nos ensina a sonhar, e brincar, junto com  muitas risadas e expressíµes incrí­veis dos atores. \r\nO roteiro é muito bom, e a interpretação agrega um toque ainda mais especial í  toda a história. \r\nMeus filhos de 6 e 3 anos se divertiram muito!',
          'created_at' => '2013-11-12 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Silmara Vidal',
          'email' => 'vidalsilmara@hotmail.com',
          'texto' => 'Assisti junto com meu filho Juan, de 4 anos, o Espetáculo Sonhatório, em Capivari. Fazia tempo que não via meu filho dar tantas gargalhadas. Me emocionei ao vê-lo tão feliz...Tenho que agradecer por isso...Me diverti muito... Parabéns, vcs são maravilhosos. Um grande abraço....',
          'created_at' => '2013-10-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cláudia',
          'email' => 'clodinha@hotmail.com',
          'texto' => 'Sempre levo meu filho í  peças infantis em diversos formatos. Neste domingo, sentei pra assistir com ele, no Shopping Metrópole a peça Sonhatório e confesso que foi a melhor que já vi na vida. Foi uma experiência incrí­vel. Envolve muita sensibilidade e criatividade. Além de dar muitas risadas, fiquei realmente emocionada com a mensagem transmitida pelos atores. A cada ato, uma surpresa, um riso e até uma lágrima. Parabéns! Aplausos de pé!',
          'created_at' => '2014-02-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Tatiane',
          'email' => 'tatianegmdealcantara@gmail.com',
          'texto' => 'Assistimos hoje no Metrópole,a peça Sonhatório,meu filho de 3 anos se divertiu muito,parabéns pelo talento,o uso de objetos comuns com tanta criatividade e pela linda mensagem da peça!!',
          'created_at' => '2014-02-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Suzi Mel',
          'email' => 'suzimkt@hotmail.com',
          'texto' => 'Olá, eu, marido e nossas 2 filhas, tivemos a grande alegria de vê-los no Shopping Metrópole domingo passado, foi maravilhoso, adoramos.... Parabéns pelo capricho, riqueza de detalhes e inteligencia. Mantenham essa simpatia e simplicidade no ser. =)',
          'created_at' => '2014-02-04 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andréa Rechineli Lara',
          'email' => 'arechineli@hotmail.com',
          'texto' => 'Olá Pessoal da Cia Truks! Eu e meu filho Breno de 11 anos assistimos ao espetáculo Sonhatório em Bertioga, neste fim-de-semana e ADORAMOS!!!!!! Ficamos impressionados com a criatividade e riqueza de detalhes tão complexos e simples ao mesmo tempo!! E os atores? Perfeitos!!! Fiquei imaginando o meu filho fazendo parte desta turma! Que orgulho seria! Parabéns!!',
          'created_at' => '2014-02-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Daniela',
          'email' => 'Danniela.malik@ymail.com',
          'texto' => 'Ola,\r\nAcabamos de assistir a peça Sonharório no SESC Santo Amaro e adoramos.  Com certeza iremos í  nova peça no SESC Pinheiros em Abril.\r\n\r\nObrigada por ótimias gargalhadas.\r\n',
          'created_at' => '2014-03-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Larissa ',
          'email' => 'larissa.sitchin@gmail.com',
          'texto' => 'Construtório é simplesmente incrí­vel!! Uma peça recheada de humor, diversão, criatividade e, principalmente, emoção! Linda mensagem... Lindo final! Amei!!!',
          'created_at' => '2014-04-01 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Flavia',
          'email' => 'flavichaa@gmail.com',
          'texto' => 'Que fantástico a peça Sonhatório no teatro Sesc Santo Amaro, a criação, os atores e a história foram bem produzidas. Por mais que seja peça infantil os adultos como eu ficaram maravilhados! Adorei a participação que tive com os atores no palco, guardarei com muito carinho o remedio e a receita.\r\n PARABENS a todos envolvidos neste lindo trabalho!  ',
          'created_at' => '2014-04-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Maria do Rosário B. Nunes',
          'email' => 'mariarosariob1@gmail.com',
          'texto' => 'Assisti Construtório com minha famí­lia, no último final de semana, no Sesc Pinheiros. Estamos todos encantados com tamanha criatividade, a serviço da poesia! Uma bonita homenagem, ao mesmo tempo, aos operários e í s crianças. estão todos de parabéns. ',
          'created_at' => '2014-04-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fernanda',
          'email' => 'nanda_duraes@hotmail.com',
          'texto' => 'SENSACIONAL! Vi com minha filha hoje no SESC VILA MARIANA, as lágrimas escorriam...mas tb sorri! E minha filha de 7 anos ficou com um semblante doce...reações í  arte incomparável de vcs, grande atores!\r\nMagistral!',
          'created_at' => '2014-03-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Roberto Sanchez Ribeiro',
          'email' => 'rsr1965@gmail.com',
          'texto' => 'Assisti há pouco Construtório e estou em estado de graça. é impecável. Humor na medida certa, uma impressionante criatividade e uma linda mensagem no final: poeta é aquele capaz de brincar. Como os poetas transformam palavras, vocês transformam as coisas mais simples em lindos poemas! Cheguei em casa e brinquei muito com meus filhos, os poetinhas, c omo vcs os chamaram. Obrigado mais uma vez a este incrí­vel grupo de teatro que está como o vinho: cada ano que passa, melhor!',
          'created_at' => '2014-04-06 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rodrigo Kaneno Rabelo',
          'email' => 'rodrigokrabelo@hotmail.com',
          'texto' => 'Assisti Construtorio na semana passada 30/03 com minha esposa e filha, ela gostou tanto que ontem 06/04 tivemos que levar avó, aví´ e prima, todos adoraram..... Chegando em casa minha filha que tem 05 anos recortou um pedacinho de papel, fez uma bandeirinha, pintou, colou em um palitinho, prendeu a uma canequinha de brinquedo e disse que era o navio do pirata.... rsrs... pra completar ficava falando Nemoooooo.... PARABENS a todos vcs !!!',
          'created_at' => '2014-04-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Dayane Gomes',
          'email' => 'dpr.gomes@gmail.com',
          'texto' => '(\"Sonhatória\" - Teatro Municipal de Bauru)\r\nO nome da peça descreve bem. A expressão dos atores, os objetos \"vivos\", a excelente trilha sonora, luzes, entre outros elementos; tudo auxiliou a transportar-me para outro mundo, dotado de outra percepção. Perdi a conta de quantas vezes emocionei-me durante a peça - especialmente com mensagem crí­tica do fim, \"voltar ao trabalho\".\r\nAcredito que teatro é uma grande expressão do ser, do viver. Induz í  reflexão mais importante e bela, a qual questiona a realidade dos paradigmas em que vivemos.\r\n\r\nEspero que esse trabalho maravilhoso continue, e dou todo o meu apoio.\r\nGratidão por esse momento de luz!',
          'created_at' => '2014-04-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Thais Alencar',
          'email' => 'thais.alencar08@yahoo.com.br',
          'texto' => 'Gostaria de parabenizar a Cia. Truks pelo excelente trabalho e por resgatar o humor inocente e principalmente inteligente em nossos palcos. Eu e minha companheira de trabalho Sheila, trabalhamos em um hospital na cidade de Bauru e sabemos da importí¢ncia da brincadeira em nosso local de trabalho e também fora dele. Agradecemos imensamente a contribuição do espetáculo Sonhatório , que nos fez rir, chorar e instigar nossa imaginação e parabenizar os atores e direção pelo trabalho incrí­vel de corpo, voz e criatividade!\r\nAgradecemos também a Oficina do diretor Henrique Sitchin que nos proporcionou um momento de grande aprendizagem e com certeza será de grande valia em nosso trabalho e principalmente em nossas vidasas meninas da cena do romance da pasta de dente e da escova apresentada na Oficina â€œA criação da fantasiaâ€, em Bauru).\r\nAgradeço novamente pela oportunidade e por nos proporcionar tão belo espetáculo.\r\nEstamos na contagem regressiva do retorno da Cia. nos palcos de Bauru!\r\n Grande abraço!\r\n',
          'created_at' => '2014-04-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fernanda Rissardi',
          'email' => 'ferissardi@gmail.com',
          'texto' => 'Pessoal, assisti Por uma Etrela no SESC são Carlos e amei! Minha filha de 5 anos resumiu o espetáculo na seguinte frase: míe com amor tudo compensa. Parabéns a todos!	',
          'created_at' => '2014-04-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cláudia Gotardo',
          'email' => 'crgotardo@ig.com.br',
          'texto' => 'Olá Moro em Jundiaí­ e ontem eu, meu marido e filho fomos de trem a são Paulo especialmente pra assistir ConstrutórioPinheiros). Parabéns pela criatividade, nós três adoramos. não conhecia a companhia e achei diferente, pois a peça praticamente não tem nenhuma palavra.Parabéns!!!',
          'created_at' => '2014-04-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Silvia Regina Barolo Caló',
          'email' => 'silviareginacalo@yahoo.com.br',
          'texto' => 'Assistimos a peça Construtório ontem no Sec Pinheiros e gostarí­amos de elogiar a historia e a representação. Os alunos, assim como nós professores, ficamos maravilhados com a maneira com que abrangeram temas como reciclagem e construção de brinquedos, com materiais simples, de maneira rica e lúdica. Sempre que for possí­vel, podem convidar nossa escola que iremos. Atenciosamente, profas. Silvia e Selma.',
          'created_at' => '2014-04-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Elaine Pirovani',
          'email' => 'elainepirovani@uol.com.br',
          'texto' => 'Como o meu pequeno adora tudo que é relacionado a construção, resolvi levá-lo p/assistir \"CONSTRUTÓRIO\". E não deu outra! ELE AMOU...Deu muita risada, e até nós adultos achamos tudo muito divertido! estão de parabéns...',
          'created_at' => '2014-05-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Adailton M. Gonçalves Freire',
          'email' => 'adail32@gmail.com',
          'texto' => 'Construtório é simplesmente genial! Assisti no último domingo e fiquei absolutamente encantado, pela criatividade sem limites, e inteligente, pelo talento dos três atores, ótimos em cena, pelo roteiro ágil, que não dá tréguas í  plateia. é diversão o tempo todo! Quem piscar vai perder alguma das incrí­veis alegrias que acontecem no palco. Parabéns a vocês todos deste grupo genial que é um oásis de inteligência, poesia e bom humor para as nossas plateias infantis! ',
          'created_at' => '2014-05-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'CLEIDE',
          'email' => 'cleideamaralf@hotmail.com',
          'texto' => 'Assisti hoje aqui em Marilia, o espataculo POR UMA ESTRELA. Realmente sensacional, emocionante e maravilhoso. Chorei um montao. Recomendo a todos uma excelente peca. Um verdadeiro presente.',
          'created_at' => '2014-05-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Roseli de Fatima Cardoso Pereira',
          'email' => 'roseli.fcp@hotmail.com',
          'texto' => 'Sou da cidade de Batatais e estive no SESC Ribeirão Preto acompanhando a escola de minha filha de 6 anos, amei o trabalho de vcs!!!!\r\num trabalho fabuloso que cativa as crianças e também adultos!!!\r\nparabéns pelo trabalho, espero velos em outras oportunidades,abraços a todos. ',
          'created_at' => '2014-05-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Simone Lima',
          'email' => 'si_sieri@yahoo.com.br',
          'texto' => 'Olá, quero dar os parabéns í  Cia Truks, pela ótima e bela apresentação que foi realizada neste fim de semana em Marilia. Amei por demais, simplesmente fantástico. Fomos eu, meu esposo e meus 2 filhos 1 de 13 e outro de 2 anos. Enfim, a casa estava cheia! Gente, sem palavras pra tanta emoção que senti. Parabéns!!!!',
          'created_at' => '2014-05-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luiz Fernando Demarqui',
          'email' => 'luizdemarqui@hotmail.com',
          'texto' => 'Hoje dia 18/05 assisti a peça \"Por Uma Estrela\" no Teatro do Sesi de Birigui/SP e adorei..muito boa e parabéns pelo belo trabalho e pela dedicação.',
          'created_at' => '2014-05-18 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Nathalia Mazza',
          'email' => 'naty.lessi@gmail.com',
          'texto' => 'Eu e minha míe Marcia fomos assistir a peça construtório. Gostamos e rimos muito! Desejo os meus parabéns ao Cia Truks,que continuem sempre assim! abraços!',
          'created_at' => '2014-05-18 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'alzira neli dos santos mosca',
          'email' => 'nelisantosm@bol.com.br',
          'texto' => 'Olá, Eu e minha filha Júlia Helena estivemos na apresentação no SESC são Caetano em 09/07/2014 do espetáculo A Bruxinha. Ficamos simplesmente encantadas. Como é incrí­vel a capacidade que vocês têm de nos deixar boquiabertos com os trejeitos e a simpatia da personagem. O que me chamou muita atenção também foi a expressão dos atores conforme a história se desenrolava. A Júlia gostou muito da bruxinha e do leão. Eu adorei o monstro. Parabéns pelo trabalho maravilhoso. não deixem de trazer tanta alegria pra todos nós. Atenciosamente Alzira Neli e Júlia',
          'created_at' => '2014-07-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Priscila Cruz',
          'email' => 'priscila.cruzs@gmail.com',
          'texto' => 'Construtório - Excelente espetáculo apresentado em Mogi das Cruzes! Parabens pelo show de criatividade e carisma!',
          'created_at' => '2014-05-18 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Margarete Silvestrini',
          'email' => 'margarete.cultura@pmmc.com.br',
          'texto' => 'Linda a apresentação de Construtório em Mogi das Cruzes. Incrí­vel como vocês dão conteúdo a todas as formas. ',
          'created_at' => '2014-05-19 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marli Naka',
          'email' => 'marlinaka@yahoo.com.br',
          'texto' => '\"Por uma Estrela\"! Peça encantadora! Eu, meu marido e meus dois filhos estivemos no SESI de Birigui no dia 18.05.14. Lindo espetáculo, cheio de emoção, de sentimentos! Parabéns! Esperamos assisti-los novamente!',
          'created_at' => '2014-05-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leandro Eleotério',
          'email' => 'leandroec31@gmail.com',
          'texto' => 'Hoje, 25/05/14, fui com minha famí­lia assistir í  peça Por uma Estrela, no teatro municipal de Franca. é a segunda vez que assisto esta peça, e consegui sair mais emocionado do que a primeira vez.\r\né simplesmente maravilhoso. Continuem sempre assim, e que possam voltar sempre í  nossa cidade.\r\nAbraços e parabéns.',
          'created_at' => '2014-05-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'lucia regina moreira',
          'email' => 'luciamoreiraregina@hotmail.com',
          'texto' => 'parabens pela linda peça hoje no teatro municipal de franca foi maravilhoso,emocionante o  trabalho de voces é lindo, eu e minha filha a adoramos continuem assim trazendo um pouco de sonhos e imaginação as pessoas nesse mundo cheio de tristeza aonde as pessoas não tem tempos a elas mesmas. abraços a todos da cia e voltem sempre a nossa cidade.',
          'created_at' => '2014-05-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cristina',
          'email' => 'cristinachavier@hotmail.com',
          'texto' => 'Minha filha de 4 anos se emocionou ao assistir Por uma Estrela. Eu também gostei muito. Parabéns í  toda equipe !',
          'created_at' => '2014-05-31 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Areta',
          'email' => 'aretahe@gmail.com',
          'texto' => 'O espetáculo \"Por uma estrela\" é um show de delicadeza, sensibilidade e talento. Parabéns! Você merecem todo sucesso. Assisti em 31/05/2014 no teatro do Sesi Campinas.',
          'created_at' => '2014-06-01 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lí­gia Andrade',
          'email' => 'LIGIA.ANDRADE@MAGAZINELUIZA.COM.BR',
          'texto' => 'Parabéns à toda equipe de Super Profissionais! Assistimos a peça Por uma Estrela,minha famí­lia e eu. Foi emocionante,chorei muito com a história e com o musical! Perfeito!',
          'created_at' => '2014-05-26 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Priscila Cristine',
          'email' => 'prycristine@gmail.com',
          'texto' => 'Tive o prazer de assistir Por uma Estrela no Sesi Campinas. Fiquei extremamente feliz de poder proporcionar este momento ao meu filho, de 2 anos e 8 meses. Que coisa mais linda! Quanta sensibilidade! Me emocionei várias vezes!',
          'created_at' => '2014-06-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Angelo Cesar',
          'email' => 'angelocm@yahoo.com.br',
          'texto' => 'Ola parabéns a cia truks e a toda a equipe,tive o prazer de assistir o espetaculo ÚLTIMA NOTICIA,galera é muito bom muito obrigado e parabéns de novo .',
          'created_at' => '2014-06-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Evelize Rodrigues Matias Marques',
          'email' => 'evelize.luiz@hotmail.com',
          'texto' => 'Assistimos a peça Sonhatrio hoje06/14) no Sesc Bauru na sessão das 11:00h e ficamos encantados com o desenvovimento dos atores e os efeitos lúdicos criados.Voltamos no mesmo dia ,agora a familia toda míe e filhos) na sessão das 16:00h e novamente foi maravilhoso.A alegria de vocês é contagiante,que Deus os abençoe muito e os conserve sempre assim,animando nossas vidas,parabéns e obrigada!!!!!!!!!!!!!!',
          'created_at' => '2014-06-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Maria do Rosário',
          'email' => 'mrosario1@gmail.com',
          'texto' => 'Olá, Cia Truks!\r\nAssistimos Última Notí­cia no Sesc e ficamos encantados. Fora ser muito divertida e comovente, a peça incentiva com inteligência as relações de pais e filhos. Ah, se todos os pais brincassem assim com sesu filhos! estão de parabéns!',
          'created_at' => '2014-06-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Bruna Lais',
          'email' => 'sj2.bruna@gmail.com',
          'texto' => 'Assistimos há alguns anos a peça Cidade Azul e ontem assistimos Última Notí­cia no Sesc Santana e adoramos! Foi tudo muito lindo e divertidí­ssimo! Super recomendo! ',
          'created_at' => '2014-06-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luciana Moscardi Grillo',
          'email' => 'lmgrillo@gmail.com',
          'texto' => 'No último domingo tivemos a gratificante experiência de conhecer o trabalho da Cia. Truks, que se apresentou no SESC Bauru com a peça Sonhatório. Eu e minha filha de seis anos ficamos encantadas. O roteiro é muito inteligente e a mensagem emocionante. Os atores foram incrí­veis e souberam tocar adultos e crianças. Rimos e nos envolvemos. Foi maravilhoso descobrir que nossa cozinha guarda um universo de possibilidades, e que brincar, de fato, é o melhor remédio! Parabéns e que Bauru possa continuar recebendo arte dessa qualidade e envergadura. Abraço. Luciana e Mariana.',
          'created_at' => '2014-06-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Juliana Basani',
          'email' => 'ju_basani@yahoo.com.br',
          'texto' => 'O Espetáculo \"A Última Notí­cia\" é maravilhoso. Já conhecia  \"A Bruxinha\" e \"O Senhor dos Sonhos\" e pude notar que as histórias só melhoram com os anos! Vocês nos fazem rir e chorar de emoção!!! As crianças participam de forma surpreendente e espontí¢nea e os adultos voltam no tempo e viram crianças outra vez! Parabéns a toda a equipe!!! é incrí­vel a maneira com que vocês dão vida aos personagens e nos trazem mensagens tão importantes! Recomendado para qualquer idade!! ',
          'created_at' => '2014-07-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Rita e Lucas Araujo',
          'email' => 'anaelucas7@hotmail.com',
          'texto' => 'hoje 15/6 assistimos ao espetáculo \"Sonhatório\" no teatro de Franca SP, eu e meu filho de 11 anos adoramos pois a peça é muito rica em criatividade os atores são um espetáculo por si só! Adoramos parabéns voltem mais vezes com outras peças, pois não perderemos.\r\nAna Rita e Lucas Araujo - 15/06/2014',
          'created_at' => '2014-06-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Adriana',
          'email' => 'd.rika2704@hotmail.com',
          'texto' => 'Hoje foi um dia especial, levei meu filho de 4 anos ao teatro pela primeira vez. E foi uma experiência gratificante, o espetáculo Última Notí­cia é lindo e divertido. Rimos muito, e o Daniel não pará de falar na baleia engolindo o tubarão, rsss. Parabéns pelo trabalho! Continuem em frente trazendo momentos alegres por onde passam',
          'created_at' => '2014-06-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Maria Elena',
          'email' => 'bolavermelha@ig.com.br',
          'texto' => 'Boa noite, tive a oportunidade de assistir ao espetáculo \"Cidade Azul\" no Céu Vila Formosa esta semana, fiquei maravilhada, parabéns pelo trabalho, tudo muito lindo, perfeito. \r\nQue os \"ANJOS\" estejam sempre iluminando vossos corações.\r\n\r\nFelicidadessssssssssssssssssssssss',
          'created_at' => '2014-06-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Eliane de Castro',
          'email' => 'elicst@uol.com.br',
          'texto' => 'Ola, levei meu filho hoje  no sesc santana.Adoramos  o Ultima Noticia.Emocionante!Esperamos que voltem ao Sesc santana. Obrigada por alegrar nosso domingo. Eliane',
          'created_at' => '2014-06-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Irineu - 22/06/2014',
          'email' => 'idammjr@gmail.com',
          'texto' => 'Fantástico, a apresentação do Grupo Truks no SESC Santana no espetáculo Última Notí­cia, minhas filhas curtiram muito. Parabéns pelo excelente trabalho.',
          'created_at' => '2014-06-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Duda e Leandro',
          'email' => 'duda.caetano@uol.com.br',
          'texto' => 'Parabéns í  cia. pelo lindo espetáculo \"O Senhor do Sonhos\". é encantador. Nosso filho amou e quando acabou disse \"mas foi muito rapidinho\". Para uma criança de 4 anos, que não para um segundo e ficou sentada na cadeira o tempo todo, é um senhor elogio. ',
          'created_at' => '2014-06-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fernando Teixeira',
          'email' => 'fernando.teixeira@magazineluiza.com.br',
          'texto' => 'Assisti ao espetáculo SONHATÓRIO no último dia 15 em Franca, promovido pela empresa Magazine Luiza aos seus funcionários. Fui surpreendido por muita criatividade, linguagem poética de simples entendimento principalmente para o público mirim, uma capacidade de \"criar\" com pouquí­ssimas palavras, figurino e objetos cenográficos. Parabéns a toda a equipe! Que a luz da imaginação continue acesa iluminando a criatividade de todos vocês.',
          'created_at' => '2014-06-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ivanilda Coimbra de Souza',
          'email' => 'ivanildacoimbrasouza@gmail.com',
          'texto' => 'Última Notí­cia: Lindo, maravilhoso, delicado, vibrante, criativo e emocionante!!! Um espetáculo para se guardar no coração de tão lindo!!! Parabéns pela atuação de todos esses artistas, impecáveis!!! Parabéns, bravo, bravo!!!',
          'created_at' => '2014-06-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'alzira catony',
          'email' => 'acattony-origami@bol.com.br',
          'texto' => 'Última Notí­cia- momentos de verdadeiro encantamento. Voltei no relógio do tempo dos 79 para os 9 anos. Parabéns!',
          'created_at' => '2014-06-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Joca M Castello',
          'email' => 'jocamcas@gmail.com',
          'texto' => 'Fui hoje ao Sesc Santana com meu filho de 5 anos, assistir A ÚLTIMA NOTíCIA. Ele se divertiu com tudo e eu saí­ muito comovido, pensando em como é bom brincar com meu filho. Nosso esconde esconde nunca mais será o mesmo. Obrigado!',
          'created_at' => '2014-06-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sergio Murilo Ribeiro',
          'email' => 's.muriloribeiro@gmail.com',
          'texto' => 'Olá queridos da Cia. Truks, é com muito prazer que deixo essa mensagem, parabenizando a todos envolvidos. Vi o espetáculo \"Construtório\" na cidade de Limeira, e o resultado não poderia ser outro além de muitos risos e lágrimas de emoção. Espalhei para meus amigos e continuarei a divulgar esse trabalho de inestimável valor. Um grande abraço ',
          'created_at' => '2014-06-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Carolina A.',
          'email' => 'carolina.arakawa@ig.com.br',
          'texto' => 'Uma tarde no SESC em sao jose do rio preto/sp. Duas filhase 5 anos). Uma peça: Construtorio. Risos e alegria, com a fantasia estimulada, e o brincar de \"faz de conta\" incentivado - envolvimento pleno da plateia, de todas as idades. Parabens pelo belo trabalho - espero que \"até breve\"!	',
          'created_at' => '2014-06-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Giovana Carnerio Farina',
          'email' => 'giovanacfarina@gmail.com',
          'texto' => 'Fui ontem ao Sesc-Bauru com meu filho Rafael de 6 anos assistir o espetáculo \"O senhor dos sonhos\" e adoramos. Foi muito lindo e divertido! \r\nParabéns í  Cia. Truks!!!',
          'created_at' => '2014-06-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lilian e meu  filho Helian ',
          'email' => 'lili_tst@yahoo.com.br',
          'texto' => 'Fui ao Sesc-Bauru com meu filho Helian de 4 anos e meio assistir o espetáculo  \" Zoiológi-co\"  e o \" O senhor dos Sonhos\" domingo passado e hoje iremos novamente, foi maravilhoso o meu filho quer ir ver toda semana  o teatro é mágico ,nos faz rir gostoso, nos faz nos emocionarmos, nos faz chorar. Vocês são Maravilhosos tudo lindo e perfeito eu fico encantada de ver meu filho concentrado e surpreso com cada cena ,fala e tudo o que acontece com os personagens, Cia.Truks vocês são abençoados por Deus . Muito Obrigado! Muito obrigado!',
          'created_at' => '2014-07-06 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Roberto Kishi',
          'email' => 'robertokishi@uol.com.br',
          'texto' => 'Parabéns í  Cia. Truks pelo excelente espetáculo A ÚLTIMA NOTíCIA, uma das melhores e mais envolventes peças infantis que já assisti com minha famí­lia. Recomendo.',
          'created_at' => '2014-07-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Renata Dalfré',
          'email' => 'renata.dalfre@yahoo.com.br',
          'texto' => 'Olá, recentemente assisti ao Espetáculo Construtório na cidade de Limeira. Venho para elogiar mais uma vez o trabalho incrivel que vcs fazem. é facil de perceber o amor que vcs tem em apresenta-lo! é tão maravilhoso esse resgate que acontece durante o espetáculo, nas brincadeiras simples, na diversão que não precisa de muito mais que amizade e criatividade para acontecer! Estou encantanda, mais uma vez, foi ótimo! Parabéns meninos, vcs são ótimos! ',
          'created_at' => '2014-07-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alzira Neli dos Santos Mosca',
          'email' => 'nelisantosm@bol.com.br',
          'texto' => 'Olá, Eu e minha filha Júlia Helena estivemos na apresentação no SESC são Caetano em 09/07/2014 do espetáculo A Bruxinha. Ficamos simplesmente encantadas. Como é incrí­vel a capacidade que vocês têm de nos deixar boquiabertos com os trejeitos e a simpatia da personagem. O que me chamou muita atenção também foi a expressão dos atores conforme a história se desenrolava. A Júlia gostou muito da bruxinha e do leão. Eu adorei o monstro. Parabéns pelo trabalho maravilhoso. não deixem de trazer tanta alegria pra todos nós. Atenciosamente Alzira Neli e Júlia',
          'created_at' => '2014-07-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'davi de oliveira pinto',
          'email' => 'ddolpi@gmail.com',
          'texto' => 'Prezados da Cia. Truks,\r\n\r\naqui é o Davi, sou professor do Departamento de Artes Cênicas da Universidade Federal de Ouro Preto e assisti hoje, quinta, 16/07, ao \"Zoo-ilógico\", do qual gostei muito.\r\n\r\nFui eu quem pedi a vocês que tirassem foto com algumas crianças alunas de uma escola pública do subdistrito do Mota, pertencente ao distrito Miguel Burnier, pertencente a Ouro Preto.\r\n\r\nA inciativa de levar essas crianças para ver o trabalho de vocês está dentro de um projeto de extensão no qual, entre outras coisas, fazemos mediação teatral.\r\n\r\nEu quero agradecer a gentileza e generosidade de vocês, tirando a foto conosco, obrigado mesmo!\r\n\r\nQuanto ao espetáculo, me chama a atenção a qualidade de vocês como palhaçosos vejo assim), a afinação da dupla em cena, e o modo poético como vocês vão dando vida a objetos do cotidiano para \"transformá-los\" em animais.\r\n\r\nUm espetáculo singelo, divertido, bom de ver!\r\n\r\nOutra coisa que me agradou foi o fato de não haver muitas palavras em cena, a imagem tem o maior foco.\r\n\r\nEnfim, é muito bom quando a gente vai ao teatro e encontra teatro bom, de qualidade, a gente sai feliz, \"alimentado\" pela experiência da beleza, da vibração, do humor e da delicadeza!\r\n\r\nQue vocês continuem encantando, emocionando a alegrando muitas e muitas plateias!\r\n\r\nParabéns a todos!\r\n\r\nAbs\r\nDavi',
          'created_at' => '2014-07-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Tatiana Vanzo',
          'email' => 'tativanzo@yahoo.com.br',
          'texto' => 'Assisti ao espetáculo Por uma estrela,com minha filha, no SESC são Carlos. Foi uma das melhores peças que vi recentemente. Delicada,emocionante e criativa. Uma história belí­ssima contada com expressíµes, gestos e música. Parabéns!!',
          'created_at' => '2014-07-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Carolina Schmaedecke',
          'email' => 'caro_schmae@terra.com.br',
          'texto' => 'Assistimos ao espetáculo \"A Última Notí­cia\" no SESC Santana. Saí­mos eu, meu marido e minha filha de 1 ano e meioachei que não fosse nem prestar muita atenção) encantados. Eu já sabia da qualidade dos bonecos, pois conheço o Dalmir, mas fiquei impressionada com a emoção que os atores conseguiram nos passar. Vocês nos deixaram com um gostinho de quero mais...',
          'created_at' => '2014-07-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Giovanna Pascale',
          'email' => 'giovana.pascale@colegiocil.com.br',
          'texto' => 'Quero parabenizar a todos pela bela apresentação de hoje, do espetáculo ÚLTIMA NOTíCIA, no Sesc Santana. Talento inquestionável, prenderam a atenção do começo ao fim de uma criança de 1 ano e 2 meses. Obrigada pela oportunidade de assistir a um espetáculo tão rico com uma mensagem tão linda. Vida longa a todos!!!',
          'created_at' => '2014-07-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Monalisa Lins',
          'email' => 'monalisalins@hotmail.com',
          'texto' => 'Quem disse que só os grandes estúdios de cinema sabem fazer histórias para crianças e adultos? Ontem fomos com nossos filhos ao espetáculo últimas Notí­cias e.. As crianças saem sorrindo e os pais saem chorando! é mesmo uma linda história que fala, entre outras coisas, da relação adulto e criança, com as crianças que estão ao nosso redor e principalmente aquela que está do lado de dentro! Adoro esta Companhia!',
          'created_at' => '2014-07-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Natália Bragato',
          'email' => 'nati_bragato@hotmail.com',
          'texto' => 'Assisti o ontem o espetáculo a \"A última notí­cia\" no Sesc Santana e sem dúvida Amei, admirei e me emocionei com o espetáculo! Gostei de tudo, dos bonecos, da criatividade da confecção dos bonecos feitos em jornalcomprometimento com o meio ambiente) e sem dúvida os atores que deram vida a tudo isso estão de muito Parabéns!! essa cia é incrí­vel e recomendo!! semana que vem no ultimo final de semana da temporada estarei com a famí­lia toda e amigos!! muito obrigada por essa experiência lúdica e fantástica!!! ',
          'created_at' => '2014-07-21 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sandra Lara',
          'email' => 'sc.lara@uol.com.br',
          'texto' => 'Gostaria de agradecer a presença do grupo no Encontro de Educadores do Colégio singular em Santo André. Foi mágico!!! Todos ficaram encantados!\r\nParabéns pelo trabalho!!!',
          'created_at' => '2015-09-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'KARLA FREITAS',
          'email' => 'karla.freitas01@hotmail.com',
          'texto' => 'Parabéns,adoramos a peça a ÚLTIMA NOTíCIA,bem feita,todos estão de parabéns mesmo!!!!!!!!!!!!!!!!!!!',
          'created_at' => '2014-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leonardo Alexandre Lima Sales',
          'email' => 'leonardo.alexandre.sales@gmail.com',
          'texto' => 'Tenho cinco anos e gostei de tudo. Fiquei com um pouquinho de medo, mas depois tudo deu certo. Eu também invento histórias na minha imaginação e gostei do menino, acho que dá para fazer o boneco na minha casa',
          'created_at' => '2014-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Keli cristina',
          'email' => 'criskeli99@yahoo.com.br',
          'texto' => ' Minha famí­lia e eu ficamos maravilhados hoje com a peça \r\nzí´o-ilógico vocês estão de parabéns pelo belo trabalho amamos .',
          'created_at' => '2014-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andrea',
          'email' => 'akyanik@hotmail.com',
          'texto' => 'Assistimos hj a peça \"última notí­cia\" no sesc Santana. Descobri a peça pela divulgação na veja SP. No site do sesc não constava a peça na programação. Gostamos bastante da apresentação. A expressão dos atores e muito boa. Queremos acompanha-los sempre. E bom conhecer trabalhos de boa qualidade destinados ao público infantil. Parabéns pela dedicação! Como míe agradeço a contribuição de vcs no desenvolvimento da cultura dos nossos pequenos. Obrigada.',
          'created_at' => '2014-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Karina Cardozo',
          'email' => 'khmcardozo@gmail.com',
          'texto' => 'Adoramos a peça \" A última noticia\" que assistimos ontem no sesc Santana. Lemos a respeito da peça no aplicativo da vejasp. Muito bem recomendada lá, só temos elogios a dizer e que todas as expectativas foram superadas. Os atores são excelentes e a ideia de fazer todo o cenário com papel é fantástica. Meu filho de 4 anos prestou atenção o tempo todo e comentou muito da peça depois com outras pessoas. Parabéns!!! Vamos seguir o grupo por onde ele estiver...',
          'created_at' => '2014-07-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Paula Dias',
          'email' => 'anapauladias@mynutry.com.br',
          'texto' => 'Ver o brilho nos olhos do meu filho proporcionado pelo encantamento e beleza dessa montagem ainda que ao final me levou as lágrimas, não tem como definir se não de Sensacional! O universo lúdico cheio de criatividade e simplicidade. Parabéns ao elenco e toda equipe.',
          'created_at' => '2014-07-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'João Carlos Cordeiro',
          'email' => 'irmaoscordeiro@ig.com.br',
          'texto' => 'Tivemos o imenso prazer de assistir hoje a peça Sonhatório, que de peça infantil conseguiu ser maiúscula e transmitiu uma mensagem fantástica, e como pai fiquei emocionado,pois tive o prazer de subir ao palco no encerramento do espetáculo, e vi meu filho sorrindo de forma impagável, o que me demostra que estou no caminho certo, mostrando a ele o teatro. Obrigado aos atores Gabriel Sitchin, Rafael Senatore e Rogério Uchoas que transmitiram e atingiram o objetivo de emocionar o publico. PARABéNS!!!!',
          'created_at' => '2014-08-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andrea Regina Vidal Costa',
          'email' => 'andreavidalcost@hotmail.com',
          'texto' => 'Boa noite! Hoje, no Sesc de Sao Jose dos Campos, tive a satisfação, o prazersei como me expressar) de conhecer a Cia Truks, com a peça Sonhatório. Que trabalho MARAVILHOSO!!! Quanta criatividade, quanta diversão. não sei quem ria mais, se eram as crianças ou os adultos, apesar de a peça ser direcionada para as crianças. Acho que parabéns seria pouco para dizer a voces... Tudo, sem exceção de nada, muuuuito bom. Desejo sucesso, reconhecimento e que vocês nao parem nunca com esse trabalhoengraçado, e acredito que sério ao mesmo tempo, devido a qualidade do trabalho). Podia escrever muuuito mais para falar bem de voces... mas para finalizar, agradeço por voces terem proporcionado a mim, a minha míe e ao meu sobrinho de 4 anos, uma tarde mais que especial. Tenho certeza que isso ficará registrado de uma forma muito gostosa na cabecinha dele, que saiu falando que a tarde foi muito divertida!!!! Muití­ssimo obrigado e um super parabéns a todos os envolvidos e principalmente para os artistas. Espero, de coração que chegue até eles esse recadinho que deixo aqui com muito carinho. Andrea',
          'created_at' => '2014-08-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Larissa Freire',
          'email' => 'lari.freire@yahoo.com.br',
          'texto' => 'Vi hoje na biblioteca Monteiro Lobato a peça \"Última notí­cia\".não tenho o que falar, foi realmente incrí­vel! Emocionante, completamente maravilhoso. Parabéns aos artistas!',
          'created_at' => '2014-08-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sofia Solano',
          'email' => 'Fernandosolanop@gmail.com',
          'texto' => 'Oi. Eu adorei o seu show. Fiquei emocionada por que este show foi maravilhoso. Obrigada e parabéns. Sofia 6 anos',
          'created_at' => '2014-08-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Lorna',
          'email' => 'lornarossi@ig.com.br',
          'texto' => 'Quero parabenizar o grupo pelo trabalho maravilhoso que realizam....levava meus alunos todos os anos para assistir seus espetáculos ....agora levo í s crianças de uma comunidade em que trabalho como voluntária....sou fí de carteirinha!!!!!Amei !!!!!',
          'created_at' => '2014-08-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Laszlo Kis',
          'email' => 'kis.laszl@gmail.com',
          'texto' => 'Ola caros amigos, sou um pai de dois prí­ncipes.\r\nNos assistimos a peça de vcs hoje.\r\nFoi um presente para todos nos e passo dizer que a peça esta continuando ao nosso redor, relembrando o dinossauro, o dragão o chule.....gostaria de expressar o meu profundo gratidão e carinho e mal posso agradeçer com palavras.\r\nGrato por as lagrimas, sorrisos e pessoalmente  por me lembrar que brincar so pode ser com todo coração!\r\n\r\nUm grande abraço para todos\r\n\r\nLászló Kis\r\n',
          'created_at' => '2014-08-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Geraldo Rodrigues Nogueira Junior',
          'email' => 'groisnoju@hotmail.com',
          'texto' => 'Queria compartilhar com vocês o quão grato sou por ter tido a honra de prestigiar a peça de vocês nesta ultima sexta-feira dia 08.08 em Pindamonhangaba. \"Construtório\" assim como \"Sonhatório\" mexeu muito comigo, é lindo ver quão esplendido é o trabalho de vocês. Pude voltar a infí¢ncia e me emocionar. E o mais prazeroso foi ouvir as Gargalhadas das crianças naquele teatro... vocês estão de parabéns e preciso ver isso mais vezes.',
          'created_at' => '2014-08-12 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marisa Martinez Solano Pereira',
          'email' => 'marisa.solano@hotmail.com',
          'texto' => 'Levei minhas netas no teatro da Biblioteca Monteiro Lobato, e fui premiada com a apresentação da peça Ultima Noticia, de vcs. Maravilhosa, emocionante ...lágrimas aos meus 61 anos, unida a emoção de Sofia com 6 anos e Helena com 4 anos. Voltei na ultima apresentação, domingo dia dos pais, com minhas netas  e meu neto Fábio, de 4 anos, que amou...\r\nSentiu medo, alegria, tristeza por deixar o boneco... Assoprou o balão, aplaudiu, gritou, e com lágrimas nos olhos queria saber mais sobre os sonhos...\r\nAgradeço a Deus essa oportunidade! Parabens!!! muito sucesso!',
          'created_at' => '2014-08-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Natasha',
          'email' => 'natashan71@hotmail.com',
          'texto' => 'Apenas mais uma para dizer que amo o trabalho de vocês. Já assisti com minhas filhas as peças Sonhatório,Construtório e a Última Notí­cia.E ,é claro, chorei nas três.não tem como descrever o que vocês fazem, é mágico,indescrití­vel...Espero que continuem com esse trabalho lindo por muito tempo.Na última peça tivemos a alegria de ter meu marido no palco.Minhas filhas ficaram empolgadí­ssimas e guardam o kit do Sonhatório com muito carinho.Parabéns em especial ao Gabriel, Rafael e Rogério por tamanha  sensibildade e por nos fazer sentir o quanto vocês amam o que fazem.Vocês são os melhores.',
          'created_at' => '2014-08-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Yasmin',
          'email' => 'yasmin.paradab@yahoo.com.br',
          'texto' => 'eu fui em tres peças com a minha irma e a minha mae.Eu achei incrivel!quando a gente assiste a gente percebe que cada detalhe da peça é especial , feito com muito carinho e ...Muitos parabens Cia Truks !Desejo muito alegria a todos !          bjs',
          'created_at' => '2014-08-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Paula Nascimento',
          'email' => 'anapaulajornalista@yahoo.com.br',
          'texto' => 'Olá, pessoal! Vimos a peça Sonhatório e achamos muito bonita e engraçada. As crianças acharam incrí­vel como vcs conseguem criar tantas coisas malucas. A mensagem que passam é tb muito sensí­vel e importante, principalmente nos dias de hoje em que os pais estão sempre correndo. Parabéns e sucesso.  Beijos. ',
          'created_at' => '2014-09-04 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andrea Mesquita Martins',
          'email' => 'andreamesquitamartins@hotmail.com',
          'texto' => 'Pela Segunda vez assistimos o espetáculo &quot;Última Notí­cia&quot;, e mais uma vez ficamos emocionados e satisfeití­ssimos com o espetáculo. \r\nParabéns. Estávamos na primeira fileira nas duas vezes e meu filho de dois anos e meio não parava de dar tchau e interagir com a peça. DEMAIS, INCRíVEL. Assistiria muito mais vezes.Abç',
          'created_at' => '2014-09-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'izilda caparellii',
          'email' => 'izildacaparellii@gmail.com',
          'texto' => 'Amei vcs. A peça Sonhatorio é linda , a mensagem que vcs transmitem é maravilhosa; as crianças tem que saber desde cedo que o importante é brincar e ser feliz, alegria de viver é tudo, e isso é saúde. Bjos',
          'created_at' => '2014-09-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Jabis Dacsander Roncato',
          'email' => 'jabis44@yahoo.com.br',
          'texto' => 'Eu, mamãe e Heitor, parabenizamos pela peça &quot;Última Notí­cia&quot;. Cenário maravilhoso e criativo; Expressão corporal e &quot;dramática&quot; dos artistas, davam a interpretação aos personagens sem vida. Da água fomos para a terra, da terra para a lua e voltamos para a terra ... vivenciamos a peça!!! As crianças interagiram e por &quot;tabela&quot; os adultos também. PARABéNS.',
          'created_at' => '2014-09-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Elba Santos',
          'email' => 'basantos1978@hotmail.com',
          'texto' => 'Parabéns pelo trabalho incrí­vel que vcs fazem. Eu, meu esposo e meu filho nos divertimos muito com vcs no Sesc Itaquera. Obrigada por fazerem o nosso domingo muito mais feliz. ',
          'created_at' => '2014-09-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Solange Nascimento',
          'email' => 'solange_sdn@hotmail.com',
          'texto' => 'Excelente espetáculo! Assisti Construtório com minha familia no Sesc Santo André e foi surpreendente. Minha filha de 7 anos amou, demos muitas risadas. Obrigada por nos proporcionar essa diversão tão saudavel. ',
          'created_at' => '2014-09-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leandro Lima',
          'email' => 'leandro.brasil@ig.com.br',
          'texto' => 'Obrigado pela tarde fantástica dada a nós no SESC Santo André. Que o riso seja sempre a assinatura da dupla. Saibam que nossa imaginação foi renovada a ver o que vocês fazem com tanta paixão e criatividade. Com imensa gratidão, Leandro e Famí­lia.Zoo-Ilógico )',
          'created_at' => '2014-09-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fabiana Lona',
          'email' => 'fabianalona09@yahoo.com.br',
          'texto' => 'Fomos assistir ao espetáculo &quot;Construtório&quot; no Sesc Belenzinho. Simplesmente espetacular, assim como o espetáculo &quot;Sonhatório&quot;. Atores fantásticos. Meus filhos ficaram fascinados! Parabéns!!!! Muito sucesso para vcs.',
          'created_at' => '2014-09-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rafael Bernardi',
          'email' => 'rafaelbernardi33@gmail.com',
          'texto' => 'Cara, vocês estao de parabéns, assisti uma de suas apresentaçoes no Sesi e dei muita risada, Parabéns pelo trabalho continue assim !!!\r\n',
          'created_at' => '2014-10-02 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Claudio Blanco',
          'email' => 'blanco_claudio@hotmail.com',
          'texto' => 'Assisti hoje a apresentação de vocês no Sesc Campo Limpo com o meu filho, adoramos, ele fez questão de cobrar eu para colocar a mensagem para vocês.',
          'created_at' => '2014-10-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Danila',
          'email' => 'contato@danilamn.adv.br',
          'texto' => 'Ontem assisti com meu filho Pedro o espetáculo Cidade Azul, no Ribeirão Shopping. Foi simplesmente mágico, emocionante. Parabéns a todos que contribuí­ram.\r\n ',
          'created_at' => '2014-10-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Jaira, Cidinho e Rebeca',
          'email' => 'jaira.lima95@gmail.com',
          'texto' => 'Ontem assistimos a peça &quot;Construtório&quot;, no Teatro Leopoldo Froes, adoramos, minha filha de 3 anos interagiu em todo o tempo e ficou brava quando acabou, acho que nem preciso dizer mais nada né?? Parabéns e espero ver muitos outros trabalhos de vocês!!',
          'created_at' => '2015-06-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Andrea Duarte Euzebio dos santos',
          'email' => 'andrea.euzebio@hotmail.com',
          'texto' => 'Boa noite a todos do espetáculo Cidade Azul, no qual se apresentou no Sesc Campo Limpo.Eu amei o espetáculo me emocionei muito, tanto que ate me interessei em fazer um curso para o mesmo.Parabéns.Vocês são sensacional. Andréa Euzebio-12/10/2014',
          'created_at' => '2014-10-12 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Kelly',
          'email' => 'kellyunesp@gmail.com',
          'texto' => 'As crianças e adultos de Registro no Vale do Ribeira, SP ficaram encantados com o espetáculo proporcionado por vocês no dia das Crianças! Parabéns, maravilhoso!',
          'created_at' => '2014-10-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Priscila Pedrino',
          'email' => 'prijacobucy@gmail.com',
          'texto' => 'Pela primeira vez assistimos a peça &quot;Ultima Notí­cia&quot;, e adoramos!! Um lindo trabalho da equipe, nos emocionou e fez querer assistir todas as outras peças. Parabéns, vocês são demais!!',
          'created_at' => '2014-10-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rita Cristina',
          'email' => 'rcris-costa@hotmail.com',
          'texto' => 'Esse ano assisti com meus filhos a peça: Sonhatório no Teatro Mario Covas em Caraguatatuba, amamos a peça!! Nos divertimos muito e a nossa surpresa foi saber que estariam de volta no Litoral Encena. Assistimos a peça: Construtório e nos divertimos muito! Parabéns a toda equipe e continuem sempre nos surpreendendo com essa criatividade. Espero revê-los em 2015 com a nova peça ... ;) Um grande abraço!! ',
          'created_at' => '2014-10-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Adriana e Marcos',
          'email' => 'adriana_kath@hotmail.com',
          'texto' => 'Hoje assistimos com a nossa filha o espetáculo O Senhor dos Sonhos na IBAB, ficamos encantados a minha filha se emocionou com a história...tudo muito lindo. Queremos agradecer toda a equipe e a IBAB por nos ter dado a oportunidade de conhecer um trabalho tão maravilhoso com uma otima reflexão tanto para os filhos quanto para os pais. SHOW!!!!!',
          'created_at' => '2014-10-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Pedro Martinelli',
          'email' => 'pedromrnunes@uol.com.br',
          'texto' => 'Ontem eu assiti a peça sonhatório í  convite de uma amiga. Eu nunca tinha ouvido falar sobre vocês até então. Me diverti muito e senti que toda a platéia também.\r\nQuero dar os parabéns por todos os responsáveis por essa iniciativa que agradou todas as idades.\r\nAos que ainda não conhecem, não percam!',
          'created_at' => '2014-10-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'ROSANE MARIA',
          'email' => 'romasi29@yahoo.com.br',
          'texto' => 'PARABéNS,ESTOU VINDO DO SESC ONDE ASSISTI A BRUXINHA DA EVA FURNARI,ADOREI MUITO FOFO.OBRIGADO POR VC TEREM VINDO E O SESC NOS PRESENTIR.VOLTEM LOGO.\r\n\r\nBEIJOS A TODOS E PRINCIPALMENTE PARA A BRUXINHA\r\nROSANE MARIA',
          'created_at' => '2014-11-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Virna Karla',
          'email' => 'vi-vika@hotmail.com',
          'texto' => 'Hoje assisti com meu filho a peça A bruxinha no Sesc Catanduva-SP, adoramos...uma teatro de alto ní­vel em nossa cidade....lindo trabalho....Parabénssss...',
          'created_at' => '2014-11-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Arlete',
          'email' => 'arletebernardi@terra.com.br',
          'texto' => 'Ontem levei meu filho e meus sobrinhos para assistir a peça &quot;A Bruxinha&quot; no Sesc Catanduva. Foi ótimo, gostamos muito. Vcs estão de parabéns!!!',
          'created_at' => '2014-11-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'PEDRO GUIMARíƒES',
          'email' => 'pedroguimaraesjr@gmail.com',
          'texto' => 'Olá! Sou um ator de Fortaleza, de 28 anos, e estive de férias em são Paulo no começo de dezembro. Entrei em uma maratona teatral e tive o prazer de descobrir vocês em Última Notí­cia. Venho parabenizar todo o grupo pelo trabalho liiiindo e sensí­vel que encantou a mim e todos, crianças e adultos, da plateia. Por favor, continuem com esse poder de nos emocionar. E desejo revê-los em breve; quem sabe aqui no Ceará? Um forte abraço e desejos de sucesso e trabalho neste 2015.',
          'created_at' => '2015-01-04 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'patricia oliveira',
          'email' => 'pati_vida12@hotmail.com',
          'texto' => 'eu me chamo patricia e moro em bertioga sempre que vcs estao na casa de cultura com suas peças eu vou e levo meus filhos vcs estao de parabéns sao incriveis as peças muito bom as crianças adoram e ficam vidradas muito bom parabens a todos vcs venham mais vezes em bertioga um abraço ',
          'created_at' => '2014-11-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'sandra',
          'email' => 'sandfernandes.matos@gmail.com',
          'texto' => 'Olá,pessoal da Truks! Sou a Sandra,míe da Beatriz.Tivemos o privilégio de assistir ao espetáculo &quot;Construtório&quot; no teatro Martins Penna e ficamos maravilhadas com a performance. Parabéns!',
          'created_at' => '2014-12-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Roberval Herminio',
          'email' => 'rhsol@bol.com.br',
          'texto' => 'Olá eu sou o Roberval pai da Julia e da Raquel estivemos hoje pela manhã na EMEB Cora Coralina em são Bernardo e nos divertimos com a história do menino sonhador que se tornou escritorsenhor dos sonhos)... Parabéns pelo excelente trabalho que além de divertido é encorajador e traz muitas lições... Valeu pessoal até a próxima ',
          'created_at' => '2014-12-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leonardo Medeiros',
          'email' => 'walmir.medeiros@gmail.com',
          'texto' => '&quot;Muito obrigado pelo teatro. A telinha do \'ratão\' é para o homem de dentro ver. O \'ratão\' não existe! A bruxinha é legal e gostei do plano do Leão. Gostei muito. O \'ratão\' ficou do bem!&quot;. Essas foram as palavras do meu filho Leonardoanos). Ele gostou da apresentação que vocês fizeram em Taubaté dia 11/1/2015. Ficou impressionado com o &quot;homenzão&quot; que ele chamou de &quot;ratão&quot; por isso dissemos que &quot;é teatro e ele é apenas  um personagem&quot;. A cia. de vocês é famosa e fizemos questão de levar nosso filho para vê-los. Parabéns pelo trabalho. Sucesso sempre.',
          'created_at' => '2015-01-12 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Janaina Teixeira',
          'email' => 'janrochas@uol.com.br',
          'texto' => 'Meus filhos e eu assistimos o Zoo-Ilogico, resumindo: demais!!! Inteligente, diní¢mico, surpreendente!!! Parabens!!! Meu filho de 3 anos no meio da apresentação: &quot;Mamae, estou adorando&quot; .',
          'created_at' => '2015-01-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'ANGELA DENISE',
          'email' => 'denisevc@ig.com.br',
          'texto' => 'vcs fizeram a diferença no meu dia de hoje....estou sorrindo até agora....obrigada!',
          'created_at' => '2015-01-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => ' Amauri',
          'email' => 'Amauri.guelmar.silva4@gmail.com',
          'texto' => ' Parabéns, pelo espetáculo de hoje, no Sesc Araraquara. \r\nVoltem sempre!',
          'created_at' => '2015-02-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Janaina ',
          'email' => 'janainalobo@hotmail.com',
          'texto' => 'Muito bom!!! Peça construtório, meu filho de 6 anos adorou, ficou esperando a 2a parte da peça..kkkk\r\nsuper criativo, lindo, objetos simples ganham vida! obrigada',
          'created_at' => '2015-02-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Israel Batista Saraiva',
          'email' => 'israelbsaraiva@gmail.com',
          'texto' => 'Gostaria imensamente de congratular a Cia pelo espetáculo promovido no Sesc Interlagos. Eu sou Instrutor de atividades Fí­sicas do local e me senti completamente privilegiado de testemunhar o teatro &quot;Construtório&quot; . Um teatro que fuja do senso comum é tudo que nossas crianças precisam nesse momento para que não cresçam reproduzindo discurso de ódio como temos acompanhado por ai. Mais uma vez, parabéns. Um grande abraço. Israel. ',
          'created_at' => '2015-02-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'DANI MARQUES',
          'email' => 'dnzmarques@gmail.com',
          'texto' => 'Olá! Assistimos hoje o espetáculo Última Notí­cia, no Sesc Interlagos. Gostarí­amos de parabenizá-los pelo excelente trabalho. Ficamos realmente impressionados com a qualidade da apresentação. Nosso pequeno de 5 anos ficou encantado. Chegou em casa, pegou jornais e preparou para a famí­lia um pequeno espetáculo. Depois construí­mos juntos um boneco feito de jornal. Muito obrigada por compartilharem conosco desse lindo dom. Todo nosso incentivo e admiração. Abraço carinhoso a toda trupe! ;)',
          'created_at' => '2015-03-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rosana Maria',
          'email' => 'rosgarbosa@hotmail.com',
          'texto' => 'Valeu pessoal, muito bom o espetáculo! A mensagem me deixou  bastante emocionada. Vocês são extremamente  criativos e encantam tanto as  crianças quanto os adultos.\r\nParabéns! Muito sucesso!!!',
          'created_at' => '2015-03-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ricardo Andre Costa',
          'email' => 'racviterbo@hotmail.com',
          'texto' => 'Parabéns a Cia. Truks, recebemos em nossa cidade Santa Rosa de Viterbo a peça &quot;Última Noticia&quot; e foi uma delicia de ser ver. No final, a impressão é que já eramos í­ntimos do personagem, uma bela evolução. Parabéns a todos!',
          'created_at' => '2015-03-16 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'FLAVIA SGARBOSA GOMES DAMASIO',
          'email' => 'flaviasgarbosagomes@yahoo.com.br',
          'texto' => 'Muito impressionante... com pedaços de jornal, criatividade, sensibilidade e muito mas muito talento vocês tocaram nossas almas. O mundo seria um lugar muito melhor se mais pessoas  como vocês existissem. Parabéns pelo trabalho equipe do teatro Última Notí­cia. E obrigada por terem vindo á Guariba Sp. Flavia.',
          'created_at' => '2015-03-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'andre',
          'email' => 'nathan.andre@gmail.com',
          'texto' => 'Hoje fomos assistir í  peça na biblioteca monteiro lobato.\r\nQue belissima montagem, emocionante. Das crianças ao aví´, todos saí­ram maravilhados.\r\nParabéns e nos veremos em breve, com certeza.',
          'created_at' => '2015-03-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sebastião Augusto Pedroso - Paraibuna/SP',
          'email' => 'augustopedrosos@gmail.com',
          'texto' => 'Assisti o espetáculo Última Notí­cia na Fundação Cultural de Paraibuna, pelo Circuito Cultural, e realmente é lindo. O espetáculo cuja a magia, aventura e a arte de sonhar inspira e muito. Gostei muito da Cia. Truks, Última Notí­cia é incrí­vel! Parabéns, sucesso e voltem sempre!\r\n\r\nGrande abraço',
          'created_at' => '2015-03-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Denilson Filipini',
          'email' => 'denilsonfi@ig.com.br',
          'texto' => 'Última Notí­cia:A criação de um sonho, espetáculo maravilhoso que nos faz vivenciar nossa infí¢ncia onde nosso herói era nosso pai. Impressionante e criativo. Mágico e inspirador. Parabéns a Cia Truks.\r\nDenilson Filipini - Paraibuna/SP',
          'created_at' => '2015-03-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'camila',
          'email' => 'camunhozbraga@gmail.com',
          'texto' => 'Conheci a Cia Truks por acaso. Todo fds procuro levar meus filho í  um programa cultural. Cheguei a livraria Monteiro Lobato sem nenhuma expectativaminha ignorancia). E saí­mos adultos e crianças) encantados!! mais do que isso, emocionados com o espetáculo zoo-ilógico. Quanta criatividade e poesia!!!! A unica coisa que meus filhos queriam...era saber quando voltarí­amos. E voltamos! assistimos ontem ao sonhatório. E mais uma vez, saí­ arrepiada e emocionada!! Parabens pelo lindo trabalho que fazem para crianças e adultos!!! Camila',
          'created_at' => '2015-03-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Paula Bernardes',
          'email' => 'paulab@uol.com.br',
          'texto' => ': Ontem fui assisti-los na Biblioteca Monteiro Lobato. levei minha filha, Maria Luiza literalmente na marra. Tirei ela aos prantos da frente da televisão, do desenho horroroso, fútil, idiotizante, &quot;Barbie sei lá eu o quê&quot;, para levá-la por recomendação de uma amiga. foi uma das melhores experiências de nós duas, ou - porque não dizer - minha, dos últimos tempos. A peça é linda. Vcs são maravilhosos. Tudo muito divertido, emocionante, poético. Pra mim foi numa maravilhosa viagem criativa embalada numa bela trilha sonora com um show de iluminação. Espetáculo na medida para um adulto, acompanhado de uma criança. Diversão maravilhosa para as duas idades. Eu saí­ do teatro as lágrimas. Muito emocionada com o trabalho de vocês e com a possibilidade que me deram de vencer a Barbie com uma arma tão poderosa. Malu amou. Eu também. Obrigada aos três talentosos artistas. VCs nos proporcionaram uma linda manhã de domingo',
          'created_at' => '2015-03-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'João Marques',
          'email' => 'monicaamada@yahoo.com.br',
          'texto' => 'Meu nome é João e eu adoro aventuras! Hoje assisti com minha míe a peça Ultima Noticia em Mogi das Cruzes e gostei muito. Eu gostei de tudo!\r\nGostei da parte do dinossauro. Eu gosto muito de jogar ví­deo game, mas gostei da história! Quero assistir um monte! Tenho seis anos e minha míe é quem está digitando pra mim!',
          'created_at' => '2015-03-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Angela O. Muniz',
          'email' => 'angelaoliveiramuniz@gmail.com',
          'texto' => 'Sou de Guararema, fomos na escola assistir a peça Ultima Notí­cia, nós amamos, o trabalho de voces eÂ´muito especial faz as crianças rir e ter imaginações boas. Deus abençoe vcs.\r\n',
          'created_at' => '2015-03-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cristiane Brasil',
          'email' => 'cristianeb@cedrodolibano.org.br',
          'texto' => 'Tive o privilégio de ser convidada e levar meu filho, a conhecer o mundo do teatro. Confesso que fiquei tão encantada e emocionada como ele. A equipe é realmente encantadora. Que trabalho lindo. Convido í  todos a embarcar nesta gostosa aventura.',
          'created_at' => '2015-03-30 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Amanda Bavaro',
          'email' => 'amandabavaro@yahoo.com.br',
          'texto' => 'Sou de Ribeirão PiresPAULISTA) , fomos assistir a peça a última notí­ciano final de março 2015 Eu e meus 3 filhos a Eloá de 13 anos, o Enrique de 5 anos e a Eliza de 1 ano. Impressionante como o espetáculo agrada as várias idades...adoramos  ',
          'created_at' => '2015-04-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'cynthia',
          'email' => 'cynthiamourao@yahoo.com.br',
          'texto' => 'Assisti o espetáculo Sonhatório em Franca e adorei!! Gostei da interação dos artistas com as crianças e uma criatividade incrí­velParabéns ao grupo!! Com certeza todas as crianças vão experimentar utensí­lios da cozinha para criarem seus personagens!!Parabéns ao grupo!!',
          'created_at' => '2015-05-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Letí­cia Silvério',
          'email' => 'leti.cia_silverio@hotmail.com',
          'texto' => 'Hoje assisti a peça sonhatório na cidade de Franca e ameii!! Sério, a peça foi demais, os atores são incrí­veis.. As crianças da escola também adoraram. \r\nParabéns pelo ótimo trabalho, e espero que voltem aqui em breve =)',
          'created_at' => '2015-05-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Cristiane Brasil',
          'email' => 'cristianeb@cedrodolibano.org.br',
          'texto' => 'Olha eu aqui de novo! Desta vez \r\nassistimos &quot;A Bruxinha&quot;, estava cheio, quase não havia lugar. Amamos meu filho ficou anestesiado. A cada espetáculo ficamos sempre curiosos pela próxima. Parabéns í  toda equipe. Agradeço também pelo carinho com as crianças,após o espetáculo. ',
          'created_at' => '2015-04-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Renata Pinheiro Vieira',
          'email' => 're_vieira25@yahoo.com.br',
          'texto' => 'oi &quot;Sonhatórios&quot;, sou Renata, professora de Franca e hoje tive o privilégio de assistir mais um espetáculo de vocês... já vi e revi alguns e sou muito fí do trabalho deste grupo. Geralmente chego entusiasmada e saio emocionada.. e hoje não foi diferente! Obrigada por me inspirarem a brincar mais, sempre mais...abraços gigantes',
          'created_at' => '2015-05-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rogério Alves Nogueira',
          'email' => 'rogerioalvesnogueira@hotmail.com',
          'texto' => 'Prestigiamos dois espetáculos recentemente na biblioteca Monteiro Lobato e ficamos impressionados com a qualidade da equipe de atores, roteiro e direção. Próximo das pessoas, quebrando barreiras entre público e elenco. são construtores de pessoinhas, que saem do espetáculo transformadas. Parabéns a todos, parabéns Prefeitura de SP.',
          'created_at' => '2015-05-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Kátia de Castro Pereira ',
          'email' => 'katialoka_@hotmail.com',
          'texto' => 'Boa noite Pessoal!! gostaria de parabenizá-los pelo espetáculo que fizeram aqui em são Carlos ontem , no SESC,fui com minha filha e uma amiga dela , ambas , tem 10 anos... cá entre nós, no começo , pensei que elas não fossem gostar... me pareceu ser  mais voltado pra crianças menores ; não precisei de mais do que 5 ou 10 minutos , pra ver as duas vidradas e eu também !   :-)  \r\nenfim, tudo muito criativo , uma linguagem simples e direta com uma interpretação sem igual de TODOS VOCÚS! \r\nRealmente , me contagiou , a maneira como conseguiram transmitir um mundo de imaginação e alegria plena, que só encontramos no universo infantil !  \r\n\r\nQue DEUS continue abençoando , cada um de vocês , para que consigam , sempre , exercer esse , que não é apenas um trabalho e sim um dom ! \r\n\r\n\r\n\r\n\r\n\r\nkátia, espetáculo ÚLTIMA NOTíCIA no SESC de são Carlos ',
          'created_at' => '2015-05-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Angélica Sant\'Anna',
          'email' => 'ssantaanja@hotmail.com',
          'texto' => 'Olá Pessoal! Vim em nome dos alunos de Teatro do Sesc Rio- são João de Meriti parabenizá-los pelo lindo espetáculo de ontem, completando com a oficina de Bonecos. Foi lindo demais, muito prazeroso aprender um pouquinho com vocês, queremos muito que vocês voltem logo, Já estamos com saudades! Grande Beijo a todos. ðŸ’™ðŸ’™ðŸ’™',
          'created_at' => '2015-05-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Liliane Serra Viana',
          'email' => 'liliane_serra@globomail.com',
          'texto' => '\r\n\r\né com imenso prazer que agradeço a esta Cia maravilhosa o dia de ontem em nossa escola. Tudo foi perfeito! As crianças amaram! Os pais que estiveram presentes para a conversa com os atores, foram convidados a assistir ao espetáculo e saí­ram maravilhados. E nós da equipe escolar não sabemos que palavra define este momento que vivemos: Perfeito? Maravilhoso? Inesquecí­vel? Tudo? é isto, foi TUDO! Tudo de lindo , tudo de melhor, tudo do coração... Que Deus os abençoe! E até a próxima!\r\n\r\nLiliane e equipe da EMEI Piratininga.',
          'created_at' => '2015-05-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marcelo C. P. Aguiar',
          'email' => 'mpcaguiar@gmail.com',
          'texto' => 'Assisti ontem, dia 07 de junho de 2015, ao espetáculo Voví´, deste grupo especial. Primeiro ficamos, eu e minha famí­lia, impressionados com a qualidade do trabalho. Mas depois, fomos nos emocionando de tal forma, que terminamos a apresentação com lágrimas de emoção nos olhos. A peça nos faz rir e chorar, mas, principalmente, nos faz pensar no que realmente é importante na vida. Nossas famí­lias, nossos entes queridos, a lembrança de nossos antepassados e o carinho com os nossos decendentes. Obrigado a este grupo maravilhoso pelos momentos especiais que nos proporcionaram. As crianças falaram do Voví´ durante todo o domingo. Parabéns para a Prefeitura de SP por um projeto assim.',
          'created_at' => '2015-06-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rafael e Taynara',
          'email' => 'rafael-1804@hotmail.com',
          'texto' => 'Assistimos hoje a Peça Construtório, é literalmente incrí­vel o trabalho de vocês. A criançada se diverte e se interage com vocês, eu Rafael, me emocionei quando vocês usaram Materiais da obra para fazer animais com criatividade. As crianças são educadas e aprendem a usar a imaginação, podendo brincar com qualquer coisa que estiver em sua volta. Interessante também que fizeram o livro, combinações de cores com as sacolas azul e amarela para dar a luz a uma sacolinha verde(Azul + Amarelo = verde). Parabéns, espero que a vida dê todo conforto que vocês merecem, que Deus ilumine o caminho de vocês! Sucesso sempre!',
          'created_at' => '2015-06-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marlene Mateus',
          'email' => 'marleneomateus@gmail.com',
          'texto' => 'Olá Pessoal! Assistimos í  peça A Bruxinha no SESC - Campinas no sábado07). Também tivemos o privilégio de ganhar um autógrafoprimeiro para o meu filho) da escritora Eva Furnarida personagem). Estava tudo perfeito! Sempre que possí­vel levo meus filhos ao teatro, mas o espetáculo de vocês é ESPECIAL... Parabéns pelo talento e o dom divino de colorir um pouco mais o nosso mundo! :D',
          'created_at' => '2015-07-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Thereza Rachel',
          'email' => 'uh_there@outlook.com',
          'texto' => 'Boa tarde,\r\n\r\nOntem fui com a minha filha assistir a Bruxinha no SESC-Bauru, amamos!! Uma delicia, divertido e lindo! Vocês todos estão de parabéns. Obrigada por nos proporcionar tamanha alegria! Vocês são muito competentes. E semana que vem estarei lá novamente para prestigiar Sonhatório!!',
          'created_at' => '2015-07-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Debora Ogawa',
          'email' => 'deboramayumiogawa@hotmail.com',
          'texto' => 'Olá!Assistimos ontem a peça Sonhatório e eu e minha filha literalmente choramos de tanto rir, fazia tempo que eu não assistia algo tão engraçado, criativo e que transmite uma mensagem linda a nós pais e as crianças em geral. Obrigada por ter nos proporcionados momentos lindos que não vamos esquecer!!!',
          'created_at' => '2015-07-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Anderson Barros',
          'email' => 'anderson@omegagrupo.com.br',
          'texto' => 'Boa Tarde, levei meus filhos para assistir a peça A Bruxinha no SESC - Campinas 11/07. Adoramos, vocês são muito bons e estão de parabéns, esperamos assistir mais peças de teatro com a CIA TRUKS. Muito obrigado. Sucesso!',
          'created_at' => '2015-07-17 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Laercio Silva',
          'email' => 'llasillva@hotmail.com',
          'texto' => 'Pessoal , venho aqui parabenizar a todos da CIA TRUKS, assiste com a famí­lia a todas as peças de teatro realizado no mês de Julho no SESC Bauru SP, pessoal vcÂ´s são de uma competência admirável, muito , mas muito bom mesmo. Espero que voltem rápido pra Bauru ',
          'created_at' => '2015-08-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Renata Chaves',
          'email' => 'chavesrenata@yahoo.com.br',
          'texto' => 'Sou apaixonada pelo trabalho de vocês, os acompanho há uns 15 anos e a cada novo espetáculo me encanto. Tenho um filho de nove anos que assistiu sua primeira peça de teatro com vocês e no sábado passado repetimos a dose com nosso caçula de 1 ano e meio. Parabéns pelo trabalho maravilhoso que nos transporta para um mundo magico e de muita alegria. Adoramos o Sonhatório!!!',
          'created_at' => '2015-07-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Amanda Serpelloni',
          'email' => 'amanda_serpelloni@hotmail.com',
          'texto' => 'Tive a honra de assistir a dois espetáculos de vocês, Sonhatório e Construtório, que me deixaram maravilhada. não tenho palavras para descrever tamanha emoção que passei, assisti ambos no acampamento de férias Sí­tio do Carroção e me fizeram relembrar ainda mais a minha criança interna e brincar com ela, vocês brincaram com ela, me encheram de alegria. Estou louca para conhecer Acampatório e levarei minha famí­lia comigo. Sou muito grata pela oportunidade que me deram de brincar com minha criança!',
          'created_at' => '2015-07-31 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'MARCELO JURANDIR P.',
          'email' => 'marcelojurandirp@gmail.com',
          'texto' => 'Sou fí de carteirinha da Cia Truks e os venho seguindo há muito tempo. Quando vi que ia estrear Acampatório, no Sesc Consolação, pensei, o que vão fazer desta vez? Minha surpresa foi ver que vocês conseguiram se superar. Achei que isso não ia ser possí­vel, porque Sonhatório e Construtório são muito bons. Acampatório é ainda melhor. Incrí­vel. Passei, com minha famí­lia, rindo o espetáculo todo, e nos emocionamos com os í­ndios e com o final da história. Parabéns para todos vocês, que a cada peça se mostram ainda melhores. V ou recomendar a todos os meus amigos.',
          'created_at' => '2015-08-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marina ',
          'email' => 'mm_psico@hotmail.com',
          'texto' => 'Amei as peças que trouxeram para Bauru no sesc.\r\n Tenho um filho que 5 anos que claro também amou todos os espetáculos,quando no teatro da bruxinha anunciaram o construtório eu não guardei o nome da peça já ele não esqueceu.  \r\nEle estuda em colegio com metodologia waldorf, onde dão muitas asas a imaginação hoje chegou na escola pegou o prendedor que sempre se transformou em avião, nave entre outros e mostrou aos amigos o pexinhoooo, até me arrepiei que lindo isso! O mundo precisa de muito mais de vocês! \r\nSó estou com um problema agora... Ele quer ver o acampatório kkk . \r\nMuito obrigado por nos presentear com a imaginação de vocês!',
          'created_at' => '2015-08-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Renata Pietsch',
          'email' => 'renata@guilger.com.br',
          'texto' => 'Quando soube da estréia do espetáculo Acampatório, fiquei muito curiosa. Levei minha filha mais velha e voltarei para levar meu mais novo e amigos também. Muito criativo, diní¢mico, um roteiro que emociona e alegra. Parabéns e até breve.',
          'created_at' => '2015-08-10 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'DANILO ALVES, VIVIAN E RAFAELA MERONI',
          'email' => 'truks@uol.com.br',
          'texto' => 'Olá. Escrevo-lhes para parabenizá-los pela apresentação de Sábado08/2015), no Sesc Consolação. Eu e minha esposa levamos nossa filha Rafaela de 4 anos para assistí­-los e foi uma grata surpresa. O espetáculo foi incrí­vel. Tudo muito simples, singelo e de fácil percepção. Por vezes flagrava minha filha í s gargalhadas durante a peça. Mais uma vez, parabéns, vocês são ótimos, deixaram até uma pequena vontade de experimentar esta arte. Vida longa para a companhia. Grande abraço Danilo Alves Vivian Meroni Rafaela Meroni ',
          'created_at' => '2015-08-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Bruno',
          'email' => 'brunohcomercial@gmail.com',
          'texto' => 'Que ESPETíCULO. Com muita simplicidade conseguem nos fazer viajar num fantástico mundo de sonhos. Emocionante. Parabéns a toda a equipe. Bruno - Pai Aquarela Guaratinguetá.',
          'created_at' => '2015-08-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vanda Santos',
          'email' => 'Vandaoliveiraprof@gmail.com',
          'texto' => 'Adorei o espetáculo: Sonhatório, apresentado no Sesi - Santos. Gostaria de parabenizar a todos da CIA Truks pela bela apresentação. Fiquei emocionada em alguns momentos da apresentação e quando subi ao palco. Vcs transmitiram uma linda mensagem. Poder participar deste momento foi maravilhoso, mágico. Que possamos ser sonhadores de um mundo melhor.Brincar será sempre um ótimo remédio para nossa vida. Vou recomendar a todos os meus amigos.Obrigada pela oportunidade de conhece-los\r\nVanda Santos - 11/08/2015 ',
          'created_at' => '2015-08-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Kamila Proença',
          'email' => 'kamila.proenca@uol.com.br',
          'texto' => 'Gostaria de parabenizar a Cia. Truks - Teatro de Bonecos pelo belí­ssimo espetáculo realizado hoje 15/08/15 em Guaratinguetá, nossas crianças adoraram, vocês são ótimos! Grande abraço!',
          'created_at' => '2015-08-15 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Netto Maia',
          'email' => 'maia.netto@yahoo.com.br',
          'texto' => 'Parabéns pela apresentação realizada no dia 15/08/15, em Guaratinguetá, em comemoração ao dia dos pais. Foi emocionante ver a alegria e o encanto nos olhos das crianças. Belo presente!  Obrigado e sucesso a vocês!  Netto Maia e famí­lia.',
          'created_at' => '2015-08-22 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marcio Flores M. Paxeco',
          'email' => 'marciofloresmpaxeco@gmail.com',
          'texto' => 'Fui com minha filhinha de 3 anos ver Acampatorio, achando que a peça ia ser pra ela e fiquei impressionado minha filha mal piscava de tanto que gostou de tudo, e eu fiquei muito emocionado e tive que enxugar lagrimas no final. Vocês está de parabéns. Já indiquei pra amigos, familia, pra todo mundo. Parabens.',
          'created_at' => '2015-08-23 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rita C V Mendonça',
          'email' => 'ritinha33@gmail.com',
          'texto' => 'ACAMPATí“RIO E LINDOOOOOOOOOOO! ESTOU ENCANTADA! SOU ESTUDANTE DE TEATRO E SAí DO TEATRO COM A CERTEZZA DE QUE é ISSO QUE QUERO FAZER DA MINHA VIDA. ENFIM, SER COMO VCS! LINDOS, SENSíVEIS! AS CRIANí‡AS PRECISA DE COISAS ASSIM, PRECISAM DE VCS! LINDOS!',
          'created_at' => '2015-08-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Giani Peres Pirozzi ',
          'email' => 'gianitata@yahoo.com.br',
          'texto' => 'Hoje tive a oportunidade de conhecer o &quot;Sonhatório&quot; - Que espetáculo fantástico! Parabéns pela criatividade e o por aflorar a magia da imaginação. Fiquei encantada... Sucesso a todos!!! ',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fabio Nakaharada',
          'email' => 'farukafn@hotmail.com',
          'texto' => 'Assisti ao espetáculo &quot;Última Notí­cia&quot;, no feriado de 07 de setembro no Sesc Santo Amaro e fui surpreendido - essa é a palavra - por  uma arte, um peça, um show, um teatro muito especial, rico, criativo e emocionante. No final, os olhos marejados de emoção. Olhei para o lado e lá estava minha esposa Mariana, igualmente emocionada. Na saí­da, diversos pais e míes agarrados aos filhos, emocionados. as crianças não piscaram um segundo e se divertiram muito com a baleia, os peixes, os dinossauros, dragíµes e com o espaço sideral. 50 minutos de riso e apreensão nos rostinhos. Já tinha ouvido falar da Cia Truks, mas nunca havia assistido a um espetáculo. Surpreendente, maravilhoso. Graças í  disponibilidade de vocês e ao Sesc, pode-se ter uma peça desse ní­vel acessí­vel a todos. Infelizmente a divulgação é muito restrita, e só descobrimos por que fuçamos regularmente a programação do Sesc. Divulgei nas escolas dos meus filhos Pedro e Rafael, no Espaço da Vila e Escola da Vila, para que todos possam ter a oportunidade de saber dessa programação e de levar seus filhos para assistir! Vocês são o diferencial nesse mundo atual de cinema 3D, 4D, filmes de crianças com piadas de adultos e referências pop... parece que a arte para crianças hoje em dia é feita para os pais, pois são esses que tem o poder de compra... e vocês fazem o espetáculo na linguagem dos pequenos, para os pequenos, no universo dos pequenos!!! Deus abençoe a todos vocês, e continue abençoando e dando muita saúde para continuarem com muita fé nessa batalha diária, que é fazer arte de qualidade no Brasil. não desanimem. Vou divulgar o máximo que puder. O mundo está precisando de mensagens e arte como a que vocês criam, inventam e oferecem. Muito obrigado!!!!!!! ',
          'created_at' => '2015-09-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'carlos eduardo',
          'email' => 'cadusoares2004@gmail.com',
          'texto' => 'Adorei o espetáculo: Sonhatório, apresentado no Sesi - Santos. Gostaria de parabenizar a todos da CIA Truks pela bela apresentação. Fiquei emocionada em alguns momentos da apresentação e quando subi ao palco. Vcs transmitiram uma linda mensagem. Poder participar deste momento foi maravilhoso, mágico. Que possamos ser sonhadores de um mundo melhor.Brincar será sempre um ótimo remédio para nossa vida. Vou recomendar a todos os meus amigos.Obrigada pela oportunidade de conhece-los Carlos 27/08/215',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ana Beatriz de Paula Fontes',
          'email' => 'abpfontes@gmail.com',
          'texto' => 'Primeiramente,PARABENS!!!!!!\r\n   Vocês são incriveis! O lugar ficou com uma energia positiva com vocês ali apresentando. E o final estava lindo e emocianante!!\r\n   obrigada o espetaculo foi lindo aqui no sesi de itu\r\n   ',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Katia Ferreira Nesteru Corrêa',
          'email' => 'diretoria@magistralescola.com.br',
          'texto' => 'Encantamento, sensibilidade e emoção... amei o espetáculo &quot;O senhor dos sonhos&quot;, nos faz refletir sobre a infí¢ncia e sua riqueza única. Parabéns!!',
          'created_at' => '2015-09-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Larissa Sitchin',
          'email' => 'larissa.sitchin@gmail.com',
          'texto' => 'Acampatório é simplesmente INCRíVEL! Uma peça que lhe surpreende a cada segundo e ainda lhe faz se emocionar no final! \r\nO mais valioso é a demonstração de como é possí­vel se divertir usando poucos objetos e como é importante dar valor aos nossos amigos, desde cedo!\r\nParabéns, parabéns, parabéns! Foi mágico!\r\n\r\n',
          'created_at' => '2015-09-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Mí´nica',
          'email' => 'monikets@leka.com.br',
          'texto' => 'Assisti a Última Notí­cia no Sesc Santo Amaro. \r\nNossa, achei suuuper bacana, muito divertido. Até prendeu a atenção do meu bebê de 11 meses, que nem piscava. \r\nNem deu para perceber a hora passar, simplesmente ótimo, encantador.\r\nParabéns!!!!!!\r\nQue continuem encantando í  todos sempre!!',
          'created_at' => '2015-09-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Maria Elza Araujo',
          'email' => 'elzaaraujo_rap@yahoo.com.br',
          'texto' => 'Eu já assisti a alguns espetáculos de vocês. E cada vez que assisto um novo fico mais maravilhada com o talento de todos. Parabéns em nome de toda garotada do CEU EMEF Alto Alegre e E E Romeu Monrtoro que assistiram vocês em Cidade Azul e Senhor dos Sonhos. ',
          'created_at' => '2015-09-26 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rhena de Faria',
          'email' => 'rhenadefaria@hotmail.com',
          'texto' => 'Aos atoresdiretor) de Acampatório; assisti ao espetáculo hoje, último dia no Sesc Consolação. Me emocionei demais. é singelo, delicado, poético e muito engraçado também. Chorei feito uma boba com a cena da cidade engolindo os í­ndios. E quando acreditei que bastava, vocês me vêm com aquele final arrebatador e acabam comigo de vez. Minha filha de três anos disse que a parte que ela mais gostou foi a da bruxa. Só lamentei uma coisa: não ter tempo de indicar aos amigos. Vou ter que aguardar a próxima temporada. Obrigada! Um beijo a todos, Rhena de Faria ',
          'created_at' => '2015-09-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Daniela Moraes Cardoso',
          'email' => 'dandancardoso@gmail.com',
          'texto' => 'Nos deliciamos ontem, em Araraquara, com &quot;Acampatório&quot;. Lindo, demais! E volta e meia meu filho Franco, de 4 anos, diz: &quot;mamãe, a parte que eu mais gostei foi...&quot; E assim, de parte em parte, ele vai comentando todo o espetáculo. Gostou de tudo, amou tudo e, especialmente, delicadamente, absorveu as mensagens. Parabéns í  Cia pela criatividade e dedicação. Semana que vem tem mais!',
          'created_at' => '2015-10-19 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marcelo Lia',
          'email' => 'marcelolia@bol.com.br',
          'texto' => 'Estive ontem em companhia de meus dois filhos, Rafaela de 3 anos e Matheus de 8 anos, assistindo a peça encenada no Teatro do Sesc de Araraquara, &quot;Voví´&quot;.\r\nEmocionante, ilustrativa, educativa, sensacional. Tenho 51 anos e me senti com cinco anos de idade. Ri, chorei, aplaudi. Parabéns.',
          'created_at' => '2015-10-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Flávia',
          'email' => 'flagfernandes@hotmail.com',
          'texto' => 'Assisti ontem, com meu filho de 3 anos e seus amiguinhos, ao espetáculo VOVí”, no SESC Araraquara. Adorei, fiquei emociona com a história e feliz de ver a alegria do meu pequeno. Ficamos encantado!! Parabéns a CIA Truks!!',
          'created_at' => '2015-10-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rafaela Scabora',
          'email' => 'rafa.scabora@gmail.com',
          'texto' => 'Assisti hoje em Araras com meus alunos do 1.ano, o espetáculo Acampatório. Foi simplesmente maravilhoso. Cheio de detalhes repleto de sentimentos, que nos preenchem de uma forma extraordinária. Meus alunos amaram, eu amei! Vocês são esplêndidos!',
          'created_at' => '2015-10-06 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'THATIANA',
          'email' => 'thati2d@bol.com.br',
          'texto' => 'OS ATORES DA CIA TRUKS ESTíƒO DE PARABéNS PELO ESPETíCULO VOVí”! ASSISTI ONTEM NO SESC DE ARARAQUARA COM MINHA FILHA E AMIGOS, E CONFESSO QUE ME EMOCIONEI E ME DIVERTI COM O TEATRO! LINDA HISTí“RIA... COMO NOSSOS VOVí”S SíƒO IMPORTANTES PARA Ní“S... TANTA COISA PARA NOS CONTAR... ME FEZ SENTIR MAIS SAUDADES AINDA DO MEU AVí”...',
          'created_at' => '2015-10-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Rosane Gonzalez',
          'email' => 'comercial@mundodocooler.com.br',
          'texto' => 'Assistimos ontem A Bruxinha no Sesc Santo Andre e simplesmente adoramos o espetáculo. Deixou todas as crianças e nós adultos hipnotizados e totalmente envolvidos com a história, estão todos de Parabéns!! Agradecemos a gentileza após o espetáculo em permitir que as crianças tirassem foto com a personagem!!',
          'created_at' => '2015-10-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Marcos Pai e Filho Yuri Araraquara sesc',
          'email' => 'leotacano@hotmail.com',
          'texto' => 'Acampatório foi uma linda apresentação que nos trouxe muita emoção ao chegar a cena dos í­ndios perdendo suas terras por prédios. Também foi grande a emoção no final, pela amizade que passou por gerações.....espero que essa amizade de voces seja eternas.....Â¨gostariamos de ver todas as cadeiras do teatro ocupadas por crianças, principalmente aquelas que não tem oportunidade de estar num momento tão mágico que é assistir o TEATROÂ¨ Parabens...',
          'created_at' => '2015-10-19 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Clodoaldo Ricardo João Pani',
          'email' => 'clodoaldoricardo@yahoo.com.br',
          'texto' => 'Boa Tarde! Sou de Araraquara e acompanhei por dois finais de semana consecutivos o trabalho de vocês pelo SESC, com os espetáculos Construtório e Acampatório. Gostaria de parabenizá-los pela riqueza da abordagem tratada, representando o universo infantil, da construção dos objetos através da imaginação ativa, sem esquecer a manifestação lúdica reprimida no adulto; e, sobretudo, atingindo a crí­tica da alienação no mundo atual do trabalho. Estamos, eu e minha famí­lia, encantados com o singular trabalho do grupo. Aguardamos ansiosos novas oportunidades.',
          'created_at' => '2015-10-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alessandra',
          'email' => 'ale.romero@bol.com.br',
          'texto' => 'Participei da festa de dia das crianças na Associação dos Advogados de são Paulo, e fiquei encantada com o espetáculo de vcs. Mesmo com poucos diálogos, a peça é de extrema sensibilidade e inteligência. Minha filha de 6 anos adorou tambem. Parabéns pelo fantástico trabalho! Espero vê-los em outras apresentações.',
          'created_at' => '2015-10-20 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Patrí­cia Kitaka',
          'email' => 'patricia.kitaka@gmail.com',
          'texto' => 'O espetáculo Acampatório é simplesmente encantador !!! Experiência extraordinária não apenas para os pequenos... Repleto de momentos surpeendentes, emocionantes e divertidos. Parabéns a toda equipe pela delicadeza e profundidade incomum em peça infantis. Esperamos tê-los em breve novamente em Campinas.',
          'created_at' => '2015-10-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Guilherme SESC Santo André5',
          'email' => 'guilhermebarrosaraujo@hotmail.com',
          'texto' => 'A &quot;ultima noticia&quot; foi de longe a melhor peça para crianças que eu tive o prazer de assistir.Parabéns!',
          'created_at' => '2015-10-28 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Janaini',
          'email' => 'janaini.rh@outlook.com',
          'texto' => 'Bom dia!\r\nOntem assisti o espetáculo Ultima Noticia no Sesc Santo André. E só gostaria de deixar minha mensagem aqui, que eu amei e meu filho se divertiu muito e ficou encantado com o espetáculo, nos divertimos muito! Parabéns!!! Queremos ir em outros espetáculos, que Deus abençoe vocês e muito Sucesso!\r\n\r\nAtt\r\nJanaini Fagundes',
          'created_at' => '2015-11-03 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Gisele e João Henrique',
          'email' => 'giselemgm@hotmail.com',
          'texto' => 'Queremos parabenizar toda a equipe pelo lindo trabalho desenvolvido na Biblioteca Padre José de Anchieta, e agradecer em especial aos atores que fizeram nosso domingo mais legal e divertido com a peça Acampatório. Lindo observar cada detalhe da peça e o desenrolar do &quot;texto&quot;. Meu filho ficou super encantado com o tema, mas confesso, fiquei mais do que ele!!! Deus abençoe grandemente o trabalho de vocês a cada dia, porque verdadeiramente sabem usar o dom que o Criador lhes deu e, amam o que fazem. Obrigado pelos risos, valeu muito a pena. Abraços',
          'created_at' => '2015-11-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Carolina  Leoni',
          'email' => 'carolaleoni@ig. com.br',
          'texto' => 'Parabéns pelo excelente trabalho. Hoje eu e meu filho Raul de 3 anos assistimos a peça O Senhor dos Sonhos no PDA em Praia Grande, ele ficou muito feliz e eu me emocionei com tanto carinho que vocês  nos prestaram. Obrigada por realizarem  momentos de alegrias a nossas famí­lias. Deus os abençoe. ',
          'created_at' => '2015-11-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Juliana Martinelli Silva',
          'email' => 'julymartinelli@hotmail.com',
          'texto' => 'Eu e meu filho Mateusanos) assistimos &quot;O Senhor dos Sonhos&quot; no PDA em Praia Grande, no dia 07/11/15.\r\nSimplesmente adoramos! Espetáculo maravilhoso. Parabéns pelo lindo trabalho que vocês realizam e que vocês retornem brevemente í  minha cidade.',
          'created_at' => '2015-11-09 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Daniela Rizzo',
          'email' => 'dannirizzo38@gmail.com',
          'texto' => 'Que bálsamo para a alma são vcs!!Se encantar nos dias de hj.Que tarefa bendita! Lindo espetáculo O Senhor dos Sonhos,a parte que todas as luzes se apagam para dar lugar ao sonhos do Lucas foi uma das coisas mais lindas que já vi na vida..Sucesso cada vez maior pra vcs da Cia Truks.abraços!',
          'created_at' => '2015-11-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sandra D.',
          'email' => 'sanducatti@gmail.com',
          'texto' => 'Assistimos vocês ontem no Sesc Piracicaba com a peça Acampatorio. Parabéns pelo belo e emocionante trabalho. Ate chorei no final e quando sai vi vários adultos com os olhos cheios de lagrimas. Bela mensagem. Que Deus os abençoe sempre para continuar com este belo trabalho, o teatro, levando fantasia, ilusão, memorias, alegrias etc, as crianças e aos adultos. Tudo de bom pra vcs. Abraços fraternos... ',
          'created_at' => '2015-11-11 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'marcia',
          'email' => 'menina_s2de@hotmail.com',
          'texto' => 'adorei assistir vcs sabado na escola prof.viriado correia em sao bernardo com a peça O SENHOR DOS SONHOS ,o meu filho joao pedro ama teatro ele chegou em casa e fez o personagem lucas e a cama quando vira um nave espacial vcs estao de parabens espero assistir vcs novamente.MARCIA 07/12/2015\r\n',
          'created_at' => '2015-12-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Roberta Strack',
          'email' => 'robertastrack@hotmail.com',
          'texto' => 'Preciso dizer, que meu filho é completamente louco por vcs! E consequentemente eu e o pai tambem! Parabens a vcs, por tanta dedicação e inteligencia para com as Crianças!\r\n',
          'created_at' => '2015-12-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Leticia S. Souza',
          'email' => 'leticiaminari@uol.com.br',
          'texto' => 'Gostaria de elogiar a peça Acampatório que acabei de ver com meu filho. Nos divertimos muito e fomos capturados pela estória simples e engraçada que agradou crianças e adultos. A criatividade e o trabalho dos 3 atores que se entregam em cena também é admirávelde todos os outros envolvidos que não vemos). Parabéns e que bom que ainda existem pessoas que continuam investindo na arte e principalmente se dedicam í  linguagem voltada para as crianças. Confesso que há tempos não via uma peça com tanta qualidade! ',
          'created_at' => '2015-12-08 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Mariane Laurentino Ferreira',
          'email' => 'marilaurentino@hotmail.com',
          'texto' => 'Sonhatório = sensacional!!!!!\r\n\r\nParabéns í  toda equipe, a peça toda foi espetacular e o final, surpreendente!\r\n\r\nVoltem í  Praia Grande mais vezes, eu e minha famí­lia com toda certeza estaremos de pé os aplaudindo!!!',
          'created_at' => '2015-12-18 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fábio Ribeiro',
          'email' => 'faldrath@gmail.com',
          'texto' => 'Estivemos hoje na Biblioteca ílvares de Azevedo para assistir Zoo-Ilógico. Foi a primeira vez que levamos nossa filha de dois anos e meio para o teatro, então estávamos preocupados. Mas foi ótimo! Ela ficou muito entretida quase o tempo todo, e a peça foi espetacular, muito divertida. Parabéns pelo trabalho!',
          'created_at' => '2016-02-14 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sandra',
          'email' => 'sanrizzi@ig.com.br',
          'texto' => 'Hoje assistimos na Casa de Cultura da Freguesia do í“, espetáculo Zoo-ilógico. Amei, interativo, engraçado, delicado. Levamos usuários da nossa unidade de saúde- CECCO Fí“. Parabéns, venham mais vezes ao nosso território pra curtimos e aplaudirmos de pé. ',
          'created_at' => '2016-02-24 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Régis Querino',
          'email' => 'reqsantos@gmail.com',
          'texto' => 'Estive ontem no Sesc Santos, com minha mulher e minha filha, para assistir ao espetáculo &quot;Cidade Azul&quot;. Foi sensacional! Já havia visto, com a filhota, o espetáculo &quot;Bruxinha&quot;, outro lindo trabalho do grupo. Fica a dica aos que ainda não viram a galera do Trucks em ação: não deixem de assisti-los quando tiverem oportunidade! Parabéns, moçada, vocês mandam muito bem! E nos fazem viajar junto com os pirralhos, lembrando que é muito bom ser criança. Grande abraço!',
          'created_at' => '2016-01-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fabiana Ribeiro',
          'email' => 'fabiribeiro@gmail.com',
          'texto' => 'Sou de Sorocaba e estive ontem no Sesc Itaquera e tive a oportunidade de assistir ao espetáculo Acampatório! E me senti na obrigação de entrar aqui pra dizer que foi incrí­vel! Meus filhosde 6 anos e outra de 3 anos) ficaram encantados e eu, fiquei com um sorriso no rosto do começo ao fim. Um espetáculo diní¢mico, divertido, visualmente atraente e porque não dizer, com um final que me emocionou! Enfim, só queria dizer que adoramos. Parabéns e muita sorte pra vocês neste lindo trabalho! ',
          'created_at' => '2016-01-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luiz Antí´nio',
          'email' => 'dslc@ig.com.br',
          'texto' => 'Assisti a dois trabalhos da Cia Truks: O senhor dos sonhos e Cidade azul. Gostei muito do estilo do trabalho, no qual identifiquei histórias com sequência lógica, de entendimento fácil, direcionadas ao público infantil, com ótima manipulação dos bonecos, efeitos visuais muito bons, interação com o público e mensagens excelentes para a formação do caráter das crianças, inclusive, no que diz respeito a questíµes raciais. O que considerei inovação, face aos trabalhos que já assisti, foi a interação dos personagens com os manipuladores, muito bom!',
          'created_at' => '2016-01-25 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sintia',
          'email' => 'svaleria@gmail.com',
          'texto' => 'Assisti o espetáculo Sonhatório hoje no teatro municipal de Anápolis e fiquei encantada! Vocês são ótimos, mal espero pra ir amanhã e depois. Parabéns! A peça é muito criativa e divertida e os atores maravilhosos.',
          'created_at' => '2016-02-26 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Alessandra',
          'email' => 'morais.ai@hotmail.com',
          'texto' => 'Ontem assistimos a peça Sonhatório, em Anápolis-GO, meus filhos e eu amamos! Um espetáculo criativo, engraçado e surpreendente. Parabéns! Voltem sempre í  nossa cidade. ',
          'created_at' => '2016-02-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Fernanda Braga',
          'email' => 'pentagriplotadora@gmail.com',
          'texto' => 'Olá, ontem assistimos o espetáculo sonhatório no teatro municipal de Anápolis, que com certeza fez juz ao nome &quot;Espetáculo&quot;.\r\nPeça muití­ssimo interessante minha familia simplismente amou, ninguém pisacava e o mais interessante é a proposta da peça, que nos mostra o quanto poder ser simples ser feliz, só basta querer.....\r\n Espero que voltem mais vezes!!!!\r\n\r\nGrande Abraço',
          'created_at' => '2016-02-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Gisele',
          'email' => 'giselefaria@anapolis.go.gov.br',
          'texto' => 'Quero agradecer a presença de vocês em nossa cidade! Principalmente agradecer pela alegria que deram aos meus filhos e a mim também, pude ver como é importante o brincar e como podemos fazer isso com as coisas que temos em casa.. realmente um grande espetáculo! Parabéns a toda equipe, que tenham muito sucesso, trazendo alegria para adultos e crianças..nunca esquecerei.. ',
          'created_at' => '2016-02-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Priscila ',
          'email' => 'reis.bio08@gmail.com',
          'texto' => '	Oi!!! Assisti hoje í  peça Construtório no teatro municipal de Anápolis e gostei MUITO!!! VOCÊS SÃO TRI CRIATIVOS! Adorei a parte do star wars e achei linda e fofafiz aawwnnn) a parte da lagarta/borboleta. Atores super divertidos. Parabéns!!!! E obrigada por nos presentear!',
          'created_at' => '2016-02-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Valquiria',
          'email' => 'quiriamarcal@yahoo.com.br',
          'texto' => 'Olá!!!Eu e minhas filhas assistimos a peça Zoo-ilógico, no sábado, 26/02, aqui em Anápolis-Go. Adoramos, elas ficaram encantadas. Desde esse dia elas ficam apresentando peças teatrais em casa, rsrs. Parabéns! E até a próxima.',
          'created_at' => '2016-02-29 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Ingrid Coelho',
          'email' => 'ingridcoelhoef@gmail.com',
          'texto' => 'Encantador é a primeira palavra! Adultos e crianças saí­ram da peça Construtórioem Resende - RJ) absolutamente felizes! Sou muito grata à  reflexão profunda que o espetáculo me proporcionou...sobre as relações humanas, as relações de trabalho, a forma que escolhemos para traduzir a vida...tudo tão sensí­vel....Gratidão!',
          'created_at' => '2016-03-05 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Luiza',
          'email' => 'kikabarbara@yahoo.com.br',
          'texto' => 'Assisti a apresentação do Zoo-ilogico em Resende/RJ e fiquei encantada com o trabalho de vcs. Parabens! Eu e as crianças adoramos. Voltem sempre!!!',
          'created_at' => '2016-03-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Vagner Brasil',
          'email' => 'vagnerbrkf@gmail.com',
          'texto' => 'Maravilhoso. Para adultos e crianças aprenderem a soltar a imaginação e ver o mundo com mais alegria. ',
          'created_at' => '2016-03-07 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Giovanna da costa souza ',
          'email' => 'gigicostasouza@outlook.com',
          'texto' => '  Na escola SESI-ITU assisti o teatro sonhatorio voces nao tem de quanto eu gostei foi muito emocionante...Eu dei muitas risadas...AAAA e nao posso esquecer voces me ajudaram e deram uma grande ideia a mim recentimente meu irmao de 4 anos quebrou o braço e ele nao pode fazer muitas coisas ai ele ficou meio tristinho e vou fazer o resumo desse teatro para ele esquecer do que aconteceu eu agradeço muito por fazer esse teatro maravilhosso sou sua fí numero 1 ',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Danilo Bertaco',
          'email' => 'danilobertacoitu@gmail.com',
          'texto' => 'gostei espetáculo: Sonhatório, apresentado no Sesi -itu. Gostaria de parabenizar a todos da CIA Truks pela bela apresentação.  Vcs transmitiram uma linda mensagem. Poder participar deste momento foi maravilhoso, mágico. Que possamos ser sonhadores de um mundo melhor.Brincar será sempre um ótimo remédio para nossa vida. Vou recomendar a todos os meus amigos.Obrigada pela oportunidade de conhece-los Danilo Bertaco - 27/08/2015',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Sérgio Yoshio Obana',
          'email' => 'sergio_obana@hotmail.com',
          'texto' => 'Gostaria de relatar aqui nesse espaço uma experiencia magica vivida hoje dia 11/09/2015 no sesc consolação, na peça da cia truks, &quot;acampatório&quot;, a melhor peça teatral que já assisti em toda a minha vida,um mixto de emoções, voltei a ser criança e confesso que chorei , alias, mais do que minha filha de 7 anos...risos, parabéns pelo trabalho, vocês conseguiram projetar com humor e emoção o que realmente  vivemos nessa jornada, e deixaram uma mensagem linda sobre amisade, parabéns.',
          'created_at' => '2015-09-13 00:00:00',
          'aprovado' => 1
        ],

        [
          'nome' => 'Camila d´Avila Salton',
          'email' => 'camila.avila@sesisp.org.br',
          'texto' => 'Rir, chorar, rir de novo, gargalhar, lembrar, emocionar!!!Um misto de emoções que não consegui explicar, só sentir! Obrigada aos &quot;doutores&quot; Gabriel, Rafael,Rogério, Thiago e Henrique pela dose de SONHOS que recebi hoje. A apresentação foi linda e, receber o &quot;tratamento&quot; no final, em cima do palco com vocês...foi sem palavras!',
          'created_at' => '2015-08-27 00:00:00',
          'aprovado' => 1
         ]
      ];

      DB::table('mensagens')->insert($registros);
    }
}
