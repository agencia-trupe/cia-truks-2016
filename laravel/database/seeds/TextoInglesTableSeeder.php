<?php

use Illuminate\Database\Seeder;

class TextoInglesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('texto_ingles')->delete();

      DB::table('texto_ingles')->insert([
        [
          'texto'  => 'Texto em inglês',
        ]
      ]);
    }
}
