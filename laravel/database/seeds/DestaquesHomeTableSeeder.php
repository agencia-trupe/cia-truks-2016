<?php

use Illuminate\Database\Seeder;

class DestaquesHomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('destaques_home')->delete();

      DB::table('destaques_home')->insert([
        [
          'titulo' => 'Destaque 1',
          'texto'   => 'Texto destaque 1',
          'link'  => '#',
          'ordem' => 0
        ],
        [
          'titulo' => 'Destaque 2',
          'texto'   => 'Texto destaque 2',
          'link'  => '#',
          'ordem' => 1
        ]
      ]);
    }
}
