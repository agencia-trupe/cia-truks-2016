<?php

use Illuminate\Database\Seeder;

class EspetaculosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('espetaculos')->delete();

      DB::table('espetaculos')->insert(
      [
        [
          'titulo' => 'Cidade Azul',
          'slug' => str_slug('Cidade Azul'),
        ],
        [
          'titulo' => 'Big Bang',
          'slug' => str_slug('Big Bang'),
        ],
        [
          'titulo' => 'Gigante',
          'slug' => str_slug('Gigante'),
        ],
        [
          'titulo' => 'Isto não é um cachimbo',
          'slug' => str_slug('Isto não é um cachimbo'),
        ],
        [
          'titulo' => 'E se as histórias fossem diferentes',
          'slug' => str_slug('E se as histórias fossem diferentes'),
        ],
        [
          'titulo' => 'O senhor dos sonhos',
          'slug' => str_slug('O senhor dos sonhos'),
        ],
        [
          'titulo' => 'Vovô',
          'slug' => str_slug('Vovô'),
        ],
        [
          'titulo' => 'Zôo-ilógico',
          'slug' => str_slug('Zôo-ilógico'),
        ],
        [
          'titulo' => 'História de bar',
          'slug' => str_slug('História de bar'),
        ],
        [
          'titulo' => 'Os vizinhos',
          'slug' => str_slug('Os vizinhos'),
        ],
        [
          'titulo' => 'A bruxinha',
          'slug' => str_slug('A bruxinha'),
        ],
        [
          'titulo' => 'Sonhatório',
          'slug' => str_slug('Sonhatório'),
        ],
        [
          'titulo' => 'Por uma estrela',
          'slug' => str_slug('Por uma estrela'),
        ],
        [
          'titulo' => 'Construtório',
          'slug' => str_slug('Construtório'),
        ],
        [
          'titulo' => 'Última notícia',
          'slug' => str_slug('Última notícia'),
        ],
        [
          'titulo' => 'Acampatório',
          'slug' => str_slug('Acampatório'),
        ]
      ]);
    }
}
