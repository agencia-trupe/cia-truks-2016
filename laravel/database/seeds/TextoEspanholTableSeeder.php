<?php

use Illuminate\Database\Seeder;

class TextoEspanholTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('texto_espanhol')->delete();

      DB::table('texto_espanhol')->insert([
        [
          'texto'  => 'Texto em espanhol',
        ]
      ]);
    }
}
